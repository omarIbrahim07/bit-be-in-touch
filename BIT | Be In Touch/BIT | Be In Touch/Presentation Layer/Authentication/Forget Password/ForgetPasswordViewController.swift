//
//  ForgetPasswordViewController.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/8/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController {

    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        closeKeypad()
    }
    
    func closeKeypad() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTapped() {
        phoneTextField.endEditing(true)
    }
    
    func configureView() {
        
        phoneView.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        phoneView.layer.shadowOpacity = 0.22
        phoneView.layer.cornerRadius = 8.0
        phoneView.layer.shadowRadius = 10
        
        forgetPasswordButton.layer.cornerRadius = 8.0
        forgetPasswordButton.layer.shadowColor = #colorLiteral(red: 0.4156862745, green: 0.007843137255, blue: 0.3803921569, alpha: 1)
        forgetPasswordButton.layer.shadowOpacity = 0.7

        forgetPasswordButton.setTitle("forget password button title".localized, for: .normal)
        phoneTextField.placeholder = "phone".localized

        self.navigationItem.title = "forget pass".localized
    }
    
    func changeTextFieldPlaceHolderColor(textField: UITextField, placeHolderString: String,fontSize: CGFloat) {
        textField.attributedPlaceholder = NSAttributedString(string: placeHolderString, attributes: [.foregroundColor: #colorLiteral(red: 0.7411764706, green: 0.7960784314, blue: 0.8862745098, alpha: 1), .font: UIFont.boldSystemFont(ofSize: fontSize)])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
