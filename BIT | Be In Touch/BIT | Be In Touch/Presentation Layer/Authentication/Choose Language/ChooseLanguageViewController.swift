//
//  ChooseLanguageViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 12/1/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH

class ChooseLanguageViewController: BaseViewController {
    
    var languageChoosed: String?
    var tutorials: [Tutorial] = [Tutorial]()

    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var arabicButton: UIButton!
    @IBOutlet weak var chooseLanguageButton: UIButton!
    @IBOutlet weak var englishImageView: UIImageView!
    @IBOutlet weak var arabImageView: UIImageView!
    @IBOutlet weak var toBeInTouchImageView: UIImageView!
    @IBOutlet weak var joinOurClubImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
        checkLanguage()
        if UserDefaults.standard.bool(forKey: "First Launch") != true {
            getTutorials()
        }
    }
    
    func checkLanguage() {
        if UserDefaults.standard.bool(forKey: "First Launch") == true {
            // MARK:- If u want to go tutorial, comment this line
            self.goToHome()
        } else {
            print("Lesa")
        }
    }

    func configure() {
//        logoView.roundCorners([.bottomLeft, .bottomRight], radius: 30.0)
        chooseLanguageButton.addCornerRadius(raduis: chooseLanguageButton.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        self.navigationItem.title = "choose language title".localized
        toBeInTouchImageView.image = UIImage(named: "To Be In Touch")
        joinOurClubImageView.image = UIImage(named: "Join Our Club")
    }
    
    func getTutorials() {
        
        let parameters: [String : AnyObject] = [
            :
        ]
        
        weak var weakSelf = self
        
        //        startLoading()
        self.animateGif(view: self.view)
        
        TutorialAPIManager().getTutorials(basicDictionary: parameters, onSuccess: { (tutorials) in
            
            self.tutorials = tutorials
            self.stopAnimateGif()
            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    //MARK:- Actions
    @IBAction func englishButtonIsPressed(_ sender: Any) {
        self.languageChoosed = "en"
        englishImageView.image = UIImage(named: "Engl")
        arabImageView.image = UIImage(named: "Arab")
        toBeInTouchImageView.image = UIImage(named: "To Be In Touch")
        joinOurClubImageView.image = UIImage(named: "Join Our Club")
    }
    
    @IBAction func arabicButtonIsPressed(_ sender: Any) {
        self.languageChoosed = "ar"
        englishImageView.image = UIImage(named: "Engo")
        arabImageView.image = UIImage(named: "Arabo")
        toBeInTouchImageView.image = UIImage(named: "To Be In Touch Ar")
        joinOurClubImageView.image = UIImage(named: "Join Our Club Ar")
    }
    
    
    @IBAction func chooseLanguageButtonIsPressed(_ sender: Any) {
        
        print("Msa Msa")
        guard let langChoosed = self.languageChoosed, langChoosed.count > 0 else {
            let apiError = APIError()
            apiError.message = "choose language error".localized
            self.showError(error: apiError)
            return
        }

        UserDefaults.standard.set(true, forKey: "First Launch")
        if langChoosed == "ar" {
            MOLH.setLanguageTo("ar")
        } else if langChoosed == "en" {
            MOLH.setLanguageTo("en")
        }

        MOLH.reset()
        resetFirstTime()
    }
    

    // MARK:- Navigation
    func goToHome() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
//        UIApplication.shared.keyWindow?.rootViewController = viewController
        
        
//        UIApplication.shared.keyWindow?.rootViewController = CustomTabbarViewController(nibName: "CustomTabbarViewController", bundle: nil)
        
//        let navigation = UINavigationController(rootViewController: CustomTabbarViewController(nibName: "CustomTabbarViewController", bundle: nil))
        let viewController = CustomTabbarViewController(nibName: "CustomTabbarViewController", bundle: nil)
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
    }
    
    func resetFirstTime() {
        if self.tutorials.count > 0 {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "TutorialViewController")
            UIApplication.shared.keyWindow?.rootViewController = viewController
        } else {
            self.goToHome()
        }
    }

}
