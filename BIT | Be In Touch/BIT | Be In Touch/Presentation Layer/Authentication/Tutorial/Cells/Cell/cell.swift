//
//  cell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 9/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView

class cell: FSPagerViewCell {

    @IBOutlet weak var pagerViewImageView: UIImageView!
    @IBOutlet weak var pagerViewTitleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
