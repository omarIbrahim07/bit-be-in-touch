//
//  TutorialViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 9/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView

extension TutorialViewController: FSPagerViewDataSource, FSPagerViewDelegate {
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
            return numberOfItems ?? 0
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index) as! cell
        let tutorial = self.tutorials[index]
        
        if let tutorialTitle: String = tutorial.tutorialTitle {
            cell.pagerViewTitleLabel.text = tutorialTitle
        }
        
        if let tutorialImage: String = tutorial.image {
            cell.pagerViewImageView.loadImageFromUrl(imageUrl: TUTORIALS_IMAGE_URL + tutorialImage)
        } else {
            cell.pagerViewImageView.image = UIImage(named: "Simulator Screen Shot ")
        }
        
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        print("Hena")
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        print("Hena2")
        self.pageControlView.currentPage = targetIndex
        self.currentIndex = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        print("Hena3")
        self.pageControlView.currentPage = pagerView.currentIndex
        self.currentIndex = pagerView.currentIndex
    }
    
    
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        switch sender.tag {
        case 1:
            let newScale = 0.5+CGFloat(sender.value)*0.5 // [0.5 - 1.0]
            self.pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
        case 2:
            self.pagerView.interitemSpacing = CGFloat(sender.value) * 20 // [0 - 20]
        case 3:
            self.numberOfItems = Int(roundf(sender.value*7.0))
            if let numberOfPages = self.numberOfItems {
                self.pageControlView.numberOfPages = numberOfPages
            }
            self.pagerView.reloadData()
        default:
            break
        }
    }

}

