//
//  TutorialViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 9/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import FSPagerView

class TutorialViewController: BaseViewController {
    
    var numberOfItems : Int? = 0
    var currentIndex: Int? = 0
    
    var tutorials: [Tutorial] = [Tutorial]()
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(UINib(nibName: "cell", bundle: nil), forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = FSPagerView.automaticSize
        }
    }
    @IBOutlet weak var pageControlView: FSPageControl! {
        didSet {
            self.pageControlView.numberOfPages = numberOfItems ?? 0
            self.pageControlView.contentHorizontalAlignment = .center
            self.pageControlView.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
            self.pageControlView.setFillColor(#colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1), for: .selected)
            self.pageControlView.setStrokeColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            //            pageControlView.setImage(UIImage(named:"AppIcon"), for: .selected)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        getTutorials()
        configureFSPageControl()
    }
    
    func checkTutorialPagesCount() {
        if self.tutorials.count < 1 {
            self.goToHome()
        }
    }
    
    // MARK:- Networking
    func getTutorials() {
        
        let parameters: [String : AnyObject] = [
            :
        ]
        
        weak var weakSelf = self
        
        //        startLoading()
        self.animateGif(view: self.view)
        
        TutorialAPIManager().getTutorials(basicDictionary: parameters, onSuccess: { (tutorials) in
            
            self.tutorials = tutorials
            self.numberOfItems = tutorials.count
            self.stopAnimateGif()
            
            self.bindPagerControlView()
            self.pagerView.reloadData()
            
                                            
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
        }
        
    }
    
    func configureFSPageControl() {
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.reloadData()
    }
    
    func bindPagerControlView() {
        self.pageControlView.numberOfPages = self.numberOfItems ?? 0
        if self.numberOfItems ?? 0 > 0 {
            self.pageControlView.currentPage = 0
        }
    }
    
    // MARK: - Actions
    @IBAction func nextButtonIsPressed(_ sender: Any) {
        if self.currentIndex != (numberOfItems ?? 0) - 1 {
            pagerView.scrollToItem(at: self.currentIndex! + 1, animated: true)
        } else if self.currentIndex == (numberOfItems ?? 0) - 1 {
            goToHome()
        }
    }
    
    @IBAction func skipButtonIsPressed(_ sender: Any) {
        goToHome()
    }
    
    // MARK: - Navigation
    func goToHome() {
        print("Log in")
        
        let viewController = CustomTabbarViewController(nibName: "CustomTabbarViewController", bundle: nil)
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
        
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        //        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
//        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeNavigationVC")
//        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
}
