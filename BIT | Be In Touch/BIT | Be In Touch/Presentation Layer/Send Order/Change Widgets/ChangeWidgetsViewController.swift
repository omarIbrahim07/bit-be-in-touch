//
//  ChangeWidgetsViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import StackedCollectionView

class ChangeWidgetsViewController: BaseViewController {
    
    var items: [Any] = Itemm.getArray()
    
//    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        besmAllah()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    let collectionView: UICollectionView = {
        let flowLayout = CustomFlowLayout()
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = UIColor.clear
        return collectionView
    }()
    
    func besmAllah() {
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.stackedDelegate = self
        collectionView.stackedDataSource = self
        collectionView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: CustomCollectionViewCell.identifier)
        
        view.addSubview(collectionView)
        topLayoutGuide.bottomAnchor.constraint(equalTo: collectionView.topAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: collectionView.leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: collectionView.rightAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true

//        let flowLayout = CustomFlowLayout()
//        collectionView.collectionViewLayout = flowLayout
//        collectionView.translatesAutoresizingMaskIntoConstraints = false
//        collectionView.dataSource = self
//        collectionView.delegate = self
//        collectionView.stackedDelegate = self
//        collectionView.stackedDataSource = self
//        collectionView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: CustomCollectionViewCell.identifier)
//
//        view.addSubview(collectionView)
//
//        topLayoutGuide.bottomAnchor.constraint(equalTo: collectionView.topAnchor).isActive = true
//        view.leftAnchor.constraint(equalTo: collectionView.leftAnchor).isActive = true
//        view.rightAnchor.constraint(equalTo: collectionView.rightAnchor).isActive = true
//        view.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: StackedCollectionViewDelegate

// MARK: UICollectionViewDataSource

extension ChangeWidgetsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCollectionViewCell.identifier, for: indexPath) as! CustomCollectionViewCell
        if let item = items[indexPath.item] as? Itemm {
            cell.items = [item]
        } else if let items = items[indexPath.item] as? [Itemm] {
            cell.items = items
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = items[sourceIndexPath.item]
        items.remove(at: sourceIndexPath.item)
        items.insert(item, at: destinationIndexPath.item)
    }
    
}

extension ChangeWidgetsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.goToFirstTemp()
    }
}

//extension HomeViewController: UICollectionViewDelegate {
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        guard let favouritecountriesCount: Int = self.countryFavouriteModel.count, favouritecountriesCount > 0 else {
//            self.showNoFavouriteCountriesPopUp()
//            return
//        }
//
//        if let item = items[indexPath.item] as? Itemm {
//
//            var bit = BIT.empty
//
//            if item.key == "currencyRates" {
//                bit = .currencyRates
//            } else if item.key == "Prayer" {
//                bit = .Prayer
//            } else if item.key == "Weather" {
//                bit = .Weather
//            } else {
//                bit = .empty
//            }
//
//            switch bit {
//                case .currencyRates:
//                    self.goToBitDetails(bit: .currencyRates)
//                case .Prayer:
//                    self.goToBitDetails(bit: .Prayer)
//                case .empty:
//                    print("empty")
//                case .Weather:
//                    self.goToBitDetails(bit: .Weather)
//            }
//
//            print(item.name)
//        }
//    }
//}

extension ChangeWidgetsViewController: StackedCollectionViewDelegate {
    
    func collectionViewGestureDidMoveOutsideTriggerRadius(_ collectionView: UICollectionView) {
        print("Gesture moved outside trigger radius")
    }
    
    func collectionView(_ collectionView: UICollectionView, animationControllerFor indexPath: IndexPath) -> UICollectionViewCellAnimatedTransitioning? {
        return CustomTransitionAnimator()
    }
    
}

// MARK: StackedCollectionViewDataSource

extension ChangeWidgetsViewController: StackedCollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt sourceIndexPath: IndexPath, into stackDestinationIndexPath: IndexPath) -> Bool {
        if
//            items[sourceIndexPath.item] is [Item], // Block moving a stack...
            items[stackDestinationIndexPath.item] is Item // ...onto an item.
        {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, into stackDestinationIndexPath: IndexPath) {
        
        print("Add item at index \(sourceIndexPath.item) into index \(stackDestinationIndexPath.item).")
        
        if let sourceItem = items[sourceIndexPath.item] as? Item {
            
            if let destinationItem = items[stackDestinationIndexPath.item] as? Item {
                
                items[stackDestinationIndexPath.item] = [sourceItem, destinationItem]
                items.remove(at: sourceIndexPath.item)
                
            } else if let destinationStack = items[stackDestinationIndexPath.item] as? [Item] {
                
                items[stackDestinationIndexPath.item] = destinationStack + [sourceItem]
                items.remove(at: sourceIndexPath.item)
                
            }
            
        } else if let sourceStack = items[sourceIndexPath.item] as? [Item] {
            
            if let destinationItem = items[stackDestinationIndexPath.item] as? Item {
                
                items[stackDestinationIndexPath.item] = sourceStack + [destinationItem]
                items.remove(at: sourceIndexPath.item)
                
            } else if let destinationStack = items[stackDestinationIndexPath.item] as? [Item] {
                
                items[stackDestinationIndexPath.item] = sourceStack + destinationStack
                items.remove(at: sourceIndexPath.item)
                
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, finishMovingItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        print("Move item from index \(sourceIndexPath.item) to index \(destinationIndexPath.item).")
    }
    
}
