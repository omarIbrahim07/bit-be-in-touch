//
//  SetWidegetViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension SetWidgetViewController: SendCountriesDelegate {
    func sendCountries(countries: [String]) {
        self.newCountries = []
        self.newCountries = countries
        print("NewCountries: \(self.newCountries)")
        print(self.newCountries.count)
    }
}

extension SetWidgetViewController: SendDataDelegate {
    func sendData(text: String) {
        print(text)
        self.newLanguage = text
    }
}

extension SetWidgetViewController: WidgetSettingButtonTableViewCellDelegate {
    func widgetSettingButtonPressed(index: Int) {
        if index == 0 {
            print(self.languages)
            self.goToSetLanguage(languages: self.languages)
        }
        if index == 1 {
//            self.goToSetCountries(countries: self.countries, settingCountries: bitSettings?.countries ?? [""])
            self.goToSetCountries(countries: self.countries, settingCountries: self.newCountries ?? [""])
        }
        if index == 2 {
            if self.searchedBIT.count > 0 {
                if let website = self.searchedBIT[0].websiteLink {
                    self.goToWebsite(website: website)
                }
            }
        }
    }
}

// MARK:- TableView Delegate
extension SetWidgetViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if "Lang".localized == "ar" {
            if self.searchedBIT.count > 0 {
                if let website = self.searchedBIT[0].websiteLink {
                    return settingNamesAr.count + 1
                } else {
                    return settingNamesArWithoutWebsite.count + 1
                }
            } else {
                return settingNamesArWithoutWebsite.count + 1
            }
        } else if "Lang".localized == "en" {
            if self.searchedBIT.count > 0 {
                if let website = self.searchedBIT[0].websiteLink {
                    return settingNamesEn.count + 1
                } else {
                    return settingNamesEnWithoutWebsite.count + 1
                }
            } else {
                return settingNamesEnWithoutWebsite.count + 1
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell: AboutBITTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AboutBITTableViewCell") as? AboutBITTableViewCell {
                
                if let bit = self.bit {
                    cell.bindDescription(bit: bit)
                }
                                
                cell.checkDescription()
                                
                if cell.pressed == true {
                    cell.moreButton.setTitle("more button title".localized, for: .normal)
                } else if cell.pressed == false {
                    cell.moreButton.setTitle("less button title".localized, for: .normal)
                }
                cell.aboutBITDelegate = self
                return cell
            }
        }
        
        if let cell: WidgetSettingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WidgetSettingTableViewCell") as? WidgetSettingTableViewCell {
            
            cell.cellButton.tag = indexPath.row - 1
            cell.delegate = self
                        
            if indexPath.row == 1 {
                if "Lang".localized == "en" {
                    cell.settingLabel.text = settingNamesEn[indexPath.row - 1]
                } else if "Lang".localized == "ar" {
                    cell.settingLabel.text = settingNamesAr[indexPath.row - 1]
                }
                cell.languageView.isHidden = true
//                cell.settingImageView.isHidden = true
//                cell.cellButton.isHidden = true
                if "Lang".localized == "en" {
                    cell.settingImageView.image = UIImage(named: "Right arrow")
                } else if "Lang".localized == "ar" {
                    cell.settingImageView.image = UIImage(named: "Arrow")
                }
            } else if indexPath.row > 1 {
                if indexPath.row > 2 {
                    cell.settingImageHeightConstraint.constant = 20
                }
                if "Lang".localized == "en" {
                    if self.searchedBIT.count > 0 {
                        if let website = self.searchedBIT[0].websiteLink {
                            cell.setSetting(forLabel: settingNamesEn[indexPath.row - 1], forImage: imageNames[indexPath.row - 2])
                        }
                    } else {
                        cell.setSetting(forLabel: settingNamesEnWithoutWebsite[indexPath.row - 1], forImage: imageNamesWithoutWebsite[indexPath.row - 2])
                    }
                } else if "Lang".localized == "ar" {
                    if self.searchedBIT.count > 0 {
                        if let website = self.searchedBIT[0].websiteLink {
                            cell.setSetting(forLabel: settingNamesAr[indexPath.row - 1], forImage: imageNamesAr[indexPath.row - 2])
                        }
                    } else {
                        cell.setSetting(forLabel: settingNamesArWithoutWebsite[indexPath.row - 1], forImage: imageNamesArWithoutWebsite[indexPath.row - 2])
                    }
                }

                cell.languageView.isHidden = true
            }
            
            if indexPath.row == settingNamesEn.count {
                cell.cellView.isHidden = true
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
}

extension SetWidgetViewController: AboutBITButtonTableViewCellDelegate {
    func didAboutBITButtonPressed(tag: Int) {
        tableView.reloadData()
    }
}

extension SetWidgetViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Discover" {
            self.goToDiscoverPage()
        } else if keyWord == "Search" {
            self.goToSearchPage()
        } else if keyWord == "Home" {
            self.goToHomeBITS()
        } else if keyWord == "Settings" {
            self.goToSettingsPage()
        } else if keyWord == "Scanner" {
            self.goToScannerPage()
        }
    }
}

