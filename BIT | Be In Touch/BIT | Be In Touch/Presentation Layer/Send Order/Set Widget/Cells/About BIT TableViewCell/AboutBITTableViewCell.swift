//
//  AboutBITTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/19/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol AboutBITButtonTableViewCellDelegate {
    func didAboutBITButtonPressed(tag: Int)
}

class AboutBITTableViewCell: UITableViewCell {
    
    var pressed: Bool = true
    
    var aboutBITDelegate: AboutBITButtonTableViewCellDelegate?
    
    @IBOutlet weak var aboutBITLabel: UILabel!
    @IBOutlet weak var aboutDescriptionLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var bitSettingLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        setLocalization()
    }
    
    func configureView() {
    }
    
    func checkDescription() {
        if aboutDescriptionLabel.maxNumberOfLines < 5 {
//            print("Meas: \(aboutDescriptionLabel.numberOfLines)")
//            print("Meas: \(aboutDescriptionLabel.text?.count)")
//            print("Meas: \(aboutDescriptionLabel.maxNumberOfLines)")
            moreButton.isHidden = true
        } else {
            moreButton.isHidden = false
        }
    }
    
    func setLocalization() {
        aboutBITLabel.text = "about bit title".localized
        bitSettingLabel.text = "settings cell title".localized
        moreButton.setTitle("more button title".localized, for: .normal)
    }
    
    func didAboutBITButtonPressed(tag: Int) {
        if let delegateValue = aboutBITDelegate {
            delegateValue.didAboutBITButtonPressed(tag: tag)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindDescription(bit: BIT) {
        if let description = bit.description {
            aboutDescriptionLabel.text = description
        }
    }
    
    @IBAction func moreButtonIsPressed(_ sender: Any) {
        print("More is pressed")
        if pressed == true {
            if aboutDescriptionLabel.maxNumberOfLines > 4 {
                pressed = false
                aboutDescriptionLabel.numberOfLines = 0
                didAboutBITButtonPressed(tag: moreButton.tag)
            }
        } else if pressed == false {
            if aboutDescriptionLabel.maxNumberOfLines > 4 {
                pressed = true
                aboutDescriptionLabel.numberOfLines = 4
                didAboutBITButtonPressed(tag: moreButton.tag)
            }
        }
    }
    
}
