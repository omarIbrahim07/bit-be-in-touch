//
//  WidgetSettingTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/18/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol WidgetSettingButtonTableViewCellDelegate {
    func widgetSettingButtonPressed(index: Int)
}
    
class WidgetSettingTableViewCell: UITableViewCell {

    var delegate: WidgetSettingButtonTableViewCellDelegate?

    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var settingImageView: UIImageView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var englishImage: UIImageView!
    @IBOutlet weak var arabicImage: UIImageView!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var cellButton: UIButton!
    @IBOutlet weak var settingImageHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setSetting(forLabel settingLabel: String, forImage settingImage: String) {
        self.settingLabel.text = settingLabel
        self.settingImageView.image = UIImage(named: settingImage)
    }
    
    func widgetSettingButtonPressed(index: Int) {
        if let delegateValue = delegate {
            delegateValue.widgetSettingButtonPressed(index: index)
        }
    }
    
    @IBAction func settingButtonIsPressed(_ sender: Any) {
        widgetSettingButtonPressed(index: cellButton.tag)
    }
    
    @IBAction func englishButtonIsPressed(_ sender: Any) {
        print("English")
        englishImage.image = UIImage(named: "Checkd")
        arabicImage.image = UIImage(named: "Unchecd")
    }
    
    @IBAction func arabicButtonIsPressed(_ sender: Any) {
        print("Arabic")
        arabicImage.image = UIImage(named: "Checkd")
        englishImage.image = UIImage(named: "Unchecd")
    }
    
}
