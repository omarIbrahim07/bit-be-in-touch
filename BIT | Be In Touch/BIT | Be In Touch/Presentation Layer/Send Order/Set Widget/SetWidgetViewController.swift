//
//  SetWidgetViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/18/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
// MARK:- Delegate between two VCS

class SetWidgetViewController: BaseViewController {
    
    var bit: BIT?
    var countries: [String] = [String]()
    var newCountries: [String] = [String]()
    var newCountriesEdited: [String] = [String]()
    var languages: [String] = [String]()
    var newLanguage: String?
    var language: String?
    var bitSettings: Setting?
    var isSet: Bool? = false
    var deepLinkBitCode: String?
    var bitCode: String?
    
    var isDeepLink: Bool? = false
    
    var searchedBIT: [BIT] = [BIT]()

    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView

    var settingNamesEn: [String] = ["BIT Language", "Countries result list", "Developer Website", "Privacy Policy"]
    var settingNamesAr: [String] = ["لغة ال بت", "البلاد المفضلة لل بت", "الموقع الإلكتروني", "خصوصية الاستخدام"]
    var imageNames: [String] = ["Right arrow", "World", "Kaf Mariem"]
    var imageNamesAr: [String] = ["Arrow", "World", "Kaf Mariem"]
    
    var settingNamesEnWithoutWebsite: [String] = ["BIT Language", "Countries result list", "Privacy Policy"]
    var settingNamesArWithoutWebsite: [String] = ["لغة ال بت", "البلاد المفضلة لل بت", "خصوصية الاستخدام"]
    var imageNamesWithoutWebsite: [String] = ["Right arrow", "Kaf Mariem"]
    var imageNamesArWithoutWebsite: [String] = ["Arrow", "Kaf Mariem"]

    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bitImageView: UIImageView!
    @IBOutlet weak var bitNameLabel: UILabel!
    @IBOutlet weak var bitOwnerLabel: UILabel!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var bitTypeLabel: UILabel!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var dateValueLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.isHidden = true
        configureView()
        configureTableView()
        
        if let bitCode = self.bitCode {
            getBIT(bitCode: bitCode)
            self.getBITSettings(bitCode: bitCode)
        }
        
        if let bit = self.bit {
            self.bindBIT(bit: bit)
            if let bitCode = bit.bitCode {
                getBIT(bitCode: bitCode)
                self.getBITSettings(bitCode: bitCode)
            }
        }
        if isDeepLink == true {
            if let bitCode = self.deepLinkBitCode {
                self.getBITSettings(bitCode: bitCode)
            }
        }
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "WidgetSettingTableViewCell", bundle: nil), forCellReuseIdentifier: "WidgetSettingTableViewCell")
        tableView.register(UINib(nibName: "AboutBITTableViewCell", bundle: nil), forCellReuseIdentifier: "AboutBITTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func configureView() {
        headerView.isHidden = true
        settingButton.setTitle("update button".localized, for: .normal)
        settingButton.setTitle("set button title".localized, for: .normal)
        if "Lang".localized == "en" {
            arrowImageView.image = UIImage(named: "Right arrow")
        } else if "Lang".localized == "ar" {
            arrowImageView.image = UIImage(named: "Arrow")
        }
        lastUpdateLabel.text = "last update label".localized
        bitTypeLabel.text = "bit type label".localized
        bitImageView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        reusableView.frame = self.viewContainToolBar.bounds
//        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
    }
        
    func bindBIT(bit: BIT) {
        if let bitName = bit.title {
            bitNameLabel.text = bitName
        }
        if let image = bit.image {
            bitImageView.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + image)
        } else {
            bitImageView.image = UIImage(named: "Default image for BIT")
        }
        if let countries = bit.countries {
            self.countries = countries
        }
        if let languages = bit.languages {
            self.languages = languages
        }
        if let category = bit.category {
            self.bitTypeLabel.text = category
        }
        if let owner = bit.ownerName {
            if "Lang".localized == "en" {
                self.bitOwnerLabel.text = "By: " + owner
            } else if "Lang".localized == "ar" {
                self.bitOwnerLabel.text = "بواسطة: " + owner
            }
        }
        headerView.isHidden = false
    }
    
    func checkIsSet(isSet: Bool) {
        if isSet == true {
            settingButton.setTitle("update button".localized, for: .normal)
        } else if isSet == false {
            settingButton.setTitle("set button title".localized, for: .normal)
        }
    }
    
//MARK:- Networking
    func getBITSettings(bitCode: String) {
        
//        self.startLoading()
        self.animateGif(view: self.view)
        
        let params: [String : AnyObject] = [
            "bit_code" : bitCode as AnyObject,
            "device_id" : deviceID as AnyObject
        ]

        SettingAPIManager().getBITSettings(basicDictionary: params, onSuccess: { (bitSettings) in
            
            self.bitSettings = bitSettings
            if let language = bitSettings.language {
                self.newLanguage = language
            }
            if let countries = bitSettings.countries {
                self.newCountries = countries
            }
            if let category = bitSettings.category {
                self.bitTypeLabel.text = category
            }
            if let updatedAt = bitSettings.updatedAt {
                self.dateValueLabel.text = updatedAt
            }
            self.isSet = true
            self.checkIsSet(isSet: self.isSet!)
            if self.isDeepLink == true {
                self.getBIT(bitCode: bitCode)
            }
            
            print(bitSettings)
            //            self.stopLoadingWithSuccess()
            self.stopAnimateGif()
            self.tableView.isHidden = false
            
            self.tableView.reloadData()
                        
        }) { (error) in
            self.stopLoadingWithError(error: error)
            self.tableView.isHidden = false
            if self.isDeepLink == true {
                self.getBIT(bitCode: bitCode)
            }
            self.headerView.isHidden = false
        }
    }
    
    func getBIT(bitCode: String) {
        
        self.searchedBIT = []
        
        let parameters: [String : AnyObject] = [
            "bit_codes[]" : bitCode as AnyObject,
            "locale": "Lang".localized as AnyObject,
        ]
                
//        self.startLoading()
        self.animateGif(view: self.view)
        
        BITAPIManager().getSearchedBIT(basicDictionary: parameters , onSuccess: { (searchedBIT) in
            
//            self.stopLoadingWithSuccess()
            self.stopAnimateGif()
            
            self.bit = searchedBIT[0]
            self.searchedBIT = searchedBIT
            if self.searchedBIT.count > 0 {
//                self.searched = true
                self.bindBIT(bit: searchedBIT[0])
                self.tableView.reloadData()
            }
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }

    
    func createSettings() {
        
        if self.isDeepLink == false {
            guard let bitCode = self.bitCode, bitCode.count > 0 else {
                let apiError = APIError()
                apiError.message = "bit code is not available error".localized
                showError(error: apiError)
                return
            }
            
            guard let language = self.newLanguage, language.count > 0 else {
                let apiError = APIError()
                apiError.message = "no language choosed error".localized
                showError(error: apiError)
                return
            }
            
            if newCountries.count == 0 {
                let apiError = APIError()
                apiError.message = "no countries choosed error".localized
                showError(error: apiError)
                return
            }
            
            let device = uuid
            
            let parameters: [String : AnyObject] = [
                "bit_code" : bitCode as AnyObject,
                "device_id" : deviceID as AnyObject,
                //            "device_id" : deviceID as AnyObject,
                "locale" : language as AnyObject,
            ]
            
            weak var weakSelf = self
            
//            startLoading()
            self.animateGif(view: self.view)
            
            SettingAPIManager().createBITSettings(countriesArray: self.newCountries, arrayKey: "countries", basicDictionary: parameters, onSuccess: { (setting) in
                
//                self.stopLoadingWithSuccess()
                self.stopAnimateGif()
                self.showSetWdigetSuccessfullyPopUp()
                print(setting)
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
        } else if self.isDeepLink == true {
            guard let bitCode = self.deepLinkBitCode, bitCode.count > 0 else {
                let apiError = APIError()
                apiError.message = "bit code is not available error".localized
                showError(error: apiError)
                return
            }
            
            guard let language = self.newLanguage, language.count > 0 else {
                let apiError = APIError()
                apiError.message = "no language choosed error".localized
                showError(error: apiError)
                return
            }
            
            if newCountries.count == 0 {
                let apiError = APIError()
                apiError.message = "no countries choosed error".localized
                showError(error: apiError)
                return
            }
            
            let device = uuid
            
            let parameters: [String : AnyObject] = [
                "bit_code" : bitCode as AnyObject,
                "device_id" : deviceID as AnyObject,
                //            "device_id" : deviceID as AnyObject,
                "locale" : language as AnyObject,
            ]
            
            weak var weakSelf = self
            
//            startLoading()
            self.animateGif(view: self.view)
            
            SettingAPIManager().createBITSettings(countriesArray: self.newCountries, arrayKey: "countries", basicDictionary: parameters, onSuccess: { (setting) in
                
//                self.stopLoadingWithSuccess()
                self.stopAnimateGif()
                self.showSetWdigetSuccessfullyPopUp()
                print(setting)
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
        }
        
    }
    
    func updateSettings() {
        if isDeepLink == false {
            guard let bitCode = self.bitCode, bitCode.count > 0 else {
                let apiError = APIError()
                apiError.message = "bit code is not available error".localized
                showError(error: apiError)
                return
            }
            
            guard let language = self.newLanguage, language.count > 0 else {
                let apiError = APIError()
                apiError.message = "no language choosed error".localized
                showError(error: apiError)
                return
            }
            
            if self.newCountries.count == 0 {
                let apiError = APIError()
                apiError.message = "no countries choosed error".localized
                showError(error: apiError)
                return
            }
            
            //        let device = uuid
            
            let parameters: [String : AnyObject] = [
                "bit_code" : bitCode as AnyObject,
                "device_id" : deviceID as AnyObject,
                //            "device_id" : device as AnyObject,
                "locale" : language as AnyObject,
            ]
            
            weak var weakSelf = self
            
//            startLoading()
            self.animateGif(view: self.view)
            
            SettingAPIManager().updateBITSettings(countriesArray: self.newCountries, arrayKey: "countries", basicDictionary: parameters, onSuccess: { (setting) in
                
//                self.stopLoadingWithSuccess()
                self.stopAnimateGif()
                self.showSetWdigetSuccessfullyPopUp()
                print(setting)
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }
        } else if isDeepLink == true {
            guard let bitCode = self.deepLinkBitCode, bitCode.count > 0 else {
                let apiError = APIError()
                apiError.message = "bit code is not available error".localized
                showError(error: apiError)
                return
            }

            guard let language = self.newLanguage, language.count > 0 else {
                let apiError = APIError()
                apiError.message = "no language choosed error".localized
                showError(error: apiError)
                return
            }
            
            if self.newCountries.count == 0 {
                let apiError = APIError()
                apiError.message = "no countries choosed error".localized
                showError(error: apiError)
                return
            }
            
            //        let device = uuid
            
            let parameters: [String : AnyObject] = [
                "bit_code" : bitCode as AnyObject,
                "device_id" : deviceID as AnyObject,
                //            "device_id" : device as AnyObject,
                "locale" : language as AnyObject,
            ]
            
            weak var weakSelf = self
            
//            startLoading()
            self.animateGif(view: self.view)
            
            SettingAPIManager().updateBITSettings(countriesArray: self.newCountries, arrayKey: "countries", basicDictionary: parameters, onSuccess: { (setting) in
                
//                self.stopLoadingWithSuccess()
                self.stopAnimateGif()
                self.showSetWdigetSuccessfullyPopUp()
                print(setting)
                
            }) { (error) in
                weakSelf?.stopLoadingWithError(error: error)
            }

        }

    }
    
    func showAboutBITPopUp(aboutBIT aboutBit: String) {
        let alertController = UIAlertController(title: "about bit title".localized, message: aboutBit, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "okay settings".localized, style: .cancel)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func showSetWdigetSuccessfullyPopUp() {
        let alertController = UIAlertController(title: "bit settings added title".localized, message: "bit settings added alert message".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "go to bits".localized, style: .default) { (action) in
            self.goToHomeBITS()
        }
//        let cancelAction = UIAlertAction(title: "cancel settings".localized, style: .cancel)
        alertController.addAction(openAction)
//        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    // MARK:- Actions
    @IBAction func setButtonIsPressed(_ sender: Any) {
        if settingButton.titleLabel?.text == "set button title".localized {
            self.createSettings()
        } else if settingButton.titleLabel?.text == "update button".localized {
            self.updateSettings()
        }
    }
    
    @IBAction func updateButtonIsPressed(_ sender: Any) {
        self.viewDidLoad()
    }
    
    
    @IBAction func backButtonIsPressed(_ sender: Any) {
//        self.goToHomeBITS()
//        self.navigationController?.popToRootViewController(animated: true)
        let viewController = CustomTabbarViewController(nibName: "CustomTabbarViewController", bundle: nil)
        appDelegate.window!.rootViewController = viewController
//        appDelegate.window!.makeKeyAndVisible()

//        UIApplication.shared.keyWindow?.rootViewController = CustomTabbarViewController(nibName: "CustomTabbarViewController", bundle: nil)
    }
    
// MARK: - Navigation
    func goToSetLanguage(languages: [String]) {
        if let setLanguageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetLanguagesViewController") as? SetLanguagesViewController {
            setLanguageVC.languages = languages
            setLanguageVC.delegate = self
            if #available(iOS 13.0, *) {
                setLanguageVC.modalPresentationStyle = .automatic
            }
            self.present(setLanguageVC, animated: true, completion: nil)
        }
    }
    
    func goToSetCountries(countries: [String], settingCountries: [String]) {
        if let setCountriesVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetCountriesViewController") as? SetCountriesViewController {
            setCountriesVC.allSettingCountries = countries
            setCountriesVC.settingCountries = settingCountries
            setCountriesVC.delegate = self
            if #available(iOS 13.0, *) {
                setCountriesVC.modalPresentationStyle = .automatic
            } else {
                // Fallback on earlier versions
            }
            self.present(setCountriesVC, animated: true, completion: nil)
        }
    }
    
    func goToWebsite(website: String) {
        if let url = URL(string: website),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
}

