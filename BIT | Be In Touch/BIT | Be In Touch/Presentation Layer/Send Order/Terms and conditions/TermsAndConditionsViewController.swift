//
//  TermsAndConditionsViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 10/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: BaseViewController {

    @IBOutlet weak var termsAndConditionsLabel: UILabel!
    @IBOutlet weak var termsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        getTermsAndConditions()
    }
    
    func configureView() {
        termsAndConditionsLabel.text = "terms & conditions label".localized
    }
    
    func bindTermsAndConditions(terms: Terms) {
        if "Lang".localized == "en" {
            if let termsEn: String = terms.conetntEN {
                termsLabel.text = termsEn
            }
        } else if "Lang".localized == "ar" {
            if let termsAr: String = terms.conetntAR {
                termsLabel.text = termsAr
            }
        }
    }
    
    // MARK:- Networking
    func getTermsAndConditions() {
                                
        let parameters: [String : AnyObject] = [:]
        
        //        self.startLoading()
        self.animateGif(view: self.view)
        
        TermsAPIManager().getTermsAndConditions(basicDictionary: parameters , onSuccess: { (terms) in
            
            //            self.stopLoadingWithSuccess()
            self.stopAnimateGif()
            self.bindTermsAndConditions(terms: terms)
                        
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
        
    }

    // MARK: - Navigation
    
}
