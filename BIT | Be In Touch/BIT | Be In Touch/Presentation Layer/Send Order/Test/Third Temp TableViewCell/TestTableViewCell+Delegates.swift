//
//  TestTableViewCell+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 9/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension TestTableViewCell: ThirdCellTableViewCellDelegate {
    func leftTableViewMoreButtonPressed(tag: Int) {
        secondLeftTableViewMoreButtonPressed(tag: tag)
    }
    
    func rightTableViewMoreButtonPressed(tag: Int) {
        secondRightTableViewMoreButtonPressed(tag: tag)
    }
    
    func leftTableViewrightArrowButtonPressed(symbol: String) {
        secondLeftTableViewrightArrowButtonPressed(symbol: symbol)
    }
    
    func rightTableViewrightArrowButtonPressed(symbol: String) {
        secondRightTableViewrightArrowButtonPressed(symbol: symbol)
    }
    
    func leftTableViewitemImageButtonPressed(tag: Int) {
        secondLeftTableViewitemImageButtonPressed(tag: tag)
    }
    
    func rightTableViewitemImageButtonPressed(tag: Int) {
        secondRightTableViewitemImageButtonPressed(tag: tag)
    }
}

extension TestTableViewCell: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 0 {
            return leftBitsDetails.count
        } else if tableView.tag == 1 {
            return rightBitsDetails.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.selectedIndex == indexPath.row && self.isCollapsed == true {
            if let cell = tableView.cellForRow(at: indexPath) as? Test2TableViewCell {
                print(cell.descriptionLabel.maxNumberOfLines)
                let lineCount = cell.descriptionLabel.maxNumberOfLines
                var externalLink: String? = cell.externalLink
                var containItems: Int? = cell.containItems
                
                print("Extra link: \(externalLink)")
                print("containItems: \(containItems)")
                if (lineCount == 0 && cell.descriptionLabel.text == "" && externalLink != nil) || (lineCount == 0 && cell.descriptionLabel.text == "" && containItems == 1) {
                    self.extraHeight = 0
                    return 158
                } else if (lineCount == 0 && cell.descriptionLabel.text == "" && externalLink == nil) || (lineCount == 0 && cell.descriptionLabel.text == "" && containItems != 1) {
                    self.extraHeight = 0
                    return 158
                } else if (lineCount != 0 && cell.descriptionLabel.text != "" && externalLink != nil) || (lineCount != 0 && cell.descriptionLabel.text != "" && containItems == 1) {
                    let height: Float = Float(205 + (lineCount * 18))
                    self.extraHeight = height
                    if let extraHeight = self.extraHeight {
                        extraDistance(distance: extraHeight)
                    }
                    return CGFloat(height)
                } else if (lineCount != 0 && cell.descriptionLabel.text != "" && externalLink == nil) || (lineCount != 0 && cell.descriptionLabel.text != "" && containItems != 1) {
                    let height: Float = Float(168 + (lineCount * 18))
                    self.extraHeight = height
                    if let extraHeight = self.extraHeight {
                        extraDistance(distance: extraHeight)
                    }
                    return CGFloat(height)
                }
            }
        } else {
            print("TETEEt")
            self.extraHeight = 0
            return 158
        }
        return 158
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if tableView.tag == 0 {
                        
            if let cell: Test2TableViewCell = tableView.dequeueReusableCell(withIdentifier: "Test2TableViewCell") as? Test2TableViewCell {
                                                
                if (self.leftBitsDetails[indexPath.row].description == nil || self.leftBitsDetails[indexPath.row].description == "") && self.leftBitsDetails[indexPath.row].externalLink == nil && self.leftBitsDetails[indexPath.row].containItens != 1 {
                    print("Leeh kol el m2asy 3la m2asy")
                    cell.arrowImageView.isHidden = true
                    cell.moreButton.isHidden = true
                    cell.arrowImageWidthConstraint.constant = 0
                }
                                
                if "Lang".localized == "en" {
                    if self.localization == "ar" {
                        //                        self.countriesCollectionView.semanticContentAttribute = .forceRightToLeft
                        //                            self.bitView.semanticContentAttribute = .forceRightToLeft
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.moreButton.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceRightToLeft
                        cell.valueLabel.semanticContentAttribute = .forceRightToLeft
                        cell.titleLabel.semanticContentAttribute = .forceRightToLeft
                    }
                } else if "Lang".localized == "ar" {
                    if self.localization == "en" {
                        //                            self.bitView.semanticContentAttribute = .forceLeftToRight
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceLeftToRight
                        cell.valueLabel.semanticContentAttribute = .forceLeftToRight
                        cell.titleLabel.semanticContentAttribute = .forceLeftToRight
                    }
                }
                
                // Five states
                
                // description only -> arrow without action
                if self.leftBitsDetails[indexPath.row].description != "" && self.leftBitsDetails[indexPath.row].description != nil {
                    print("Arrow without action")
                }
                
                // contain items or external link only -> arrow with action
                if (self.leftBitsDetails[indexPath.row].containItens == 1 && (self.leftBitsDetails[indexPath.row].description == "" || self.leftBitsDetails[indexPath.row].description == nil)) || (self.leftBitsDetails[indexPath.row].externalLink != nil && (self.leftBitsDetails[indexPath.row].description == "" || self.leftBitsDetails[indexPath.row].description == nil)) {
                    print("Arrow with Action")
                    cell.delegate = self
                }
                
                // description with ( contain or external ) -> more with action & arrow without
                if (self.leftBitsDetails[indexPath.row].containItens == 1 && self.leftBitsDetails[indexPath.row].description != "" && self.leftBitsDetails[indexPath.row].description != nil) || (self.leftBitsDetails[indexPath.row].externalLink != nil && self.leftBitsDetails[indexPath.row].description != "" && self.leftBitsDetails[indexPath.row].description != nil) {
                    print("Arrow with more Action")
                    cell.arrowButton.isHidden = true
                    cell.delegate = self
                }
                
                cell.moreButton.tag = indexPath.row
                cell.arrowButton.tag = indexPath.row
                cell.imageButton.tag = indexPath.row
//                cell.delegate = self
                cell.tag = 0
                
                
                if let attr1: String = self.leftBitsDetails[indexPath.row].attribue1 {
                    cell.titleLabel.text = attr1
                }
                
                if let attr2: String = leftBitsDetails[indexPath.row].attribue2 {
                    cell.valueLabel.text = attr2
                }
                
                if let image: String = leftBitsDetails[indexPath.row].image {
                    cell.cellImageView.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + image)
                }
                
//                cell.descriptionLabel.text = "Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah"
                if let description: String = leftBitsDetails[indexPath.row].description {
                    cell.descriptionLabel.text = description
                }
                
                if let bitExternalLink = leftBitsDetails[indexPath.row].externalLink {
                    cell.externalLink = bitExternalLink
                }
                
                if let bitContainItems = leftBitsDetails[indexPath.row].containItens {
                    cell.containItems = bitContainItems
                }
                
                if "Lang".localized == "en" {
                    cell.arrowImageView.image = UIImage(named: "right arrow")
                } else if "Lang".localized == "ar" {
                    cell.arrowImageView.image = UIImage(named: "left arrow")
                }
                print("s1")
                
                if indexPath == currentlySelectedRow {
                    if cell.descriptionLabel.text?.isEmpty == true {
                        print("s2")
                        if "Lang".localized == "en" {
                            print("s21")
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            print("s22")
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
//                        cell.moreButton.isHidden = true
                    } else {
                        print("s3")
                        if selectedTableView == 0 {
                            cell.arrowImageView.image = UIImage(named: "down")
                        }
                        if cell.externalLink == nil {
                            print("s31")
//                            cell.moreButton.isHidden = true
                        } else {
                            print("s32")
//                            cell.moreButton.isHidden = false
                        }
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == true {
                    if cell.descriptionLabel.text == "" {
                        print("s5")
                        if "Lang".localized == "en" {
                            print("s51")
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            print("s52")
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
//                        cell.moreButton.isHidden = true
                    } else {
                        print("s6")
                        if currentlySelectedRow! == indexPath {
                            if self.selectedTableView == 0 {
                                print("Selected: \(selectedTableView)")
                                cell.arrowImageView.image = UIImage(named: "down")
                            }
                        }
                        if cell.externalLink == nil {
                            print("s61")
//                            cell.moreButton.isHidden = true
                        } else {
                            print("s62")
//                            cell.moreButton.isHidden = false
                        }
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == false {
                    print("s4")
                    if "Lang".localized == "en" {
                        print("s41")
                        cell.arrowImageView.image = UIImage(named: "right arrow")
                    } else if "Lang".localized == "ar" {
                        print("s42")
                        cell.arrowImageView.image = UIImage(named: "left arrow")
                    }
//                    cell.moreButton.isHidden = true
                }
                
                //                if leftBitsDetails[indexPath.row].containItens == 1 {
//                    if "Lang".localized == "en" {
//                        cell.arrowImageView.image = UIImage(named: "right arrow")
//                    } else if "Lang".localized == "ar" {
//                        cell.arrowImageView.image = UIImage(named: "left arrow")
//                    }
//                }
                
                return cell
            }
            
            
        } else if tableView.tag == 1 {
                        
            if let cell: Test2TableViewCell = tableView.dequeueReusableCell(withIdentifier: "Test2TableViewCell") as? Test2TableViewCell {
                                
                if (self.rightBitsDetails[indexPath.row].description == nil || self.rightBitsDetails[indexPath.row].description == "") && self.rightBitsDetails[indexPath.row].externalLink == nil && self.rightBitsDetails[indexPath.row].containItens != 1 {
                    print("Leeh kol el m2asy 3la m2asy")
                    cell.arrowImageView.isHidden = true
                    cell.moreButton.isHidden = true
                    cell.arrowImageWidthConstraint.constant = 0
                }

                
                if "Lang".localized == "en" {
                    if self.localization == "ar" {
                        //                        self.countriesCollectionView.semanticContentAttribute = .forceRightToLeft
                        //                            self.bitView.semanticContentAttribute = .forceRightToLeft
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.moreButton.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceRightToLeft
                        cell.valueLabel.semanticContentAttribute = .forceRightToLeft
                        cell.titleLabel.semanticContentAttribute = .forceRightToLeft
                    }
                } else if "Lang".localized == "ar" {
                    if self.localization == "en" {
                        //                            self.bitView.semanticContentAttribute = .forceLeftToRight
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceLeftToRight
                        cell.valueLabel.semanticContentAttribute = .forceLeftToRight
                        cell.titleLabel.semanticContentAttribute = .forceLeftToRight
                    }
                }
                
                // Five states
                
                // description only -> arrow without action
                if self.rightBitsDetails[indexPath.row].description != "" && self.rightBitsDetails[indexPath.row].description != nil {
                    print("Arrow without action")
                }
                
                // contain items or external link only -> arrow with action
                if (self.rightBitsDetails[indexPath.row].containItens == 1 && (self.rightBitsDetails[indexPath.row].description == "" || self.rightBitsDetails[indexPath.row].description == nil)) || (self.rightBitsDetails[indexPath.row].externalLink != nil && (self.rightBitsDetails[indexPath.row].description == "" || self.rightBitsDetails[indexPath.row].description == nil)) {
                    print("Arrow with Action")
                    cell.delegate = self
                }
                
                // description with ( contain or external ) -> more with action & arrow without
                if (self.rightBitsDetails[indexPath.row].containItens == 1 && self.rightBitsDetails[indexPath.row].description != "" && self.rightBitsDetails[indexPath.row].description != nil) || (self.rightBitsDetails[indexPath.row].externalLink != nil && self.rightBitsDetails[indexPath.row].description != "" && self.rightBitsDetails[indexPath.row].description != nil) {
                    print("Arrow with more Action")
                    cell.arrowButton.isHidden = true
                    cell.delegate = self
                }
                
                cell.moreButton.tag = indexPath.row
                //                cell.arrowButton.tag = indexPath.row
                cell.imageButton.tag = indexPath.row
                //                cell.delegate = self
                cell.tag = 1
                
                
                if let attr1: String = self.rightBitsDetails[indexPath.row].attribue1 {
                    cell.titleLabel.text = attr1
                }
                
                if let attr2: String = rightBitsDetails[indexPath.row].attribue2 {
                    cell.valueLabel.text = attr2
                }
                
                if let image: String = rightBitsDetails[indexPath.row].image {
                    cell.cellImageView.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + image)
                }
                
                //                cell.descriptionLabel.text = "Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah"
                if let description: String = rightBitsDetails[indexPath.row].description {
                    cell.descriptionLabel.text = description
                }
                
                if let bitExternalLink = rightBitsDetails[indexPath.row].externalLink {
                    cell.externalLink = bitExternalLink
                }
                
                if let bitContainItems = rightBitsDetails[indexPath.row].containItens {
                    cell.containItems = bitContainItems
                }
                
                if "Lang".localized == "en" {
                    cell.arrowImageView.image = UIImage(named: "right arrow")
                } else if "Lang".localized == "ar" {
                    cell.arrowImageView.image = UIImage(named: "left arrow")
                }
//                cell.moreButton.isHidden = true
                print("s1")
                
                if indexPath == currentlySelectedRow {
                    if cell.descriptionLabel.text?.isEmpty == true {
                        print("s2")
                        if "Lang".localized == "en" {
                            print("s21")
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            print("s22")
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
//                        cell.moreButton.isHidden = true
                    } else {
                        print("s3")
                        if selectedTableView == 1 {
                            print("s30")
                            cell.arrowImageView.image = UIImage(named: "down")
                        }
                        if cell.externalLink == nil {
                            print("s31")
//                            cell.moreButton.isHidden = true
                        } else {
                            print("s32")
//                            cell.moreButton.isHidden = false
                        }
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == false {
                    print("s4")
                    if "Lang".localized == "en" {
                        print("s41")
                        cell.arrowImageView.image = UIImage(named: "right arrow")
                    } else if "Lang".localized == "ar" {
                        print("s42")
                        cell.arrowImageView.image = UIImage(named: "left arrow")
                    }
//                    cell.moreButton.isHidden = true
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == true {
                    if cell.descriptionLabel.text == "" {
                        print("s5")
                        if "Lang".localized == "en" {
                            print("s51")
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            print("s52")
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
//                        cell.moreButton.isHidden = true
                    } else {
                        print("s6")
                        if currentlySelectedRow! == indexPath {
                            if selectedTableView == 1 {
                                print("Selected: \(selectedTableView)")
                                cell.arrowImageView.image = UIImage(named: "down")
                            }
                        }
                        if cell.externalLink == nil {
                            print("s61")
//                            cell.moreButton.isHidden = true
                        } else {
                            print("s62")
//                            cell.moreButton.isHidden = false
                        }
                    }
                }
                
//                if rightBitsDetails[indexPath.row].containItens == 1 {
//                    if "Lang".localized == "en" {
//                        cell.arrowImageView.image = UIImage(named: "right arrow")
//                    } else if "Lang".localized == "ar" {
//                        cell.arrowImageView.image = UIImage(named: "left arrow")
//                    }
//                }
                
                return cell
            }
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 0 {
//            tableView.deselectRow(at: indexPath, animated: true)
                        
            self.selectedTableView = 0
            
            lastSelectedRow = currentlySelectedRow  // previous indexpath
            currentlySelectedRow = indexPath    // current indexpath
            
            if selectedIndex == indexPath.row {
                if self.isCollapsed == false {
                    self.isCollapsed = true
                } else if self.isCollapsed == true {
                    self.isCollapsed = false
                }
            } else {
                self.isCollapsed = true
            }
            self.selectedIndex = indexPath.row
            tableView.reloadRows(at: [indexPath,(lastSelectedRow ?? indexPath)], with: .automatic)  // Update current & previous indexpath
            rightTableView.reloadData()
        } else if tableView.tag == 1 {
//            tableView.deselectRow(at: indexPath, animated: true)

            self.selectedTableView = 1
                        
            lastSelectedRow = currentlySelectedRow  // previous indexpath
            currentlySelectedRow = indexPath    // current indexpath

            if selectedIndex == indexPath.row {
                if self.isCollapsed == false {
                    self.isCollapsed = true
                } else if self.isCollapsed == true {
                    self.isCollapsed = false
                }
            } else {
                self.isCollapsed = true
            }
            self.selectedIndex = indexPath.row
            tableView.reloadRows(at: [indexPath,(lastSelectedRow ?? indexPath)], with: .automatic)  // Update current & previous indexpath
            leftTableView.reloadData()
        }
        
//        if let extraHeight = self.extraHeight {
//            extraDistance(distance: extraHeight)
//        }
        
    }
    
}
