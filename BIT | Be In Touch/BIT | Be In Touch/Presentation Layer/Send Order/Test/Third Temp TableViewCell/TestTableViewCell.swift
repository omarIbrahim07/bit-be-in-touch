//
//  TestTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 9/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol SecondThirdCellTableViewCellDelegate {
    func secondLeftTableViewMoreButtonPressed(tag: Int)
    func secondRightTableViewMoreButtonPressed(tag: Int)
    func secondLeftTableViewrightArrowButtonPressed(symbol: String)
    func secondRightTableViewrightArrowButtonPressed(symbol: String)
    func secondLeftTableViewitemImageButtonPressed(tag: Int)
    func secondRightTableViewitemImageButtonPressed(tag: Int)
    func extraDistance(distance: Float)
}

class TestTableViewCell: UITableViewCell {
        
    var bitsDetails: [BITDetails] = [BITDetails]()
    var selectedIndex = -1
    var isCollapsed = false
    var currentlySelectedRow: IndexPath?
    var lastSelectedRow: IndexPath?
    var localization: String?
    
    var delegate: SecondThirdCellTableViewCellDelegate?
    
    var leftBitsDetails: [BITDetails] = [BITDetails]()
    var rightBitsDetails: [BITDetails] = [BITDetails]()
    
    var extraHeight: Float?
    var selectedTableView: Int?
    
    @IBOutlet weak var rightTableView: UITableView!
    @IBOutlet weak var leftTableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureTableViews()
        self.selectionStyle = .none
    }
    
    func configureTableViews() {
        rightTableView.register(UINib(nibName: "Test2TableViewCell", bundle: nil), forCellReuseIdentifier: "Test2TableViewCell")
        rightTableView.delegate = self
        rightTableView.dataSource = self
        leftTableView.register(UINib(nibName: "Test2TableViewCell", bundle: nil), forCellReuseIdentifier: "Test2TableViewCell")
        leftTableView.delegate = self
        leftTableView.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func extraDistance(distance: Float) {
        if let delegateValue = delegate {
            delegateValue.extraDistance(distance: distance)
        }
    }
    
    func secondLeftTableViewrightArrowButtonPressed(symbol: String) {
        if let delegateValue = delegate {
            delegateValue.secondLeftTableViewrightArrowButtonPressed(symbol: symbol)
        }
    }
    
    func secondRightTableViewrightArrowButtonPressed(symbol: String) {
        if let delegateValue = delegate {
            delegateValue.secondRightTableViewrightArrowButtonPressed(symbol: symbol)
        }
    }
    
    func secondLeftTableViewMoreButtonPressed(tag: Int) {
        if let delegateValue = delegate {
            delegateValue.secondLeftTableViewMoreButtonPressed(tag: tag)
        }
    }
    
    func secondRightTableViewMoreButtonPressed(tag: Int) {
        if let delegateValue = delegate {
            delegateValue.secondRightTableViewMoreButtonPressed(tag: tag)
        }
    }

    
    func secondLeftTableViewitemImageButtonPressed(tag: Int) {
    if let delegateValue = delegate {
            delegateValue.secondLeftTableViewitemImageButtonPressed(tag: tag)
        }
    }

    func secondRightTableViewitemImageButtonPressed(tag: Int) {
    if let delegateValue = delegate {
            delegateValue.secondRightTableViewitemImageButtonPressed(tag: tag)
        }
    }
    
}
