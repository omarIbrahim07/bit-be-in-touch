//
//  Test2TableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 9/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol ThirdCellTableViewCellDelegate {
    func leftTableViewMoreButtonPressed(tag: Int)
    func rightTableViewMoreButtonPressed(tag: Int)
    func leftTableViewrightArrowButtonPressed(symbol: String)
    func rightTableViewrightArrowButtonPressed(symbol: String)
    func leftTableViewitemImageButtonPressed(tag: Int)
    func rightTableViewitemImageButtonPressed(tag: Int)
}

class Test2TableViewCell: UITableViewCell {

    var delegate: ThirdCellTableViewCellDelegate?
    var externalLink: String?
    var containItems: Int?
    
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var expendableImageView: UIImageView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var arrowImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var arrowButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func leftTableViewrightArrowButtonPressed(symbol: String) {
        if let delegateValue = delegate {
            delegateValue.leftTableViewrightArrowButtonPressed(symbol: symbol)
        }
    }
    
    func rightTableViewrightArrowButtonPressed(symbol: String) {
        if let delegateValue = delegate {
            delegateValue.rightTableViewrightArrowButtonPressed(symbol: symbol)
        }
    }
    
    func leftTableViewMoreButtonPressed(tag: Int) {
        if let delegateValue = delegate {
            delegateValue.leftTableViewMoreButtonPressed(tag: tag)
        }
    }
    
    func rightTableViewMoreButtonPressed(tag: Int) {
        if let delegateValue = delegate {
            delegateValue.rightTableViewMoreButtonPressed(tag: tag)
        }
    }

    
    func leftTableViewitemImageButtonPressed(tag: Int) {
    if let delegateValue = delegate {
            delegateValue.leftTableViewitemImageButtonPressed(tag: tag)
        }
    }

    func rightTableViewitemImageButtonPressed(tag: Int) {
    if let delegateValue = delegate {
            delegateValue.rightTableViewitemImageButtonPressed(tag: tag)
        }
    }
    
    @IBAction func arrowButtonIsPressed(_ sender: Any) {
        if self.tag == 0 {
            print("mesa2 el5eer 1")
            leftTableViewrightArrowButtonPressed(symbol: valueLabel.text ?? "")
            leftTableViewMoreButtonPressed(tag: moreButton.tag)
        } else if self.tag == 1 {
            print("mesa2 el5eer 2")
            rightTableViewrightArrowButtonPressed(symbol: valueLabel.text ?? "")
            rightTableViewMoreButtonPressed(tag: moreButton.tag)
        }
    }

    @IBAction func moreButtonIsPressed(_ sender: Any) {
        if self.tag == 0 {
            print("mesa2 el5eer 3")
            leftTableViewrightArrowButtonPressed(symbol: valueLabel.text ?? "")
            leftTableViewMoreButtonPressed(tag: moreButton.tag)
        } else if self.tag == 1 {
            print("mesa2 el5eer 4")
            rightTableViewrightArrowButtonPressed(symbol: valueLabel.text ?? "")
            rightTableViewMoreButtonPressed(tag: moreButton.tag)
        }
    }
    
    @IBAction func imageButtonIsPressed(_ sender: Any) {
        if self.tag == 0 {
            print("mesa2 el5eer 5")
            leftTableViewitemImageButtonPressed(tag: imageButton.tag)
        } else if self.tag == 1 {
            print("mesa2 el5eer 6")
            rightTableViewitemImageButtonPressed(tag: imageButton.tag)
        }
    }

}
