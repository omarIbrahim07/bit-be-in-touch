//
//  TestViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 9/23/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class TestViewController: BaseViewController {
    
    @IBOutlet weak var containerTableView: UITableView!
    
    var itemsCount: Int? = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup aufter loading the view.
        containerTableView.register(UINib(nibName: "TestTableViewCell", bundle: nil), forCellReuseIdentifier: "TestTableViewCell")
        containerTableView.delegate = self
        containerTableView.dataSource = self
    }
        
     // MARK: - Navigation
    
}

extension TestViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return self.view.frame.height
        return CGFloat((itemsCount ?? 0) * 145)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: TestTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TestTableViewCell") as? TestTableViewCell {
            
            
            
            return cell
        }
        
        return UITableViewCell()
    }
    
}
