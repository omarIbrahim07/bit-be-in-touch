//
//  CountriesCollectionViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 4/24/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class CountriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var countryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        countryView.addCornerRadius(raduis: 10.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 2)
    }

}
