//
//  DetailsTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 4/24/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class DetailsTableViewCell: UITableViewCell {
    
    enum BIT: String {
        case currencyRates
        case Prayer
        case empty
    }
    
    var bit: BIT?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
        
    func to12(time: String) -> String {

            let dateFormatter = DateFormatter()
        
            if "Lang".localized == "en" {
                dateFormatter.locale = NSLocale(localeIdentifier: "en") as Locale
            } else if "Lang".localized == "ar" {
                dateFormatter.locale = NSLocale(localeIdentifier: "ar") as Locale
            }

            dateFormatter.dateFormat = "H:mm"
            let date12 = dateFormatter.date(from: time)!

            dateFormatter.dateFormat = "h:mm a"

            let date22 = dateFormatter.string(from: date12)

            print(date22)

            print("output \(time)")
        return date22
    }

    func bindPrayerTimings(prayerTimings: PrayerTimings) {
        if self.tag == 0 {
            self.titleLabel.text = "fajr".localized
            if let fajr = prayerTimings.fajr {
                let fajr12 = to12(time: fajr)
                self.amountLabel.text = fajr12
//                if "Lang".localized == "en" {
//                    self.amountLabel.text = fajr12.withWesternNumbers
//                } else if "Lang".localized == "ar" {
//                    self.amountLabel.text = fajr12.withArabicNumbers
//                }
            }
        } else if self.tag == 1 {
            self.titleLabel.text = "sunrise".localized
            if let sunrise = prayerTimings.sunRise {
                let sunrise12 = to12(time: sunrise)
                self.amountLabel.text = sunrise12
            }
        } else if self.tag == 2 {
            self.titleLabel.text = "duhur".localized
            if let dhuhr = prayerTimings.dhuhr {
                let duhur12 = to12(time: dhuhr)
                self.amountLabel.text = duhur12
            }
        } else if self.tag == 3 {
            self.titleLabel.text = "asr".localized
            if let asr = prayerTimings.asr {
                let asr12 = to12(time: asr)
                self.amountLabel.text = asr12
            }
        } else if self.tag == 4 {
            self.titleLabel.text = "maghrib".localized
            if let maghrib = prayerTimings.maghrib {
                let maghrib12 = to12(time: maghrib)
                self.amountLabel.text = maghrib12
            }
        } else if self.tag == 5 {
            self.titleLabel.text = "isha".localized
            if let isha = prayerTimings.isha {
                let isha12 = to12(time: isha)
                self.amountLabel.text = isha12
            }
        }
    }

    func bindCurrencyRates(currencyRates: CurrencyRates) {
        if self.tag == 0 {
            self.titleLabel.text = "USD"
            if let usd = currencyRates.usd {
                self.amountLabel.text = String(1 / usd)
            }
        } else if self.tag == 1 {
            self.titleLabel.text = "EUR"
            if let eur = currencyRates.eur {
                self.amountLabel.text = String(1 / eur)
            }
        } else if self.tag == 2 {
            self.titleLabel.text = "SAR"
            if let sar = currencyRates.sar {
                self.amountLabel.text = String(1 / sar)
            }
        } else if self.tag == 3 {
            self.titleLabel.text = "GBP"
            if let gbp = currencyRates.gbp {
                self.amountLabel.text = String(1 / gbp)
            }
        } else if self.tag == 4 {
            self.titleLabel.text = "KWD"
            if let kwd = currencyRates.kwd {
                self.amountLabel.text = String(1 / kwd)
            }
        } else if self.tag == 5 {
            self.titleLabel.text = "AED"
            if let aed = currencyRates.aed {
                self.amountLabel.text = String(1 / aed)
            }
        } else if self.tag == 6 {
            self.titleLabel.text = "CNY"
            if let cny = currencyRates.cny {
                self.amountLabel.text = String(1 / cny)
            }
        }
    }
    

    func bindWeather(weather: WeatherJSON) {
        if self.tag == 0 {
            self.titleLabel.text = "weather".localized
            if let weatherr = weather.weather {
                if let weatherArr: Weather = weatherr[0] {
                    if let weatherState = weatherArr.main {
                        self.amountLabel.text = weatherState
                    }
                }
            }
        } else if self.tag == 1 {
            self.titleLabel.text = "temperature".localized
            if let main = weather.main {
                if let temp = main.temp {
                    let tempInDegree = temp - 272.5
                    self.amountLabel.textColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
                    self.amountLabel.text = String(tempInDegree) + "°"
                }
            }
        } else if self.tag == 2 {
            self.titleLabel.text = "pressure".localized
            if let main = weather.main {
                if let pressure = main.pressure {
                    self.amountLabel.text = String(pressure)
                }
            }
        } else if self.tag == 3 {
            self.titleLabel.text = "wind speed".localized
            if let wind = weather.wind {
                if let windSpeed = wind.speed {
                    self.amountLabel.textColor = UIColor.red
                    self.amountLabel.text = String(windSpeed)
                }
            }
        } else if self.tag == 4 {
            self.titleLabel.text = "humidity".localized
            if let main = weather.main {
                if let humidity = main.humidity {
                    self.amountLabel.text = String(humidity)
                }
            }
        }
    }



    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
