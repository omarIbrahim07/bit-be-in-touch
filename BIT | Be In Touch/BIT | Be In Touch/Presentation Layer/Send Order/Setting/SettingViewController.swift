//
//  SettingViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/31/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {
    
    var settingsArray: [String] = ["App Language", "Terms & Conditions", "Notifications"]
    var arabicSettingsArray: [String] = ["لغة التطبيق", "الشروط والأحكام", "الإشعارات"]
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView

    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var guestLabel: UILabel!
    @IBOutlet weak var rightArrowImageView: UIImageView!
    @IBOutlet weak var userViewHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
    }

    func configureView() {
        self.userViewHeightConstraint.constant = 0
        settingsLabel.text = "settings title label".localized
        guestLabel.text = "guest label".localized
        
        if "Lang".localized == "en" {
            rightArrowImageView.image = UIImage(named: "Right arrow")
        } else if "Lang".localized == "ar" {
            rightArrowImageView.image = UIImage(named: "Arrow")
        }
        
//        reusableView.frame = self.viewContainToolBar.bounds
//        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "SettingTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }

    // MARK: - Navigation
    func goToNotifications() {
        if let notificationsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController {
//            setWidgetVC.bit = bit
//            self.present(notificationsVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(notificationsVC, animated: true)
        }
    }
    
    func goToChangeLanguage() {
        if let changeLanguageVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ChangeLanguageViewController") as? ChangeLanguageViewController {
//            setWidgetVC.bit = bit
            self.present(changeLanguageVC, animated: true, completion: nil)
        }
    }
    
    func goToTermsAndConditions() {
        if let termsAndConditionsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as? TermsAndConditionsViewController {
            //            setWidgetVC.bit = bit
            self.present(termsAndConditionsVC, animated: true, completion: nil)
        }
    }

}

