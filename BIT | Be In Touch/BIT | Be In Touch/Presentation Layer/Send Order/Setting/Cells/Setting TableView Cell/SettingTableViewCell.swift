//
//  SettingTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/31/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol SettingButtonTableViewCellDelegate {
    func settingButtonPressed(index: Int)
}

class SettingTableViewCell: UITableViewCell {
    
    var delegate: SettingButtonTableViewCellDelegate?

    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var rightArrowImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if "Lang".localized == "en" {
            rightArrowImageView.image = UIImage(named: "Right arrow")
        } else if "Lang".localized == "ar" {
            rightArrowImageView.image = UIImage(named: "Arrow")
        }
    }
    
    func setSetting(forSetting setting: String) {
        settingLabel.text = setting
    }
    
    func settingButtonPressed(index: Int) {
        if let delegateValue = delegate {
            delegateValue.settingButtonPressed(index: index)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func settinggButtonIsPressed(_ sender: Any) {
        settingButtonPressed(index: settingButton.tag)
    }
    
}
