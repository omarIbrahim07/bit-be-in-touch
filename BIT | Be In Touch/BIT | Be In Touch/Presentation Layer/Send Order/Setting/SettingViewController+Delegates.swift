//
//  SettingViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//
import UIKit

extension SettingViewController: SettingButtonTableViewCellDelegate {
    func settingButtonPressed(index: Int) {
        print(index)
        if index == 0 {
            goToChangeLanguage()
        } else if index == 1 {
            self.goToTermsAndConditions()
        } else if index == 2 {
            goToNotifications()
        }
    }
}
extension SettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: SettingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SettingTableViewCell") as? SettingTableViewCell {
            
            if "Lang".localized == "en" {
                cell.setSetting(forSetting: settingsArray[indexPath.row])
            } else if "Lang".localized == "ar" {
                cell.setSetting(forSetting: arabicSettingsArray[indexPath.row])
            }
            cell.settingButton.tag = indexPath.row
            cell.delegate = self
            
            if indexPath.row == settingsArray.count - 1 {
                cell.bottomView.isHidden = true
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
}

extension SettingViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Discover" {
            self.goToDiscoverPage()
        } else if keyWord == "Search" {
            self.goToSearchPage()
        } else if keyWord == "Home" {
            self.goToHomeBITS()
        } else if keyWord == "Settings" {
//            self.goToSettingsPage()
        } else if keyWord == "Scanner" {
            self.goToScannerPage()
        }
    }
}
