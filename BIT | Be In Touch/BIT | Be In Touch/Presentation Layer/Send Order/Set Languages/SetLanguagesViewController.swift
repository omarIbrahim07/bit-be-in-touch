//
//  SetLanguagesViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

// MARK:- Delegate between two VCS
protocol SendDataDelegate {
    func sendData(text: String)
}

class SetLanguagesViewController: BaseViewController {
    
    //    private var tap: UITapGestureRecognizer!
    
    var searching = false
    var languages: [String] = [String]()
    var searchedLanguages: [String] = [String]()
    
    var timer: Timer?

    var delegate: SendDataDelegate!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var languagesLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        checkOneLanguageCode()
    }
    
    func configureView() {
        languagesLabel.text = "languages title label".localized
        searchBar.placeholder = "language searchbar placeholder".localized
        searchBar.delegate = self
        searchBar.enablesReturnKeyAutomatically = true
        searchBar.searchTextField.clearButtonMode = .never
        closeKeypad()
    }
    
    func checkOneLanguageCode() {
        if self.languages.count == 1 {
            self.delegate.sendData(text: languages[0])
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(action), userInfo: nil, repeats: false)
        }
    }
    
    @objc func action () {
        print("done")
        self.dismiss(animated: true, completion: nil)
    }
    
    func closeKeypad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "LanguageTableViewCell", bundle: nil), forCellReuseIdentifier: "LanguageTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - Navigation
    
}
