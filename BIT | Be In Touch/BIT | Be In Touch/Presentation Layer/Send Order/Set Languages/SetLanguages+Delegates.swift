//
//  SetLanguages+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension SetLanguagesViewController: UISearchBarDelegate {
            
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchBar.showsCancelButton = true
        self.searching = true
        self.searchedLanguages = languages.filter({ (subcatChar) -> Bool in
            guard let text = searchBar.text else { return false }
            return (subcatChar.contains(text.localizedLowercase))
        })
        print(searchedLanguages)
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searching = false
        searchBar.showsCancelButton = false
        searchBar.text = ""
        tableView.reloadData()
    }

}

extension SetLanguagesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching == true {
            return searchedLanguages.count
        } else if searching == false {
            if self.languages.count == 1 {
                return 1
            } else {
                return languages.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: LanguageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell") as? LanguageTableViewCell {
            
            tableView.tableFooterView = UIView()

            if searching {

                let language = searchedLanguages[indexPath.row]
                
                cell.languageLabel.text = language
            } else if !searching {
                let language = languages[indexPath.row]
                
                cell.languageLabel.text = language
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searching {
            let language = self.searchedLanguages[indexPath.row]
            self.delegate.sendData(text: language)
            self.dismiss(animated: true, completion: nil)
        } else if !searching {
            let language = self.languages[indexPath.row]
            self.delegate.sendData(text: language)
            self.dismiss(animated: true, completion: nil)
        }
    }
}

//extension SetLanguagesViewController: UIGestureRecognizerDelegate {
//    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
//        if touch.view?.isDescendant(of: self.tableView) == true {
//            return false
//        }
//        return true
//    }
//}
