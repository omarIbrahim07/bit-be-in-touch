//
//  SetCountriesViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

// MARK:- Delegate between two VCS
protocol SendCountriesDelegate {
   func sendCountries(countries: [String])
}

class SetCountriesViewController: BaseViewController {

    var allCountries: [Country] = [Country]()
    var allSettingCountries: [String] = [String]()
    var settingCountries: [String] = [String]()
    var choosedCountries: [Country] = [Country]()
    
    var delegate: SendCountriesDelegate!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countriesLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        print(allCountries)
        print(allSettingCountries)
        print(settingCountries)
        print(choosedCountries)
        if allSettingCountries.count > 0 {
            // MARK:- This code for forcing selecting country if total countries number is 1
            if allSettingCountries.count == 1 {
                bindCountries(countries: allSettingCountries, choosedCountries: allSettingCountries)
                self.delegate.sendCountries(countries: self.allSettingCountries)
                return
            }
            // MARK:- This line for multi countries
            bindCountries(countries: allSettingCountries, choosedCountries: settingCountries)
        }
    }
    
    func configureView() {
        countriesLabel.text = "set Countries title label".localized
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "CountryTableViewCell", bundle: nil), forCellReuseIdentifier: "CountryTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func bindCountries(countries: [String], choosedCountries: [String]) {
        for n in 0...countries.count - 1 {
            self.choosedCountries.append(Country(name: countries[n], selected: false))
        }
        if choosedCountries.count >= 1 {
            for i in 0...choosedCountries.count - 1 {
                for n in 0...countries.count - 1 {
                    if self.choosedCountries[n].name == choosedCountries[i] {
                        self.choosedCountries[n].selected = true
                    }
                }
            }
        }
        
//        self.choosedCountries = self.allCountries
        
//        for t in 0...countries.count - 1 {
////            print(allCountries[t].selected)
//            print(self.choosedCountries[t].selected)
//        }
        self.tableView.reloadData()
    }

    // MARK: - Navigation

}
