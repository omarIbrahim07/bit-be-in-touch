//
//  HomeViewController.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/10/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation
import StackedCollectionView
import SwiftyGif
import SwiftGifOrigin
import Alamofire
import GoogleMobileAds

class HomeViewController: BaseViewController, UIGestureRecognizerDelegate {
            
    var items: [Any] = Itemm.getArray()
//    var items: [Itemm] = [Itemm]()
    
    lazy var viewModel: HomeViewModel = {
        return HomeViewModel()
    }()
    
    var error: APIError?
    var countryModel: [Country] = [Country]()
    var countryFavouriteModel: [Country] = [Country]()
    var homeBITS: [BIT] = [BIT]()
    var index: Int?
    var choosedHomeBit: BIT?
    var adIsViewed: Bool?
    
    var viewWidth: Float?
    
    var homeBitsCalled: Bool? = false
        
    let locationManager = CLLocationManager()
    
    //    var reusableView = Bundle.main.loadNibNamed("ViewContainingPlaySoundUIToolbar", owner: self, options: nil)?.first as! ViewContainingPlaySoundUIToolbar
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
    
    //    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var theView: UIView!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var noBitLabel: UILabel!
    @IBOutlet weak var discoverButton: UIButton!
    @IBOutlet weak var testImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    //    @IBOutlet weak var settingsLabel: UILabel!
    
    func checkAdViewed() {
        if UserDefaults.standard.bool(forKey: "ad is viewed") == true {
//            if let homeBit = self.choosedHomeBit {
//                if let bitCode = homeBit.bitCode {
//                    goToFirstTemp(bitCode: bitCode)
//                }
//            }
            if UserDefaults.standard.string(forKey: "bitID") != "" {
                goToFirstTemp(bitCode: UserDefaults.standard.string(forKey: "bitID") ?? "", sorting2: self.choosedHomeBit?.sorting2, sorting3: self.choosedHomeBit?.sorting3, ads: self.choosedHomeBit?.ads)
            }
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print("Islam Elmarg")
//        checkAdViewed()
        setNavigationTitle()
        configureView()
        configureColours()
        getHomeBits()
        configureCollectionView()
//        self.getBITSOffline()
    
        setupLongGestureRecognizerOnCollection()
        
        print(HomeViewController.getAllObjects)
    }
    
    static var getAllObjects: [BITTest] {
         let defaultObject = BITTest(name: "My Object Name")
         if let objects = UserDefaults.standard.value(forKey: "home bits") as? Data {
            let decoder = JSONDecoder()
            if let objectsDecoded = try? decoder.decode(Array.self, from: objects) as [BITTest] {
               return objectsDecoded
            } else {
               return [defaultObject]
            }
         } else {
            return [defaultObject]
         }
      }

    static func saveAllObjects(allObjects: [BITTest]) {
         let encoder = JSONEncoder()
         if let encoded = try? encoder.encode(allObjects){
            UserDefaults.standard.set(encoded, forKey: "home bits")
         }
        
    }

    private func setupLongGestureRecognizerOnCollection() {
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressedGesture.minimumPressDuration = 0.5
        longPressedGesture.delegate = self
        longPressedGesture.delaysTouchesBegan = true
        collectionView?.addGestureRecognizer(longPressedGesture)
    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if (gestureRecognizer.state != .began) {
            return
        }
        
        let p = gestureRecognizer.location(in: collectionView)
        
        if let indexPath = collectionView?.indexPathForItem(at: p) {
            print("Long press at item: \(indexPath.row)")
            self.index = indexPath.row
            self.setupOptionSheet()
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }
        
    func setNavigationTitle() {
        navigationItem.title = "BIT"
    }
    
    func configureView() {
        noBitLabel.isHidden = true
        discoverButton.isHidden = true
        discoverButton.setTitle("discover label".localized, for: .normal)
        self.navigationItem.title = "BIT title".localized
        discoverButton.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//        reusableView.frame = self.viewContainToolBar.bounds
//        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
    }
    
    func configureColours() {
        noBitLabel.textColor = SamnyColour
    }
    
    func configureCollectionView() {
        collectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func getBITSOffline() {
        
        let parameters: [String : AnyObject] = [
            "device_id" : deviceID as AnyObject,
            "locale" : "Lang".localized as AnyObject,
        ]

//        Alamofire.request(startAppUrl, method: .post, parameters: requestParams , encoding: JSONEncoding.default).responseData{
        Alamofire.request("https://app.beintouch.club/api/v1/backend/bitsfav", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseData {
            response in
            switch response.result {
                case .success(let data):
                    do {
                        let parsedResult = try JSONDecoder().decode( bTest.self, from: data)
                        print("||||||||||||||||")
                        print(parsedResult.bitTestArray)
//                        self.homeBITS = parsedResult.bitTestArray
                        HomeViewController.saveAllObjects(allObjects: parsedResult.bitTestArray!)
                        
                        print("||||||||||||||||")
                        let secondDecoder = JSONDecoder()
                        secondDecoder.keyDecodingStrategy = .convertFromSnakeCase
                    } catch {
                        print(error)
                    }

                case .failure(let error):
                    print((error.localizedDescription))
            }
        }
    }
        
    // MARK:- Networking
    func getHomeBits() {
        
        let parameters: [String : AnyObject] = [
            "device_id" : deviceID as AnyObject,
            "locale" : "Lang".localized as AnyObject,
        ]
        
        weak var weakSelf = self
        
//        startLoading()
        self.animateGif(view: self.view)
        
        BITAPIManager().getHomeBITS(basicDictionary: parameters, onSuccess: { (homeBITS) in
            self.homeBitsCalled = true
            
            self.homeBITS = homeBITS
            
            if HomeViewController.getAllObjects.count != self.homeBITS.count {
                self.getBITSOffline()
            }
        
            self.collectionView.reloadData()
            
            self.stopAnimateGif()

//            self.items = []
//            cellImagesArray = []
//            cellTitlesArray = []
//
//            if homeBITS.count == 0 {
//                self.noBitLabel.isHidden = false
//                self.discoverButton.isHidden = false
//                self.noBitLabel.text = "no bits added".localized
//            }
//
//            if self.homeBITS.count > 0 {
//                for i in 0...homeBITS.count - 1 {
//                    cellTitlesArray.append(homeBITS[i].title ?? "BIT")
//                    cellImagesArray.append(homeBITS[i].image ?? ITEM_DEFAULT_IMAGE)
//                }
//
//                for n in 0...homeBITS.count - 1 {
//                    let item = Itemm()
//                    if "Lang".localized == "en" {
//                        item.name = cellTitlesArray[n]
//                    } else if "Lang".localized == "ar" {
//                        item.name = cellTitlesArray[n]
//                    }
//                    let url = URL(string: BIT_IMAGE_URL + cellImagesArray[n])
//                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
//
//                    if let imag = data {
//                        item.image = UIImage(data: imag)
//                    }
//                    self.items.append(item)
//                }
//
//                //            self.stopLoadingWithSuccess()
//                self.stopAnimateGif()
//
//                self.collectionView.reloadData()
//
//            }
                        
        }) { (error) in
            weakSelf?.stopLoadingWithError(error: error)
            self.stopAnimateGif()
            self.homeBitsCalled = true
            self.getBITSOffline()
            self.collectionView.reloadData()
        }
        
    }
    
    func deleteBIT(bitCode: String) {
        let parameters: [String : AnyObject] = [
            "bit_code" : bitCode as AnyObject,
            "device_id": deviceID as AnyObject,
        ]
        
//        self.startLoading()
        self.animateGif(view: self.view)
        
        BITAPIManager().deleteBIT(basicDictionary: parameters , onSuccess: { (deleted) in
            
            if deleted == true {
                self.goToHomeBITS()
            }
            
            //            self.stopLoadingWithSuccess()
            self.stopAnimateGif()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
            self.stopAnimateGif()
        }
    }
    
    func reportBIT(bitCode: String) {
        
        weak var weakSelf = self
        
        let parameters: [String : AnyObject] = [
            "device_id" : deviceID as AnyObject,
            "bit_code" : bitCode as AnyObject,
        ]
        
//        startLoading()
        self.animateGif(view: self.view)
        
        BITAPIManager().reportBIT(basicDictionary: parameters, onSuccess: { (saved) in
            
            self.showReportPopUp()

            self.stopAnimateGif()
            
        }) { (error) in
            self.showReportPopUp()
            weakSelf?.stopLoadingWithSuccess()
        }
    }
    
    func viewBITAdvertisement(bitCode: String) {
        let parameters: [String : AnyObject] = [
            "bit_code" : bitCode as AnyObject,
            "ad_view": 1 as AnyObject,
        ]
        
//        self.animateGif(view: self.view)
        
        BITAPIManager().viewBITAdvertisement(basicDictionary: parameters , onSuccess: { (saved) in
            
            print(saved)
//            self.stopAnimateGif()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
            self.stopAnimateGif()
        }
    }
        
    func openBITAdvertisement(bitCode: String) {
        let parameters: [String : AnyObject] = [
            "bit_code" : bitCode as AnyObject,
            "ad_open": 1 as AnyObject,
        ]
        
//        self.animateGif(view: self.view)
        
        BITAPIManager().openBITAdvertisement(basicDictionary: parameters , onSuccess: { (saved) in
            
            print(saved)
//            self.stopAnimateGif()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
            self.stopAnimateGif()
        }
    }

    func showReportPopUp() {
        let alertController = UIAlertController(title: "report title".localized, message: "report alert message".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
//        let openAction = UIAlertAction(title: "go to settings button".localized, style: .default) { (action) in
//            self.goToSettings()
//        }
        let cancelAction = UIAlertAction(title: "cancel settings".localized, style: .cancel)
//        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func setupOptionSheet() {
        
        guard let homeBitsCount: Int = self.homeBITS.count, homeBitsCount > 0 else {
            let apiError = APIError()
            apiError.message = "Sorry no internet"
            self.showError(error: apiError)
            return
        }
        
        let sheet = UIAlertController()
        //        let sheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        sheet.addAction(UIAlertAction(title: "bit settings".localized, style: .default, handler: {_ in
            print("BIT Settings")
                        
            if let index = self.index {
                let bit = self.homeBITS[index]
                    self.goToSetWidget(bit: bit)
            }

        }))
        sheet.addAction(UIAlertAction(title: "share bit".localized, style: .default, handler: {_ in
            print("Share BIT")
                                    
            if let index = self.index {
                let bit = self.homeBITS[index]
                if let bitCode = bit.bitCode {
                    guard let bitImage = bit.image else { return }
                    let img: UIImage = UIImage(named:"Default image for BIT")!
                    
                    guard let bitURL: URL = URL(string: bitImage) else { return }
                    
                    let application = UIApplication.shared
                    let appPath = "bit://secretPage?bitCode=\(bitCode)"
                    //                                let appPath = "www.google.com"
                    let websiteUrl = URL(string: "https://egydesigner.com")!
                    
                    let pasteboard = UIPasteboard.general
                    pasteboard.string = appPath
                    let apiError = APIError()
                    apiError.message = "copy".localized
                    self.showError(error: apiError)
                    
                    
                    let activityController = UIActivityViewController(activityItems: [img, NSURL(string: appPath)], applicationActivities: nil)
                    
                    activityController.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.copyToPasteboard, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToVimeo]
                    activityController.popoverPresentationController?.sourceView = self.view
                    self.present(activityController, animated: true, completion: nil)
                }
            }
            
        }))
        sheet.addAction(UIAlertAction(title: "report bit".localized, style: .default, handler: {_ in
            print("Report BIT")
            if let index = self.index {
                let bit = self.homeBITS[index]
                if let bitCode = bit.bitCode {
                    self.reportBIT(bitCode: bitCode)
                }
            }
        }))
        sheet.addAction(UIAlertAction(title: "delete bit".localized, style: .default, handler: {_ in
            if let index = self.index {
                let bit = self.homeBITS[index]
                if let bitCode = bit.bitCode {
                    self.deleteBIT(bitCode: bitCode)
                }
            }
            print("Delete BIT")
        }))
        sheet.addAction(UIAlertAction(title: "cancel bit".localized, style: .cancel, handler: nil))
        self.present(sheet, animated: true, completion: nil)
    }
    
    // MARK:- Navigation
    func goToSetWidget(bit: BIT) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SetWidgetViewController") as! SetWidgetViewController
        viewController.bit = bit
        viewController.bitCode = bit.bitCode
        viewController.deepLinkBitCode = bit.bitCode

        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToTest() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TestViewController") as! TestViewController
        
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToBitDetails(bit: bit) {
    }
        
    func goToFirstTemp(bitCode: String, sorting2: Int?, sorting3: Int?, ads: Int?) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "FirstTempViewController") as! FirstTempViewController
        viewController.bitCode = bitCode
        viewController.sorting2 = sorting2
        viewController.sorting3 = sorting3
        viewController.ads = ads
        
        navigationController?.pushViewController(viewController, animated: true)
        UserDefaults.standard.set(false, forKey: "ad is viewed")
//        self.present(viewController, animated: true, completion: nil)
    }
    
    func goToChangeWidth() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ChangeWidgetsViewController") as! ChangeWidgetsViewController
        
        navigationController?.pushViewController(viewController, animated: true)
    }
            
    @IBAction func discoverButtonIsPressed(_ sender: Any) {
        print("Discover")
        self.goToDiscoverPage()
    }
    
}

// MARK:- Operate on internal json file

//    let collectionView: UICollectionView = {
//        let flowLayout = CustomFlowLayout()
//        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
//        collectionView.translatesAutoresizingMaskIntoConstraints = false
//        collectionView.backgroundColor = UIColor.clear
//        return collectionView
//    }()

//    func besmAllah() {
        
//        collectionView.dataSource = self
//        collectionView.delegate = self
//        collectionView.stackedDelegate = self
//        collectionView.stackedDataSource = self
//        collectionView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: CustomCollectionViewCell.identifier)
        
//        theView.addSubview(collectionView)
//        topLayoutGuide.bottomAnchor.constraint(equalTo: collectionView.topAnchor).isActive = true
//        theView.leftAnchor.constraint(equalTo: collectionView.leftAnchor).isActive = true
//        theView.rightAnchor.constraint(equalTo: collectionView.rightAnchor).isActive = true
//        theView.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true
        
        //        let flowLayout = CustomFlowLayout()
        //        collectionView.collectionViewLayout = flowLayout
        //        collectionView.translatesAutoresizingMaskIntoConstraints = false
        //        collectionView.dataSource = self
        //        collectionView.delegate = self
        //        collectionView.stackedDelegate = self
        //        collectionView.stackedDataSource = self
        //        collectionView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: CustomCollectionViewCell.identifier)
        //
        //        view.addSubview(collectionView)
        //
        //        topLayoutGuide.bottomAnchor.constraint(equalTo: collectionView.topAnchor).isActive = true
        //        view.leftAnchor.constraint(equalTo: collectionView.leftAnchor).isActive = true
        //        view.rightAnchor.constraint(equalTo: collectionView.rightAnchor).isActive = true
        //        view.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true
//    }


//func loadCountries() {
//    if let data = try? Data(contentsOf: dataFilePath!) {
//        let decoder = PropertyListDecoder()
//
//        do {
//            //            countryModel = try decoder.decode([Country].self, from: data)
//        } catch {
//            print(Error.self)
//        }
//    }
//
//    if countryModel.count == 0 {
//        readLocalJsonFile()
//    }
//}
//
//func saveCountries() {
//    let encoder = PropertyListEncoder()
//
//    do {
//        //            let data = try encoder.encode(self.countryModel)
//        //            try data.write(to: self.dataFilePath!)
//    } catch {
//        print(Error.self)
//    }
//}
//
//// MARK:- Reading internal JSON file in xcode
//private func readLocalJsonFile() {
//
//    if let urlPath = Bundle.main.url(forResource: "data", withExtension: "json") {
//
//        do {
//            let jsonData = try Data(contentsOf: urlPath, options: .mappedIfSafe)
//
//            if let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: AnyObject] {
//
//                if let countryArray = jsonDict["name"] as? [[String: AnyObject]] {
//
//                    for countryDict in countryArray {
//
//                        let newItem = Country()
//
//                        for (key, value) in countryDict {
//
//                            if key == "name_en" {
//                                //                                        newItem.nameEn = value as? String
//                            } else if key == "name_ar" {
//                                //                                        newItem.nameAr = value as? String
//                            } else if key == "en_capital" {
//                                //                                        newItem.capitalEn = value as? String
//                            } else if key == "alpha_2" {
//                                //                                        newItem.alph2 = value as? String
//                            }
//                            //                                    newItem.selected = false
//
//                            //                            print(key, value)
//                        }
//                        self.countryModel.append(newItem)
//                        self.saveCountries()
//                        print("\n")
//                    }
//                    print(self.countryModel.count)
//                }
//            }
//        }
//        catch let jsonError {
//            print(jsonError)
//        }
//    }
//}
//
//
//func filterCountries() {
//    for country in countryModel {
//        if country.selected == true {
//            self.countryFavouriteModel.append(country)
//        }
//    }
//}
