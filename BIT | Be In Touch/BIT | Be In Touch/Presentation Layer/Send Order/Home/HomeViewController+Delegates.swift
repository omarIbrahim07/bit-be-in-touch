//
//  HomeViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/16/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import StackedCollectionView
import SwiftyGif
import GoogleMobileAds

//extension HomeViewController : SwiftyGifDelegate {
//    
//    func gifDidLoop(sender: UIImageView) {
//        print("gifDidLoop")
////        presentSplashScreen()
//    }
//}

//extension HomeViewController: UIGestureRecognizerDelegate {
//
//}

// MARK: UICollectionViewDataSource
//extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return homeBITS.count
////        return items.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CustomCollectionViewCell.identifier, for: indexPath) as! CustomCollectionViewCell
//        if let item = items[indexPath.item] as? Itemm {
//            cell.items = [item]
//        } else if let items = items[indexPath.item] as? [Itemm] {
//            cell.items = items
//        }
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let homeBit = homeBITS[indexPath.row]
////        if let bitCode: String = homeBit.bitCode, let firstCountry: String = homeBit.countries?[0], let bitName: String = homeBit.title, let bitOwner: String = homeBit.ownerName, let image: String = homeBit.image, let localization = homeBit.languages?[0], let tempChoosed = homeBit.defaultTemplate {
////            if homeBit.countries?.count ?? 0 > 0 {
////                self.goToFirstTemp(bitCode: bitCode, firstCountry: firstCountry, countries: homeBit.countries!, bitName: bitName, bitOwner: bitOwner, dynamicLinks: homeBit.dynamicLinks, bitImage: image, bit: homeBit, localization: localization, tempChoosed: tempChoosed)
////            }
////        }
//        if let bitCode: String = homeBit.bitCode {
//            self.goToFirstTemp(bitCode: bitCode)
//        }
//
//
//
////        items = []
//    }
//
//}

// MARK: StackedCollectionViewDelegate

//extension HomeViewController: UICollectionViewDelegate {
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        guard let favouritecountriesCount: Int = self.countryFavouriteModel.count, favouritecountriesCount > 0 else {
//            self.showNoFavouriteCountriesPopUp()
//            return
//        }
//
//        if let item = items[indexPath.item] as? Itemm {
//
//            var bit = BIT.empty
//
//            if item.key == "currencyRates" {
//                bit = .currencyRates
//            } else if item.key == "Prayer" {
//                bit = .Prayer
//            } else if item.key == "Weather" {
//                bit = .Weather
//            } else {
//                bit = .empty
//            }
//
//            switch bit {
//                case .currencyRates:
//                    self.goToBitDetails(bit: .currencyRates)
//                case .Prayer:
//                    self.goToBitDetails(bit: .Prayer)
//                case .empty:
//                    print("empty")
//                case .Weather:
//                    self.goToBitDetails(bit: .Weather)
//            }
//
//            print(item.name)
//        }
//    }
//}

//extension HomeViewController: StackedCollectionViewDelegate {
//
//    //    func collectionViewGestureDidMoveOutsideTriggerRadius(_ collectionView: UICollectionView) {
//    //        self.setupOptionSheet()
//    //    }
//    // MARK:- React as long gesture on collectionview cell
//    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
//        index = indexPath.row
//        self.setupOptionSheet()
//        return false
//    }
//
//    //    func collectionView(_ collectionView: UICollectionView, animationControllerFor indexPath: IndexPath) -> UICollectionViewCellAnimatedTransitioning? {
//    //        return CustomTransitionAnimator()
//    //    }
//
//}
//
// MARK: StackedCollectionViewDataSource

//extension HomeViewController: StackedCollectionViewDataSource {
    
    //    func collectionView(_ collectionView: UICollectionView, canMoveItemAt sourceIndexPath: IndexPath, into stackDestinationIndexPath: IndexPath) -> Bool {
    //        if
    //            items[sourceIndexPath.item] is [Item], // Block moving a stack...
    //            items[stackDestinationIndexPath.item] is Item // ...onto an item.
    //        {
    //            return false
    //        }
    //        return true
    //    }
    
    //    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, into stackDestinationIndexPath: IndexPath) {
    //
    //        print("Add item at index \(sourceIndexPath.item) into index \(stackDestinationIndexPath.item).")
    //
    //        if let sourceItem = items[sourceIndexPath.item] as? Item {
    //
    //            if let destinationItem = items[stackDestinationIndexPath.item] as? Item {
    //
    //                items[stackDestinationIndexPath.item] = [sourceItem, destinationItem]
    //                items.remove(at: sourceIndexPath.item)
    //
    //            } else if let destinationStack = items[stackDestinationIndexPath.item] as? [Item] {
    //
    //                items[stackDestinationIndexPath.item] = destinationStack + [sourceItem]
    //                items.remove(at: sourceIndexPath.item)
    //
    //            }
    //
    //        } else if let sourceStack = items[sourceIndexPath.item] as? [Item] {
    //
    //            if let destinationItem = items[stackDestinationIndexPath.item] as? Item {
    //
    //                items[stackDestinationIndexPath.item] = sourceStack + [destinationItem]
    //                items.remove(at: sourceIndexPath.item)
    //
    //            } else if let destinationStack = items[stackDestinationIndexPath.item] as? [Item] {
    //
    //                items[stackDestinationIndexPath.item] = sourceStack + destinationStack
    //                items.remove(at: sourceIndexPath.item)
    //
    //            }
    //
    //        }
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, finishMovingItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    //        print("Move item from index \(sourceIndexPath.item) to index \(destinationIndexPath.item).")
    //    }
    
//}


// MARK:- CollectionView Delegate
extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return viewModel.numberOfCells
        if self.homeBitsCalled == true {
            if self.homeBITS.count >= 1 {
                return self.homeBITS.count
            } else if HomeViewController.getAllObjects.count >= 1 {
                return HomeViewController.getAllObjects.count
            }
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell: HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as? HomeCollectionViewCell {

            if self.homeBITS.count > 0 {
                if let image = homeBITS[indexPath.row].image {
                    cell.serviceImage.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + image)
                }
                if let title = homeBITS[indexPath.row].title {
                    cell.serviceTitle.text = title
                }
            }
            
            if self.homeBitsCalled == true {
                if self.homeBITS.count == 0 {
                    if HomeViewController.getAllObjects.count > 0 {
                        if let image = HomeViewController.getAllObjects[indexPath.row].image {
                            cell.serviceImage.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + image)
                        }
                        if let title = HomeViewController.getAllObjects[indexPath.row].title {
                            cell.serviceTitle.text = title
                        }
                    }
                }
            }

            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let homeBitsCount: Int = self.homeBITS.count, homeBitsCount > 0 else {
            let apiError = APIError()
            apiError.message = "Sorry no internet"
            showError(error: apiError)
            return
        }
        
        let homeBit = homeBITS[indexPath.row]
        choosedHomeBit = homeBITS[indexPath.row]
        UserDefaults.standard.set(homeBITS[indexPath.row].bitCode, forKey: "bitID")

        if let bitCode = choosedHomeBit?.bitCode {
            self.goToFirstTemp(bitCode: bitCode, sorting2: choosedHomeBit?.sorting2, sorting3: choosedHomeBit?.sorting3, ads: choosedHomeBit?.ads)
        }
        
    }

//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
////        self.viewModel.userPressed(at: indexPath)
//        guard let favouritecountriesCount: Int = self.countryFavouriteModel.count, favouritecountriesCount > 0 else {
//            self.showNoFavouriteCountriesPopUp()
//            return
//        }
//
//        self.goToChangeWidth()
//        return
//
//        var bit = BIT.empty
//
//        if indexPath.row == 0 {
//            bit = .currencyRates
//        } else if indexPath.row == 2 {
//            bit = .Prayer
//        } else if indexPath.row == 3 {
//            bit = .Weather
//        } else {
//            bit = .empty
//        }
//
//        switch bit {
//            case .currencyRates:
//                self.goToBitDetails(bit: .currencyRates)
//            case .Prayer:
//                self.goToBitDetails(bit: .Prayer)
//            case .empty:
//                print("empty")
//            case .Weather:
//                self.goToBitDetails(bit: .Weather)
//        }
//
//        if let selectedServie = self.viewModel.selectedService {
////            self.goToTechnicians(service: selectedServie)
//        }
//
//    }
}

// MARK:- CollectionView Layout delegate
extension HomeViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 300) / 2.0
        let height = width * 1.2
        //        print(width)
        viewWidth = Float(width)
        print(viewWidth)
        print(view.frame.width)
        print("Width: \(width)")
        print("Height: \(width * 1.1)")
        return CGSize (width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 10 , left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 25
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = (collectionView.bounds.width - 100 ) / 3.0
//        let height = width * 1.2
////        print(width)
////        return CGSize (width: width, height: width * 1.1)
//        viewWidth = Float(width)
//        print(viewWidth)
//        return CGSize (width: 80, height: 104)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets.init(top: 10 , left: 35, bottom: 0, right: 35)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 20
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 25
//    }
}

extension HomeViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Discover" {
            self.goToDiscoverPage()
        } else if keyWord == "Search" {
            self.goToSearchPage()
        } else if keyWord == "Home" {
//            self.goToHomeBITS()
        } else if keyWord == "Settings" {
            self.goToSettingsPage()
        } else if keyWord == "Scanner" {
            self.goToScannerPage()
        }
    }
}
