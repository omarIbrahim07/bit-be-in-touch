//
//  HomeViewModel.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/11/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import Toast_Swift

public enum State {
    case loading
    case error
    case populated
    case empty
}

class HomeViewModel {
    
    var selectedService: Service?
    
    private var services: [Service] = [Service]()
    var error: APIError?
    
    var cellViewModels: [HomeCellViewModel] = [HomeCellViewModel]() {
           didSet {
               self.reloadTableViewClosure?()
           }
       }
    
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    var updateSelectedOffer: (()->())?
    var reloadOffersPagerView: (()->())?
    
    // callback for interfaces
    var state: State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var numberOfCells: Int {
         return cellViewModels.count
     }
    
    var offers: [Offer] = [Offer]() {
        didSet {
            self.reloadOffersPagerView?()
        }
    }

    var selectedOffer: Offer? {
        didSet {
            self.updateSelectedOffer?()
        }
    }
    
//    var offerCellViewModels: [OfferCellViewModel] = [OfferCellViewModel]() {
//        didSet {
//            self.reloadOffersPagerView?()
//        }
//    }
    
    func initFetch() {
        state = .loading
        
        ServicesAPIManager().getServices(basicDictionary: [:], onSuccess: { (services) in
            
            self.processFetchedPhoto(services: services)
            self.state = .populated

        }) { (error) in
            print(error)
        }
    }
    
    func fetchOffers() {
        state = .loading
        
        let params: [String : AnyObject] = [:]
        
        OffersAPIManager().getOffers(basicDictionary: params, onSuccess: { (offers) in

        self.processOffers(offers: offers)
            
        self.state = .populated

        }) { (error) in
            self.error = error
            self.state = .error
        }
    }

    
    func createCellViewModel( service: Service ) -> HomeCellViewModel {

        //Wrap a description
        var descTextContainer: [String] = [String]()
        if let camera = service.name {
            descTextContainer.append(camera)
        }
                
        if let description = service.image {
            descTextContainer.append( description )
        }
        
        if "Lang".localized == "ar" {
            return HomeCellViewModel(titleText: service.name!, imageUrl: service.image!)
        } else if "Lang".localized == "en" {
            return HomeCellViewModel(titleText: service.nameEN!, imageUrl: service.image!)
        }
        
        return HomeCellViewModel(titleText: service.name!, imageUrl: service.image!)
    }
    
    func processFetchedPhoto( services: [Service] ) {
        self.services = services // Cache
        var vms = [HomeCellViewModel]()
        for service in services {
            vms.append( createCellViewModel(service: service) )
        }
        self.cellViewModels = vms
    }
    
    func processOffers( offers: [Offer] ) {
        self.offers = offers // Cache

    }
    
    func getOffer() -> Offer {
        return selectedOffer!
    }

    func offerPressed( at indexPath: IndexPath ) {
        let offer = self.offers[indexPath.row]

        self.selectedOffer = offer
    }

    
    var numberOfOffers: Int {
        return self.offers.count
     }
    
    func getCellViewModel( at indexPath: IndexPath ) -> HomeCellViewModel {
        return cellViewModels[indexPath.row]
    }
    
    func userPressed( at indexPath: IndexPath ){
           let service = self.services[indexPath.row]

        self.selectedService = service
    }
    
    func getError() -> APIError {
        return self.error!
    }
    
}
