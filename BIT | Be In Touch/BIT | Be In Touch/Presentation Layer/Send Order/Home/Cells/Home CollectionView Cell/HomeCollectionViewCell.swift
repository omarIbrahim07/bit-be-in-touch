//
//  HomeCollectionViewCell.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 10/13/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var serviceTitle: UILabel!
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var titleViewWidthConstraint: NSLayoutConstraint!
//    @IBOutlet weak var titleWidthConstraint: NSLayoutConstraint!
    
    var photoListCellViewModel : HomeCellViewModel? {
        didSet {
            serviceTitle.text = photoListCellViewModel?.titleText
            if let imageURL: String = ImageURLVegetables + photoListCellViewModel!.imageUrl {
                serviceImage.loadImageFromUrl(imageUrl: imageURL)
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        containerView.addCornerRadius(raduis: 20.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        containerView.roundCorners([.allCorners], radius: 25.0)
    }

}
