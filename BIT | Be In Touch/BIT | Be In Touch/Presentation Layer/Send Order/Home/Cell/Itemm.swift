//
//  Itemm.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/5/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

//var cellTitlesArrayArabic: [String] = ["تحويل العملة", "إحصائيات كورونا", "مواقيت الصلاة", "الطقس"]
//var cellImagesArray: [String] = ["exchange-rate", "virus", "Masjid", "cloudy"]
//var cellTitlesArray: [String] = ["Currency rates", "statistics \n (Covid-19)", "Prayer timings", "Weather"]
//var keys: [String] = ["currencyRates", "C", "Prayer", "Weather"]

var cellTitlesArrayArabic: [String] = []
var cellImagesArray: [String] = []
var cellTitlesArray: [String] = []
var keys: [String] = []

class Itemm {

    var name: String!
    var image: UIImage!
    var key: String!
    
    class func getArray() -> [Itemm] {
        var items = [Itemm]()
        
        if "Lang".localized == "en" {
            for i in 0..<cellImagesArray.count {
                let item = Itemm()
                item.name = cellTitlesArray[i] 
                item.image = UIImage(named: cellImagesArray[i])
//                item.key = keys[i]
                items.append(item)
            }
        }
        
        if "Lang".localized == "ar" {
            for i in 0..<cellImagesArray.count {
                let item = Itemm()
//                item.name = cellTitlesArrayArabic[i]
                item.name = cellTitlesArray[i]
                item.image = UIImage(named: cellImagesArray[i])
//                item.key = keys[i]
                items.append(item)
            }
        }
        
//        if let imageURLs = Bundle.main.urls(forResourcesWithExtension: "png", subdirectory: nil) {
//            for url in imageURLs {
//                let item = Itemm()
//                item.name = url.deletingPathExtension().lastPathComponent
//                item.image = UIImage(contentsOfFile: url.path)
//                items.append(item)
//            }
//        }
        return items
    }
    
}
