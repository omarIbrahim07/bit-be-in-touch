//
//  Country.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 4/26/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct Country {
    var name: String?
    var selected: Bool?
}
