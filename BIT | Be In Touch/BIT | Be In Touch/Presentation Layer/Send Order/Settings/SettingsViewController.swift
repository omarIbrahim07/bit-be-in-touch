////
////  SettingsViewController.swift
////  BIT | Be In Touch
////
////  Created by Omar Ibrahim on 4/24/20.
////  Copyright © 2020 EgyDesigner. All rights reserved.
////
//
//import UIKit
//import MOLH
//
//class SettingsViewController: BaseViewController {
//    
//    var countryModel = [Country]()
//    
//    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var saveButton: UIButton!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Do any additional setup after loading the view.
//        configureView()
//        configureTableView()
//        configureNavigationBar()
//        //        getCountries()
//        //        readLocalJsonFile()
//        loadCountries()
//        print("CountriesCount: \(countryModel.count)")
//    }
//    
//    func saveCountries() {
//        let encoder = PropertyListEncoder()
//        
//        do {
//            let data = try encoder.encode(self.countryModel)
//            try data.write(to: self.dataFilePath!)
//        } catch {
//            print(Error.self)
//        }
//    }
//    
//    func loadCountries() {
//        if let data = try? Data(contentsOf: dataFilePath!) {
//            let decoder = PropertyListDecoder()
//            
//            do {
//                countryModel = try decoder.decode([Country].self, from: data)
//            } catch {
//                print(Error.self)
//            }
//        }
//        
//        if countryModel.count == 0 {
//            readLocalJsonFile()
//        }
//    }
//    
//    // MARK:- Reading internal JSON file in xcode
//    private func readLocalJsonFile() {
//        
//        if let urlPath = Bundle.main.url(forResource: "data", withExtension: "json") {
//            
//            do {
//                let jsonData = try Data(contentsOf: urlPath, options: .mappedIfSafe)
//                
//                if let jsonDict = try JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String: AnyObject] {
//                    
//                    if let countryArray = jsonDict["name"] as? [[String: AnyObject]] {
//                        
//                        for countryDict in countryArray {
//                            
//                            let newItem = Country()
//                            
//                            for (key, value) in countryDict {
//                                
//                                if key == "name_en" {
//                                    newItem.nameEn = value as? String
//                                } else if key == "name_ar" {
//                                    newItem.nameAr = value as? String
//                                } else if key == "en_capital" {
//                                    newItem.capitalEn = value as? String
//                                } else if key == "alpha_2" {
//                                    newItem.alph2 = value as? String
//                                }
//                                newItem.selected = false
//                                
//                                //                            print(key, value)
//                            }
//                            self.countryModel.append(newItem)
//                            self.saveCountries()
//                            print("\n")
//                        }
//                        print(self.countryModel.count)
//                    }
//                }
//            }
//            catch let jsonError {
//                print(jsonError)
//            }
//        }
//    }
//    
//    func configureTableView() {
//        tableView.register(UINib(nibName: "CountryTableViewCell", bundle: nil), forCellReuseIdentifier: "CountryTableViewCell")
//        tableView.delegate = self
//        tableView.dataSource = self
//    }
//    
//    func configureView() {
//        self.navigationItem.title = "settings title".localized
//        self.saveButton.setTitle("save button".localized, for: .normal)
//        saveButton.addCornerRadius(raduis: 8.0, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
//    }
//    
//    func configureNavigationBar() {
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "language button".localized, style: .plain, target: self, action: #selector(report))
//    }
//    
//    @objc fileprivate func report() {
//        print("Report is pressed")
//        changeLanguage()
//    }
//    
//    func changeLanguage() {
//        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//        MOLH.reset()
//        reset()
//        //        languageLabel.text = "Language".localized
//    }
//    
//    func reset() {
//        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//        let stry = UIStoryboard(name: "Main", bundle: nil)
//        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "HomeNavigationVC")
//    }
//    
//    
//    // MARK: - Actions
//    @IBAction func saveButtonIsPressed(_ sender: Any) {
//        saveCountries()
//        reset()
//    }
//    
//}
//
//extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return countryModel.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if let cell: CountryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CountryTableViewCell") as? CountryTableViewCell {
//            
//            if "Lang".localized == "ar" {
//                if let countryNameAr: String = countryModel[indexPath.row].nameAr {
//                    cell.countryNameTitle.text = countryNameAr
//                }
//            } else if "Lang".localized == "en" {
//                if let countryNameEn: String = countryModel[indexPath.row].nameEn {
//                    cell.countryNameTitle.text = countryNameEn
//                }
//            }
//            
//            cell.tintColor = #colorLiteral(red: 0.2980392157, green: 0.6862745098, blue: 0.3137254902, alpha: 1)
//            cell.accessoryType = countryModel[indexPath.row].selected == true ? .checkmark : .none
//            
//            return cell
//        }
//        return UITableViewCell()
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        
//        // we can replace this code by only one code
//        countryModel[indexPath.row].selected = !countryModel[indexPath.row].selected!
//        
//        //        self.saveCountries()
//        
//        tableView.reloadData()
//        
//        // add checkmark to selected cell
//        // This code is commented after item DataModel is added
//        //        if tableView.cellForRow(at: indexPath)?.accessoryType == .checkmark {
//        //            tableView.cellForRow(at: indexPath)?.accessoryType = .none
//        //        } else {w
//        //            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
//        //        }
//        
//        
//        tableView.deselectRow(at: indexPath, animated: true)
//    }
//    
//}
