//
//  ScannerViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerViewController: BaseViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView
    
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var scannerContainerView: UIView!
    @IBOutlet weak var scannerLabelView: UIView!
    @IBOutlet weak var scannerLabel: UILabel!
    
    func configureView() {
        scannerLabel.text = "scanner setup label".localized
//        reusableView.frame = self.viewContainToolBar.bounds
//        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        view.layer.addSublayer(previewLayer)
        view.bringSubviewToFront(viewContainToolBar)
        view.bringSubviewToFront(scannerLabelView)
//        self.scannerContainerView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        //        captureSession.stopRunning()
        
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            showQRcodePopUp(Qrcode: stringValue)
            captureSession.stopRunning()
        }
    }
    
//    func found(code: String) {
//        print(code)
//        showQRcodePopUp(Qrcode: code)
//    }
    
    //    override var prefersStatusBarHidden: Bool {
    //        return true
    //    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func showQRcodePopUp(Qrcode: String) {
    self.scannerLabel.isHidden = true   
        let alertController = UIAlertController(title: "Code captured!".localized, message: "go to search with bit code".localized, preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "إلغاء", style: .cancel, handler: nil)
        //        alertController.addAction(cancelAction)
        let okayAction = UIAlertAction(title: "Okay".localized, style: .default) { (action) in
            self.goToSearchWithQRCode(Qrcode: Qrcode)
        }
        let retakeAction = UIAlertAction(title: "Retake".localized, style: .default) { (action) in
            self.captureSession.startRunning()
        }
        alertController.addAction(okayAction)
        alertController.addAction(retakeAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    func goToSearchWithQRCode(Qrcode: String) {
        if let searchVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController {
            searchVC.QRcode = Qrcode
//            self.present(searchVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
    
}
