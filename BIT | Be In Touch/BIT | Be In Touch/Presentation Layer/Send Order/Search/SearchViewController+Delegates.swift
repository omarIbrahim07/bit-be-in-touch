//
//  SearchViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension SearchViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         // your Action According to your textfield
        checkSearchedText()
        self.setTrendingLabel()
        return true
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searched = false
        self.tableView.reloadData()
        self.setTrendingLabel()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For numers
        print(enterBITTextField.text?.count)
        if enterBITTextField.text?.count == 1 {
            searched = false
            self.tableView.reloadData()
            self.setTrendingLabel()
        }
        if textField == enterBITTextField {
            let allowedCharacters = CharacterSet(charactersIn:"0123456789")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searched == false {
            return trendingBITs.count
        } else {
            return searchedBIT.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if searched == false {
                if let cell: TrendingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TrendingTableViewCell") as? TrendingTableViewCell {

//                    if indexPath.row == 0 {
//                        cell.setTrendingCell(forID: "3847234", forBIT: "Gold rates")
//                    } else if indexPath.row == 1 {
//                        cell.setTrendingCell(forID: "2983736", forBIT: "Silver rates")
//                    } else if indexPath.row == 2 {
//                        cell.setTrendingCell(forID: "1234567", forBIT: "Prayer")
//                    }
                    let trendingBit = trendingBITs[indexPath.row]
                    
                    if let bitCode = trendingBit.bitCode, let title = trendingBit.title {
                        cell.setTrendingCell(forID: bitCode, forBIT: title)
                    }
                    return cell
                }
            } else {
                if let cell: BITSTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BITSTableViewCell") as? BITSTableViewCell {
                    
                    let bit = searchedBIT[indexPath.row]
                    
                    if let title = bit.title {
                        cell.bitNameLabel.text = title
                    }
                    
                    if let ownerName = bit.ownerName {
                        cell.bitOwnerLabel.text = "By: " + ownerName
                    }
                    
                    if let bitImage = bit.image {
                        cell.bitImageView.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + bitImage)
                    }
                    if let category = bit.category {
                        cell.bitTypeLabel.text = category
                    }

//                    if let bit = self.bit {
////                        if "Lang".localized == "ar" {
////                            if let bitName = bit.name {
////                                cell.bitNameLabel.text = bitName
////                            }
////                        } else if "Lang".localized == "en" {
////                            if let bitName = bit.nameEn {
////                                cell.bitNameLabel.text = bitName
////                            }
////                        }
//                        if let title = bit.title {
//                            cell.bitNameLabel.text = title
//                        }
//
//                        if let ownerName = bit.ownerName {
//                            cell.bitOwnerLabel.text = "By: " + ownerName
//                        }
//                    }

                    return cell
                }
            }
                
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searched == true {
            //            if let bit = self.bit {
            //                self.goToSetWidget(bit: bit)
            //            }
            if self.searchedBIT.count > 0 {
                let bit = self.searchedBIT[0]
                self.goToSetWidget(bit: bit)
            }
        } else if searched == false {
            let bit = self.trendingBITs[indexPath.row]
            self.goToSetWidget(bit: bit)
        }
    }
}

extension SearchViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Discover" {
            self.goToDiscoverPage()
        } else if keyWord == "Search" {
//            self.goToSearchPage()
        } else if keyWord == "Home" {
            self.goToHomeBITS()
        } else if keyWord == "Settings" {
            self.goToSettingsPage()
        } else if keyWord == "Scanner" {
            self.goToScannerPage()
        }
    }
}
