//
//  SearchViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SearchViewController: BaseViewController {
    
    var bit: BIT?
    var searched: Bool? = false
    var QRcode: String?
    
    var searchedBIT: [BIT] = [BIT]()
    var trendingBITs: [BIT] = [BIT]()
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView

    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var searchTitle: UILabel!
    @IBOutlet weak var enterBITTextField: UITextField!
    @IBOutlet weak var trendingLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        closeKeypad()
        configureTableView()
        getTrendingBits()
        checkQRCode()
    }
    
    func checkQRCode() {
        if let qrcode = self.QRcode {
            enterBITTextField.text = qrcode
            self.getSearchedBits(searchText: qrcode)
        }
    }
        
    func configureView() {
        searchTitle.text = "search title".localized
        trendingLabel.text = "trending title label".localized
        enterBITTextField.placeholder = "search textfield placeholder".localized
        
        if "Lang".localized == "en" {
            enterBITTextField.textAlignment = .left
        } else if "Lang".localized == "ar" {
            enterBITTextField.textAlignment = .right
        }
        
//        reusableView.frame = self.viewContainToolBar.bounds
//        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
        enterBITTextField.delegate = self
    }
    
    func setTrendingLabel() {
        if self.searched == false {
            trendingLabel.text = "trending title label".localized
        } else if self.searched == true {
            trendingLabel.text = "Serching Result".localized
        }
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "TrendingTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendingTableViewCell")
        tableView.register(UINib(nibName: "BITSTableViewCell", bundle: nil), forCellReuseIdentifier: "BITSTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 62, right: 0)
    }
    
//    func checkSearchedTextClear() {
//        if self.searched == true {
//            self.tableView.reloadData()
//        }
//    }
    
    func checkSearchedText() {
        guard let textMessage = self.enterBITTextField.text, textMessage.count == 7 else {
            let apiError = APIError()
            apiError.message = "search textfield text is failed".localized
            showError(error: apiError)
            return
        }
        self.getSearchedBits(searchText: textMessage)
    }

    func closeKeypad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    // MARK:- Networking
        func getTrendingBits() {
            self.searchedBIT = []
            
            let parameters: [String : AnyObject] = [
                "locale": "Lang".localized as AnyObject,
            ]
                    
//            self.startLoading()
            self.animateGif(view: self.view)
            
            BITAPIManager().getTrendingBITs(basicDictionary: parameters , onSuccess: { (trendingBits) in
                
//                self.stopLoadingWithSuccess()
                self.stopAnimateGif()
                
                self.trendingBITs = trendingBits
                self.searched = false
                self.tableView.reloadData()
                
            }) { (error) in
                self.stopLoadingWithError(error: error)
            }
        }

    
    func getSearchedBits(searchText: String) {
        self.searchedBIT = []
        
        let parameters: [String : AnyObject] = [
            "bit_codes[]" : searchText as AnyObject,
            "locale": "Lang".localized as AnyObject,
        ]
                
//        self.startLoading()
        self.animateGif(view: self.view)
        
        BITAPIManager().getSearchedBIT(basicDictionary: parameters , onSuccess: { (searchedBIT) in
            
//            self.stopLoadingWithSuccess()
            self.stopAnimateGif()
            
            self.searchedBIT = searchedBIT
            if self.searchedBIT.count > 0 {
                self.searched = true
                self.tableView.reloadData()
                self.setTrendingLabel()
            } else {
                self.trendingLabel.text = "Search result \nPlease check BIT number".localized
                self.searched = true
                self.tableView.reloadData()
            }
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }

        
//        let parameters: [String : AnyObject] = [
//            "bit_code" : searchText as AnyObject,
//            "locale": "Lang".localized as AnyObject,
//            "country": "EG" as AnyObject
//        ]
//
//        self.startLoading()
//
//        BITAPIManager().getBITDetails(basicDictionary: parameters , onSuccess: { (bitDetails) in
//
//            self.stopLoadingWithSuccess()
//
//            self.bit = bitDetails
//            self.searched = true
//            self.tableView.reloadData()
//
//        }) { (error) in
//            self.stopLoadingWithError(error: error)
//        }
    }
    
    // MARK:- Actions
    @IBAction func searchButtonIsPRessed(_ sender: Any) {
        checkSearchedText()
    }
    

    // MARK: - Navigation
    func goToSetWidget(bit: BIT) {
        if let setWidgetVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetWidgetViewController") as? SetWidgetViewController {
            setWidgetVC.bit = bit
//            self.present(setWidgetVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(setWidgetVC, animated: true)
        }
    }

}
