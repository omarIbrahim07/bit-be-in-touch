//
//  TrendingTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class TrendingTableViewCell: UITableViewCell {

    @IBOutlet weak var bitIDLabel: UILabel!
    @IBOutlet weak var trendingBITLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTrendingCell(forID id: String, forBIT bit: String) {
        bitIDLabel.text = id
        trendingBITLabel.text = bit
    }
    
}
