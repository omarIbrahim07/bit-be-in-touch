//
//  FirstTempViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import GoogleMobileAds

class FirstTempViewController: BaseViewController {
    
    // MARK:- 7agat malha4 lazma 2
    var allVegetables: [Vegetable] = [Vegetable]()
    var allVegetablesPrice: [VegetablePrice] = [VegetablePrice]()
    var currencies: [CurrencySymbols] = [CurrencySymbols]()
    var goldRates: [GoldRate] = [GoldRate]()
    var specialSceinario: Int?
    
    var selectedIndex = -1
    var isCollapsed = false
    var countryChoosedIndex = 0
    var lastCountryChoosedIndex: Int?
    var tempChoosed: Int? = 0
    var lastSelectedRow: IndexPath?
    var currentlySelectedRow: IndexPath?
//    var specialSceinario: Int? = 3
    
    var bitCode: String?
    var currentTempLevel: Int?
    var tempIndex: [Int] = [Int]()
    var parentID: Int?
    
    var countries: [String] = [String]()
    var bitsDetails: [BITDetails] = [BITDetails]()
    var leftBitsDetails: [BITDetails] = [BITDetails]()
    var rightBitsDetails: [BITDetails] = [BITDetails]()
    var bitName: String?
    var bitOwner: String?
    var firstCountry: String?
    var dynamicLinks: [DynamicLink] = [DynamicLink]()
    var bitImage: String?
    var bit: BIT?
    var localization: String?
    
    var backToHome: Bool? = true
    var defaultImageBool: Bool? = true
    
    var extraHeight: Float = 0
    
    var sorting2: Int?
    var sorting3: Int?
    var ads: Int?
    
    var countryIndex: Int? = 0
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var updateStackView: UIStackView!
    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var countriesCollectionView: UICollectionView!
    @IBOutlet weak var firstTempImageView: UIImageView!
    @IBOutlet weak var secondTempImageView: UIImageView!
    @IBOutlet weak var thirdTempImageView: UIImageView!
    @IBOutlet weak var fourthTempImageView: UIImageView!
    @IBOutlet weak var bitNameLabel: UILabel!
    @IBOutlet weak var bitOwnerLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var firstTempView: UIView!
    @IBOutlet weak var secondTempView: UIView!
    @IBOutlet weak var thirdTempView: UIView!
    @IBOutlet weak var fourthTempView: UIView!
    @IBOutlet weak var bitImageView: UIImageView!
    @IBOutlet weak var setButton: UIButton!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var dateValueLabel: UILabel!
    @IBOutlet weak var bitNameTitle: UILabel!
    @IBOutlet weak var bitNameValue: UILabel!
    @IBOutlet weak var fiftehTempBitNameTitle: UILabel!
    @IBOutlet weak var fiftehTempBitFirstValue: UILabel!
    @IBOutlet weak var fiftehTempBitSecondValue: UILabel!
    @IBOutlet weak var bottomViewOfColletionView: UIView!
    @IBOutlet weak var countriesCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bitView: UIView!
    @IBOutlet weak var fifthTempView: UIView!
    @IBOutlet weak var priceStackView: UIStackView!
    @IBOutlet weak var priceTitleLabel: UILabel!
    @IBOutlet weak var priceValueLabel: UILabel!
    @IBOutlet weak var bitViewHeight: NSLayoutConstraint!
    @IBOutlet weak var fifthViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sorting2ArrowDownImageView: UIImageView!
    @IBOutlet weak var sorting3ArrowDownImageView: UIImageView!
    @IBOutlet weak var bitViewSorting2ArrowDownImageView: UIImageView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.sorting2ArrowDownImageView.image = UIImage(named: "down")
        self.sorting3ArrowDownImageView.image = UIImage(named: "down")
        self.bitViewSorting2ArrowDownImageView.image = UIImage(named: "down")
        lastSelectedRow = nil
        currentlySelectedRow = nil
        self.countryChoosedIndex = 0
        if self.countries.count > 0 {
            self.countriesCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .right, animated: true)
        }
        self.navigationController?.navigationBar.isHidden = true
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        let indexPath = IndexPath(item: 7, section: 0)
//        self.countriesCollectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .right], animated: true)
//    }
    
    func viewBITAdvertisement(bitCode: String) {
        let parameters: [String : AnyObject] = [
            "bit_code" : bitCode as AnyObject,
            "ad_view": 1 as AnyObject,
        ]
        
        //        self.animateGif(view: self.view)
        
        BITAPIManager().viewBITAdvertisement(basicDictionary: parameters , onSuccess: { (saved) in
            
            print(saved)
            //            self.stopAnimateGif()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
            self.stopAnimateGif()
        }
    }
    
    func openBITAdvertisement(bitCode: String) {
        let parameters: [String : AnyObject] = [
            "bit_code" : bitCode as AnyObject,
            "ad_open": 1 as AnyObject,
        ]
        
        //        self.animateGif(view: self.view)
        
        BITAPIManager().openBITAdvertisement(basicDictionary: parameters , onSuccess: { (saved) in
            
            print(saved)
            //            self.stopAnimateGif()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
            self.stopAnimateGif()
        }
    }
    
    var interstitialAd: GADInterstitial!

    func createInterstitialAd() -> GADInterstitial {
        let ad = GADInterstitial(adUnitID: BITAdId)
        ad.delegate = self
        ad.load(GADRequest())
        return ad
    }

    @objc func didTapButton() {
        if interstitialAd.isReady {
            interstitialAd.present(fromRootViewController: self)
        }
//        if rewardedAd?.isReady == true {
//           rewardedAd?.present(fromRootViewController: self, delegate: self)
//        }
    }
    
    var rewardedAd: GADRewardedAd?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        countriesCollectionView.collectionViewLayout = {
            let flowLayout = UICollectionViewFlowLayout()
            flowLayout.scrollDirection = .horizontal
            return flowLayout
        }()
        
        if ads == 1 {
//            self.interstitialAd = createInterstitialAd()
            
//            rewardedAd = GADRewardedAd(adUnitID: "ca-app-pub-9861973495408000/4433389248")
//            rewardedAd?.load(GADRequest()) { error in
//                if let error = error {
//                    // Handle ad failed to load case.
//                } else {
//                    // Ad successfully loaded.
//                    self.didTapButton()
//                }
//            }
        }

        // Do any additional setup after loading the view.
        configureViews()
        
        if let bitCode = self.bitCode {
            self.getBITSettings(bitCode: bitCode, sorting2: 0, sorting3: 0)
        }
        
//        if let defaultTemp = self.tempChoosed {
//            setTemp(forTempChoosed: defaultTemp)
//        }
        
        if let bitCode = self.bitCode, let firstCountry = self.firstCountry {
            self.getBIT(bitCode: bitCode, firstCountry: firstCountry, parentID: nil, sorting2: nil, sorting3: nil)
        }
        
//        if let specialSceinario = self.specialSceinario {
//            self.getSpecialBIT(specialBIT: specialSceinario)
//        }
        
//        let b = UIButton(frame: CGRect(x: 200, y: 200, width: 20, height: 20))
//        b.backgroundColor = UIColor.red
//        b.setTitle("Name your Button ", for: .normal)
//        b.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
//        self.view.addSubview(b)
    }
        
    func configureViews() {
        
        bitViewSorting2ArrowDownImageView.isHidden = true
        sorting2ArrowDownImageView.isHidden = true
        sorting3ArrowDownImageView.isHidden = true
        headerView.isHidden = true
//        setButton.setTitle("update button".localized, for: .normal)
//        lastUpdateLabel.text = "last update label".localized
//        priceTitleLabel.text = "price title label".localized
//        priceValueLabel.text = "price value label".localized

        bitImageView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        firstTempImageView.addCornerRadius(raduis: firstTempImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        secondTempImageView.addCornerRadius(raduis: secondTempImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        thirdTempImageView.addCornerRadius(raduis: thirdTempImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        fourthTempImageView.addCornerRadius(raduis: fourthTempImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        firstTempView.isHidden = true
        secondTempView.isHidden = true
        thirdTempView.isHidden = true
        fourthTempView.isHidden = true

//        reusableView.frame = self.viewContainToolBar.bounds
//        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
        
        tableView.isHidden = true
        
        tableView.register(UINib(nibName: "TestTableViewCell", bundle: nil), forCellReuseIdentifier: "TestTableViewCell")
        tableView.register(UINib(nibName: "FirstTempTableViewCell", bundle: nil), forCellReuseIdentifier: "FirstTempTableViewCell")
        tableView.register(UINib(nibName: "SecondTempTableViewCell", bundle: nil), forCellReuseIdentifier: "SecondTempTableViewCell")
        tableView.register(UINib(nibName: "FourthTempTableViewCell", bundle: nil), forCellReuseIdentifier: "FourthTempTableViewCell")
        collectionView.register(UINib(nibName: "ThirdTempCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ThirdTempCollectionViewCell")
        tableView.register(UINib(nibName: "FifthTempTableViewCell", bundle: nil), forCellReuseIdentifier: "FifthTempTableViewCell")
        countriesCollectionView.register(UINib(nibName: "CountryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CountryCollectionViewCell")
        tableView.tableFooterView = UIView()
        collectionView.delegate = self
        collectionView.dataSource = self
        countriesCollectionView.delegate = self
        countriesCollectionView.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 178
        tableView.rowHeight = UITableView.automaticDimension
        
        if "Lang".localized == "ar" {
            self.arrowImageView.image = UIImage(named: "Arrow")
        } else if "Lang".localized == "en" {
            self.arrowImageView.image = UIImage(named: "Right arrow")
        }
    }
    
    func updateHeaderView() {
        self.headerView.isHidden = false
        if "Lang".localized == "en" {
            if self.localization?.contains("ع") ?? false || self.localization?.contains("r") ?? false {
                lastUpdateLabel.text = "اخر تحديث"
                priceTitleLabel.text = "كل الأسعار السابقة"
                priceValueLabel.text = "بالعملة  EGP"
                setButton.setTitle("تحديث", for: .normal)
                self.headerView.semanticContentAttribute = .forceRightToLeft
                updateStackView.semanticContentAttribute = .forceRightToLeft
                self.arrowImageView.image = UIImage(named: "Arrow")
            } else {
                lastUpdateLabel.text = "LAST UPDATE"
                priceTitleLabel.text = "ANY PRICES ABOVE"
                priceValueLabel.text = "by EGP currency"
                setButton.setTitle("Update", for: .normal)
                self.arrowImageView.image = UIImage(named: "Right arrow")
            }
        } else if "Lang".localized == "ar" {
            if self.localization?.contains("n") ?? false {
                lastUpdateLabel.text = "LAST UPDATE"
                priceTitleLabel.text = "ANY PRICES ABOVE"
                priceValueLabel.text = "by EGP currency"
                setButton.setTitle("Update", for: .normal)
                self.headerView.semanticContentAttribute = .forceLeftToRight
                updateStackView.semanticContentAttribute = .forceLeftToRight
                self.arrowImageView.image = UIImage(named: "Right arrow")
            } else {
                lastUpdateLabel.text = "اخر تحديث"
                priceTitleLabel.text = "كل الأسعار السابقة"
                priceValueLabel.text = "بالعملة  EGP"
                setButton.setTitle("تحديث", for: .normal)
                self.arrowImageView.image = UIImage(named: "Arrow")
            }
        }
    }
    
    func bindCollectionView(countries: [String]) {
        
        if "Lang".localized == "ar" {
//            self.countriesCollectionView.transform = CGAffineTransform(scaleX: -1,y: 1)
            self.countriesCollectionView.semanticContentAttribute = .forceLeftToRight
        }
        self.countries = []
        for i in 0...countries.count - 1 {
            self.countries.append(countries[i])
        }
        if self.countries.count > 1 {
            self.countriesCollectionView.isHidden = false
            self.countriesCollectionView.reloadData()
        } else {
            self.countriesCollectionView.isHidden = true
            self.bottomViewOfColletionView.isHidden = true
            self.countriesCollectionViewHeightConstraint.constant = 0
        }
//        self.countriesCollectionView.reloadData()
    }
    
    func bindDynamicLinks(dynamicLinks: [DynamicLink]) {
        for i in 0...dynamicLinks.count - 1 {
            if i == 0 {
                if let image = dynamicLinks[i].image {
                    self.firstTempView.isHidden = false
                    self.firstTempImageView.loadImageFromUrl(imageUrl: DYNAMIC_LINKS_IMAGE_URL + image)
                }
            } else if i == 1 {
                if let image = dynamicLinks[i].image {
                    self.secondTempView.isHidden = false
                    self.secondTempImageView.loadImageFromUrl(imageUrl: DYNAMIC_LINKS_IMAGE_URL + image)
                }
            } else if i == 2 {
                if let image = dynamicLinks[i].image {
                    self.thirdTempView.isHidden = false
                    self.thirdTempImageView.loadImageFromUrl(imageUrl: DYNAMIC_LINKS_IMAGE_URL + image)
                }
            } else if i == 3 {
                if let image = dynamicLinks[i].image {
                    self.fourthTempView.isHidden = false
                    self.fourthTempImageView.loadImageFromUrl(imageUrl: DYNAMIC_LINKS_IMAGE_URL + image)
                }
            }
        }
    }
    
    func setTemp(forTempChoosed tempChoosed: Int, firstTitle: String?, secondTitle: String?, thirdTitle: String?) {
        print("Temp Choosed: \(tempChoosed)")
        self.isCollapsed = false
        self.tempChoosed = tempChoosed
        collectionView.isHidden = true
        tableView.isHidden = false
        self.bitViewHeight.constant = 50
        self.fifthViewHeightConstraint.constant = 50
        tableView.reloadData()
        
        if tempChoosed != 6 {
            self.setTwoTitles(title1: firstTitle, title2: secondTitle)
        }
        
        if tempChoosed == 3 || tempChoosed == 2 {
//            self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 10))
//            self.bitView.isHidden = true
//            self.fifthTempView.isHidden = true
            self.bitViewHeight.constant = 5
            self.fifthViewHeightConstraint.constant = 5
            self.tableView.separatorStyle = .none
        }
        
        if tempChoosed == 3 {
//            self.isCollapsed = false
//            self.tempChoosed = tempChoosed
//            collectionView.isHidden = false
//            tableView.isHidden = true
//            collectionView.reloadData()
        } else if tempChoosed == 6 {
            self.bitView.isHidden = true
            self.fifthTempView.isHidden = false
            self.setThreeTitles(title1: firstTitle, title2: secondTitle, title3: thirdTitle)
        }
    }
    
    func setTwoTitles(title1: String?, title2: String?) {
        if sorting2 == 1 {
            self.bitViewSorting2ArrowDownImageView.isHidden = false
            self.bitViewSorting2ArrowDownImageView.image = UIImage(named: "down")
        }
        if let firstTitle = title1 {
            self.bitNameTitle.text = firstTitle
        }
        if let secondTitle = title2 {
            self.bitNameValue.text = secondTitle
        }
    }
    
    func setThreeTitles(title1: String?, title2: String?, title3: String?) {
        if sorting2 == 1 {
            self.sorting2ArrowDownImageView.isHidden = false
            self.bitViewSorting2ArrowDownImageView.isHidden = false
            self.bitViewSorting2ArrowDownImageView.image = UIImage(named: "down")
            self.sorting2ArrowDownImageView.image = UIImage(named: "down")
        }
        if sorting3 == 1 {
            self.sorting3ArrowDownImageView.isHidden = false
            self.sorting3ArrowDownImageView.image = UIImage(named: "down")
        }
        if let firstTitle = title1 {
            self.fiftehTempBitNameTitle.text = firstTitle
        }
        if let secondTitle = title2 {
            self.fiftehTempBitFirstValue.text = secondTitle
        }
        if let thirdTitle = title3 {
            self.fiftehTempBitSecondValue.text = thirdTitle
        }
    }
    
    func goToWebsite(website: String) {
        if let url = URL(string: website),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    // MARK:- Networking
    func getBITSettings(bitCode: String, sorting2: Int, sorting3: Int) {

//        self.startLoading()
        self.animateGif(view: self.view)

        let params: [String : AnyObject] = [
            "bit_code" : bitCode as AnyObject,
            "device_id" : deviceID as AnyObject
        ]

        SettingAPIManager().getBITSettings(basicDictionary: params, onSuccess: { (bitSettings) in
            
            print(self.currentTempLevel)
//            self.bitSettings = bitSettings
            if let language = bitSettings.language {
                self.localization = language
            }
            if let countries = bitSettings.countries {
                self.countries = countries
                self.bindCollectionView(countries: self.countries)
            }
            if let bitName: String = bitSettings.bitName {
                self.bitNameLabel.text = bitName
            }
            if let bitOwner: String = bitSettings.bitOwner {
                if self.localization?.contains("n") ?? false {
                    self.bitOwnerLabel.text = "By: " + bitOwner
                } else if self.localization?.contains("ع") ?? false || self.localization?.contains("r") ?? false {
                    self.bitOwnerLabel.text = "بواسطة: " + bitOwner
                }
//                if "Lang".localized == "en" {
//                    self.bitOwnerLabel.text = "By: " + bitOwner
//                } else if "Lang".localized == "ar" {
//                    self.bitOwnerLabel.text = "بواسطة: " + bitOwner
//                }
            }
            if let bitImage: String = bitSettings.bitImage {
                self.bitImageView.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + bitImage)
            } else {
                self.bitImageView.image = UIImage(named: "Default image for BIT")
            }
            
            if let category: String = bitSettings.category {
                
            }
            
            if let sorting2 = bitSettings.sorting2 {
                self.sorting2 = sorting2
            }
            if let sorting3 = bitSettings.sorting3 {
                self.sorting3 = sorting3
            }

            if let updatedAt: String = bitSettings.updatedAt {
                self.dateValueLabel.text = updatedAt
            }
            
            if bitSettings.dynamicLinks?.count ?? 0 > 0 {
                self.dynamicLinks = bitSettings.dynamicLinks!
                self.bindDynamicLinks(dynamicLinks: self.dynamicLinks)
            }
            
            if self.currentTempLevel == nil {
                self.getBIT(bitCode: bitCode, firstCountry: self.countries[0], parentID: nil, sorting2: nil, sorting3: nil)
                if let firstTemp = bitSettings.firstTemplate {
                    self.setTemp(forTempChoosed: firstTemp, firstTitle: bitSettings.bitLabel11, secondTitle: bitSettings.bitLabel12, thirdTitle: bitSettings.bitLabel13)
                }
            } else if self.currentTempLevel != nil {
                self.getBIT(bitCode: bitCode, firstCountry: self.countries[0], parentID: self.parentID, sorting2: nil, sorting3: nil)
                if self.currentTempLevel == 1 {
                    if let secondTemp = bitSettings.secondTemplate {
                        self.setTemp(forTempChoosed: secondTemp, firstTitle: bitSettings.bitLabel21, secondTitle: bitSettings.bitLabel22, thirdTitle: bitSettings.bitLabel23)
                    }
                } else if self.currentTempLevel == 2 {
                    if let thirdTemp = bitSettings.thirdTemplate {
                        self.setTemp(forTempChoosed: thirdTemp, firstTitle: bitSettings.bitLabel31, secondTitle: bitSettings.bitLabel32, thirdTitle: bitSettings.bitLabel33)
                    }
                }
            }
            
            //            self.stopLoadingWithSuccess()
            self.updateHeaderView()
            self.stopAnimateGif()
            
            self.tableView.reloadData()

        }) { (error) in
            self.headerView.isHidden = false
            self.stopLoadingWithError(error: error)
        }
    }

    
    func getBIT(bitCode: String, firstCountry: String, parentID: Int?, sorting2: String?, sorting3: String?) {
        
        var parameters: [String : AnyObject] = [String : AnyObject]()
        
        if parentID == nil {
            parameters = [
                "bit_code" : bitCode as AnyObject,
                //            "locale": "Lang".localized as AnyObject,
                "locale": self.localization as AnyObject,
                "country": firstCountry as AnyObject,
                "sorting2": sorting2 as AnyObject,
                "sorting3": sorting3 as AnyObject
            ]
        } else {
            parameters = [
                "bit_code" : bitCode as AnyObject,
                "parent_id": parentID as AnyObject,
                "locale": self.localization as AnyObject,
                "country": firstCountry as AnyObject,
                "sorting2": sorting2 as AnyObject,
                "sorting3": sorting3 as AnyObject
            ]
        }
        
//        self.startLoading()
        self.animateGif(view: self.view)
        
//        BITAPIManager().getBITDetails(basicDictionary: parameters , onSuccess: { (bitsDetails, headerData)  in
        BITAPIManager().getBITDetails(basicDictionary: parameters , onSuccess: { (bitsDetails)  in
            
//            print("gggggg \(headerData?.title)")
            self.bitsDetails = []
            self.leftBitsDetails = []
            self.rightBitsDetails = []
//            self.bitNameTitle.text = bitNameTitle
//            self.bitNameValue.text = bitNameValue
//
//            self.fiftehTempBitNameTitle.text = bitNameTitle
//            self.fiftehTempBitFirstValue.text = bitNameValue
//            self.fiftehTempBitSecondValue.text = bitNameValueTwo
            
            self.bitsDetails = bitsDetails
            
            if bitsDetails.count > 0 {
                for i in 0...bitsDetails.count - 1 {
                    if bitsDetails[i].image != ITEM_DEFAULT_IMAGE {
                        self.defaultImageBool = false
                    }
                    //                self.bitsDetails[i].description = "Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm "
                    //                self.bitsDetails[i].description = "Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah"
                }
                
                for i in 0...bitsDetails.count - 1 {
                    if i % 2 == 0 {
                        self.leftBitsDetails.append(bitsDetails[i])
                    } else if i % 2 == 1 {
                        self.rightBitsDetails.append(bitsDetails[i])
                    }
                }
            }
            
            //            self.stopLoadingWithSuccess()
            self.stopAnimateGif()

            self.tableView.reloadData()
            self.collectionView.reloadData()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }

    // MARK: - Navigation
    func goToSetWidget(bitCode: String) {
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        //        let viewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
        let viewController = storyboard.instantiateViewController(withIdentifier: "SetWidgetViewController") as! SetWidgetViewController
        viewController.bitCode = bitCode
        viewController.deepLinkBitCode = bitCode

//        UIApplication.shared.keyWindow?.rootViewController = viewController
        
//        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: "SetWidgetViewController") as! SetWidgetViewController
//
//        viewController.bitCode = bitCode
//        viewController.deepLinkBitCode = bitCode
//
        navigationController?.pushViewController(viewController, animated: true)
//        self.present(viewController, animated: true, completion: nil)
    }
    
    func goToSecondLayerTemp(symbol: String, bitName: String, bitOwner: String, dynamicLinks: [DynamicLink]?, bitImage: String, bit: BIT) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SecondLayerTempViewController") as! SecondLayerTempViewController
        
        viewController.symbol = symbol
        viewController.bitName = bitName
        viewController.bitOwner = bitOwner
        if self.dynamicLinks.count > 0 {
            viewController.dynamicLinks = dynamicLinks!
        }
        viewController.bitImage = bitImage
        viewController.bit = bit
        
        navigationController?.pushViewController(viewController, animated: true)
//        self.present(viewController, animated: true, completion: nil)
    }
    
    func goToImageDetails(imageURL: String) {
        if let imageDetailsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ImageZoomedViewController") as? ImageZoomedViewController {
            imageDetailsVC.imageURL = imageURL
            self.present(imageDetailsVC, animated: true, completion: nil)
        }
    }
    
    func goToTempStage(bitCode: String, parentID: Int, currentTempLevel: Int?) {
        if let firstTempVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstTempViewController") as? FirstTempViewController {
            //            setWidgetVC.bit = bit
            firstTempVC.parentID = parentID
//            firstTempVC.firstCountry = firstCountry
            firstTempVC.bitCode = bitCode
            firstTempVC.currentTempLevel = currentTempLevel
            firstTempVC.backToHome = false
//            self.present(firstTempVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(firstTempVC, animated: true)
        }
    }
    
    // MARK:- Actions
    @IBAction func backButtonIsPressed(_ sender: Any) {
        if self.backToHome == true {
//            self.goToHomeBITS()
            self.navigationController?.popToRootViewController(animated: true)
//            self.dismiss(animated: true, completion: nil)
        } else if self.backToHome == false {
            self.navigationController?.popViewController(animated: true)
//            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func firstTempIsPressed(_ sender: Any) {
        if let website = self.dynamicLinks[0].link {
            goToWebsite(website: website)
        }
//        self.setupOptionSheet()
    }
    
    @IBAction func secondTempIsPressed(_ sender: Any) {
        if let website = self.dynamicLinks[1].link {
            goToWebsite(website: website)
        }
    }
    
    @IBAction func thirdTempIsPressed(_ sender: Any) {
        if let website = self.dynamicLinks[2].link {
            goToWebsite(website: website)
        }
    }
    
    @IBAction func fourthTempIsPressed(_ sender: Any) {
        if let website = self.dynamicLinks[3].link {
            goToWebsite(website: website)
        }
    }
    
    @IBAction func setWidgetButtonIsPressed(_ sender: Any) {
        if let bitCode = self.bitCode {
            self.goToSetWidget(bitCode: bitCode)
        }
    }
    
    @IBAction func updateButtonIsPressed(_ sender: Any) {
        self.viewDidLoad()
        self.viewWillAppear(true)
    }
    
    @IBAction func sorting2ButtonIsPressed(_ sender: Any) {
        print("Sorting 2")
        if let bitCode = self.bitCode, let choosedCountryIndex: Int = self.countryChoosedIndex {
            if sorting2ArrowDownImageView.image == UIImage(named: "down") {
                self.getBIT(bitCode: bitCode, firstCountry: self.countries[choosedCountryIndex], parentID: self.parentID, sorting2: "asc", sorting3: nil)
                self.sorting2ArrowDownImageView.image = UIImage(named: "up")
                self.bitViewSorting2ArrowDownImageView.image = UIImage(named: "up")
            } else if sorting2ArrowDownImageView.image == UIImage(named: "up") {
                self.getBIT(bitCode: bitCode, firstCountry: self.countries[choosedCountryIndex], parentID: self.parentID, sorting2: "desc", sorting3: nil)
                self.sorting2ArrowDownImageView.image = UIImage(named: "down")
                self.bitViewSorting2ArrowDownImageView.image = UIImage(named: "down")
            }
//            self.sorting2ArrowDownImageView.image = UIImage(named: "up")
//            self.bitViewSorting2ArrowDownImageView.image = UIImage(named: "up")
        }
    }
    
    @IBAction func sorting3ButtonIsPressed(_ sender: Any) {
        print("Sorting 3")
        if let bitCode = self.bitCode, let choosedCountryIndex: Int = self.countryChoosedIndex {
            if sorting3 == 1 {
                if sorting3ArrowDownImageView.image == UIImage(named: "up") {
                    self.getBIT(bitCode: bitCode, firstCountry: self.countries[choosedCountryIndex], parentID: self.parentID, sorting2: nil, sorting3: "desc")
                    self.sorting3ArrowDownImageView.image = UIImage(named: "down")
                }
            } else if sorting3ArrowDownImageView.image == UIImage(named: "down") {
                self.getBIT(bitCode: bitCode, firstCountry: self.countries[choosedCountryIndex], parentID: self.parentID, sorting2: nil, sorting3: "asc")
                self.sorting3ArrowDownImageView.image = UIImage(named: "up")
            }
//            self.sorting3ArrowDownImageView.image = UIImage(named: "up")
        }
    }

}
