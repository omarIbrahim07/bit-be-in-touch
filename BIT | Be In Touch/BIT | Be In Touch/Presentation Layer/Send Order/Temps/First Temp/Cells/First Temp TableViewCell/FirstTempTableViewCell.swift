//
//  FirstTempTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol RightArrowButtonTableViewCellDelegate {
    func rightArrowButtonPressed(symbol: String)
}

protocol MoreButtonPressedTableViewCellDelegate {
    func firstTempMoreButtonPressed(tag: Int)
    func secondTempMoreButtonPressed(tag: Int)
    func thirdTempMoreButtonPressed(tag: Int)
    func fourthTempMoreButtonPressed(tag: Int)
}

protocol ItemImageTableViewCellDelegate {
    func itemImageButtonPressed(tag: Int)
}
    
class FirstTempTableViewCell: UITableViewCell {
    
    var delegate: RightArrowButtonTableViewCellDelegate?
    var moreDelegate: MoreButtonPressedTableViewCellDelegate?
    var imageDelegate: ItemImageTableViewCellDelegate?
    var externalLink: String?

    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var lastView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var arrowImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var valueLabelTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var arrowButton: UIButton!
    @IBOutlet weak var cellImageWidthCinstraint: NSLayoutConstraint!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        moreButton.setTitle("more button title".localized, for: .normal)
        if "Lang".localized == "en" {
            arrowImageView.image = UIImage(named: "right arrow")
        } else if "Lang".localized == "ar" {
            arrowImageView.image = UIImage(named: "left arrow")
        }
        self.selectionStyle = .none
    }
    
    func setCell(forTitle title: String, forValue value: String) {
        self.titleLabel.text = title
        self.valueLabel.text = value
    }
    
    func setImage(forImage image: String) {
        self.cellImage.loadImageFromUrl(imageUrl: ImageURLVegetables + image)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func rightArrowButtonPressed(symbol: String) {
        if let delegateValue = delegate {
            delegateValue.rightArrowButtonPressed(symbol: symbol)
        }
    }
    
    func firstTempMoreButtonPressed(tag: Int) {
        if let delegateValue = moreDelegate {
            delegateValue.firstTempMoreButtonPressed(tag: tag)
        }
    }
    
    func itemImageButtonPressed(tag: Int) {
    if let delegateValue = imageDelegate {
            delegateValue.itemImageButtonPressed(tag: tag)
        }
    }

    
    @IBAction func arrowButtonIsPressed(_ sender: Any) {
        rightArrowButtonPressed(symbol: valueLabel.text ?? "")
        firstTempMoreButtonPressed(tag: moreButton.tag)
    }
    
    @IBAction func moreButtonIsPressed(_ sender: Any) {
        rightArrowButtonPressed(symbol: valueLabel.text ?? "")
        firstTempMoreButtonPressed(tag: moreButton.tag)
    }
    
    @IBAction func imageButtonIsPressed(_ sender: Any) {
        itemImageButtonPressed(tag: imageButton.tag)
    }
    
}
