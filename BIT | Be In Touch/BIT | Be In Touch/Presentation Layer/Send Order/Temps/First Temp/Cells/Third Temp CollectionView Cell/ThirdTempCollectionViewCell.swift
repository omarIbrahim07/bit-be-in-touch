//
//  ThirdTempCollectionViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ThirdTempCollectionViewCell: UICollectionViewCell {
    
    var delegate: RightArrowButtonTableViewCellDelegate?
    var moreDelegate: MoreButtonPressedTableViewCellDelegate?
    var imageDelegate: ItemImageTableViewCellDelegate?
    var externalLink: String?
    @IBOutlet weak var arrowImageView: UIImageView!
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var expendableImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var arrowImageWidthConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        moreButton.setTitle("more button title".localized, for: .normal)
    }
    
    func rightArrowButtonPressed(symbol: String) {
        if let delegateValue = delegate {
            delegateValue.rightArrowButtonPressed(symbol: symbol)
        }
    }
    
    func firstTempMoreButtonPressed(tag: Int) {
        if let delegateValue = moreDelegate {
            delegateValue.firstTempMoreButtonPressed(tag: tag)
        }
    }
    
    func itemImageButtonPressed(tag: Int) {
    if let delegateValue = imageDelegate {
            delegateValue.itemImageButtonPressed(tag: tag)
        }
    }
        
    @IBAction func arrowButtonIsPressed(_ sender: Any) {
        rightArrowButtonPressed(symbol: valueLabel.text ?? "")
        firstTempMoreButtonPressed(tag: moreButton.tag)
    }

    @IBAction func moreButtonIsPressed(_ sender: Any) {
        firstTempMoreButtonPressed(tag: moreButton.tag)
    }
    
    @IBAction func imageButtonIsPressed(_ sender: Any) {
        itemImageButtonPressed(tag: imageButton.tag)
    }
}
