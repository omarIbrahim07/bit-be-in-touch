//
//  FifthTempTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/17/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class FifthTempTableViewCell: UITableViewCell {

    @IBOutlet weak var tempImageView: UIImageView!
    @IBOutlet weak var tempTitleLabel: UILabel!
    @IBOutlet weak var buyValueLabel: UILabel!
    @IBOutlet weak var sellValueLabel: UILabel!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var cellImageWidthCinstraint: NSLayoutConstraint!
    
    var imageDelegate: ItemImageTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func itemImageButtonPressed(tag: Int) {
    if let delegateValue = imageDelegate {
            delegateValue.itemImageButtonPressed(tag: tag)
        }
    }
    
    @IBAction func imageButtonIsPressed(_ sender: Any) {
        itemImageButtonPressed(tag: imageButton.tag)
    }

}
