//
//  CountryCollectionViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/15/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class CountryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var choosedView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
