//
//  SecondTempTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/11/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SecondTempTableViewCell: UITableViewCell {

    var moreDelegate: MoreButtonPressedTableViewCellDelegate?
    var delegate: RightArrowButtonTableViewCellDelegate?
    var imageDelegate: ItemImageTableViewCellDelegate?

    var externalLink: String?

    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var arrowImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var valueLabelTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var arrowButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        moreButton.setTitle("more button title".localized, for: .normal)
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func secondTempMoreButtonPressed(tag: Int) {
        if let delegateValue = moreDelegate {
            delegateValue.secondTempMoreButtonPressed(tag: tag)
        }
    }
    
    func rightArrowButtonPressed(symbol: String) {
        if let delegateValue = delegate {
            delegateValue.rightArrowButtonPressed(symbol: symbol)
        }
    }
    
    func itemImageButtonPressed(tag: Int) {
    if let delegateValue = imageDelegate {
            delegateValue.itemImageButtonPressed(tag: tag)
        }
    }
    
    @IBAction func arrowButtonIsPressed(_ sender: Any) {
        print("Basboosa")
        rightArrowButtonPressed(symbol: valueLabel.text ?? "")
        secondTempMoreButtonPressed(tag: moreButton.tag)
    }
    
    @IBAction func moreButtonIsPressed(_ sender: Any) {
        rightArrowButtonPressed(symbol: valueLabel.text ?? "")
        secondTempMoreButtonPressed(tag: moreButton.tag)
    }

    @IBAction func imageButtonIsPressed(_ sender: Any) {
        itemImageButtonPressed(tag: imageButton.tag)
    }
}
