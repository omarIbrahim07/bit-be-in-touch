//
//  FirstTempViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/19/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import GoogleMobileAds

//extension FirstTempViewController: GADInterstitialDelegate {
//    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
//        self.interstitialAd = self.createInterstitialAd()
//    }
//}

extension FirstTempViewController: GADRewardedAdDelegate {
    func rewardedAd(_ rewardedAd: GADRewardedAd, userDidEarn reward: GADAdReward) {
        print("LLL")
    }
    
}

extension FirstTempViewController: GADInterstitialDelegate {
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        self.didTapButton()
    }

    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("H present")
        if let bitCode = self.bitCode {
            self.viewBITAdvertisement(bitCode: bitCode)
        }
    }
    
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
//        self.interstitialAd = self.createInterstitialAd()
    }
    
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("Sba7o")
        print("coda \(self.bitCode)")
        if let bitCode = self.bitCode {
            self.openBITAdvertisement(bitCode: bitCode)
        }
    }

}

extension FirstTempViewController: SecondThirdCellTableViewCellDelegate {
    func extraDistance(distance: Float) {
        self.extraHeight = distance
        self.tableView.reloadData()
    }
    
    func secondLeftTableViewMoreButtonPressed(tag: Int) {
        if leftBitsDetails[tag].containItens == 1 {
            if let parentID = leftBitsDetails[tag].id, let bitCode = self.bitCode {
                if self.currentTempLevel == nil {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 1)
                } else if self.currentTempLevel == 1 {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 2)
                }
            }
        } else if leftBitsDetails[tag].externalLink != nil {
            goToWebsite(website: leftBitsDetails[tag].externalLink!)
        }
    }
    
    func secondRightTableViewMoreButtonPressed(tag: Int) {
        if self.rightBitsDetails[tag].containItens == 1 {
            if let parentID = rightBitsDetails[tag].id, let bitCode = self.bitCode {
                if self.currentTempLevel == nil {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 1)
                } else if self.currentTempLevel == 1 {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 2)
                }
            }
        } else if rightBitsDetails[tag].externalLink != nil {
            goToWebsite(website: rightBitsDetails[tag].externalLink!)
        }
    }

    func secondLeftTableViewrightArrowButtonPressed(symbol: String) {
        if let bitName = self.bitName, let bitOwner = self.bitOwner, let bitImage = self.bitImage, let bit = self.bit {
            self.goToSecondLayerTemp(symbol: symbol, bitName: bitName, bitOwner: bitOwner, dynamicLinks: self.dynamicLinks, bitImage: bitImage, bit: bit)
        }
    }
    
    func secondRightTableViewrightArrowButtonPressed(symbol: String) {
        if let bitName = self.bitName, let bitOwner = self.bitOwner, let bitImage = self.bitImage, let bit = self.bit {
            self.goToSecondLayerTemp(symbol: symbol, bitName: bitName, bitOwner: bitOwner, dynamicLinks: self.dynamicLinks, bitImage: bitImage, bit: bit)
        }
    }
    
    func secondLeftTableViewitemImageButtonPressed(tag: Int) {
        if self.leftBitsDetails[tag].image != nil {
            self.goToImageDetails(imageURL: leftBitsDetails[tag].image ?? "")
        }
    }
    
    func secondRightTableViewitemImageButtonPressed(tag: Int) {
        if self.rightBitsDetails[tag].image != nil {
            self.goToImageDetails(imageURL: rightBitsDetails[tag].image ?? "")
        }
    }
    
    
}

extension FirstTempViewController: ItemImageTableViewCellDelegate {
    func itemImageButtonPressed(tag: Int) {
        if bitsDetails[tag].image != nil {
            self.goToImageDetails(imageURL: bitsDetails[tag].image ?? "")
        }
    }
}

extension FirstTempViewController: MoreButtonPressedTableViewCellDelegate {
    func secondTempMoreButtonPressed(tag: Int) {
        if bitsDetails[tag].containItens == 1 {
            if let parentID = bitsDetails[tag].id, let bitCode = self.bitCode {
                if self.currentTempLevel == nil {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 1)
                } else if self.currentTempLevel == 1 {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 2)
                }
            }
        } else if bitsDetails[tag].externalLink != nil {
            goToWebsite(website: bitsDetails[tag].externalLink!)
        }
    }
    
    func thirdTempMoreButtonPressed(tag: Int) {
        print(tag)
    }
    
    func fourthTempMoreButtonPressed(tag: Int) {
        if bitsDetails[tag].containItens == 1 {
            print(bitsDetails[tag].id)
            if let parentID = bitsDetails[tag].id, let bitCode = self.bitCode {
                if self.currentTempLevel == nil {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 1)
                } else if self.currentTempLevel == 1 {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 2)
                }
            }
        } else if bitsDetails[tag].externalLink != nil {
            goToWebsite(website: bitsDetails[tag].externalLink!)
        }
    }
    
    func firstTempMoreButtonPressed(tag: Int) {
        if bitsDetails[tag].containItens == 1 {
            if let parentID = bitsDetails[tag].id, let bitCode = self.bitCode {
                if self.currentTempLevel == nil {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 1)
                } else if self.currentTempLevel == 1 {
                    self.goToTempStage(bitCode: bitCode, parentID: parentID, currentTempLevel: 2)
                }
            }
        } else if bitsDetails[tag].externalLink != nil {
            goToWebsite(website: bitsDetails[tag].externalLink!)
        }
    }
}

extension FirstTempViewController: RightArrowButtonTableViewCellDelegate {
    
    func rightArrowButtonPressed(symbol: String) {
        if let bitName = self.bitName, let bitOwner = self.bitOwner, let bitImage = self.bitImage, let bit = self.bit {
            self.goToSecondLayerTemp(symbol: symbol, bitName: bitName, bitOwner: bitOwner, dynamicLinks: self.dynamicLinks, bitImage: bitImage, bit: bit)
        }
    }
}

extension FirstTempViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var alternativeHeight: CGFloat?
        
        if self.tempChoosed == 1 || self.tempChoosed == 5 {
            if self.selectedIndex == indexPath.row && self.isCollapsed == true {
                alternativeHeight = 40
                if let cell = tableView.cellForRow(at: indexPath) as? FirstTempTableViewCell {
                    print(cell.descriptionLabel.maxNumberOfLines)
                    let lineCount = cell.descriptionLabel.maxNumberOfLines
                    print("Description: \(cell.descriptionLabel.text)")
                    var externalLink: String? = cell.externalLink
                    if lineCount == 0 && cell.descriptionLabel.text == "" && externalLink != nil {
                        return 40
                    } else if lineCount == 0 && cell.descriptionLabel.text == "" && externalLink == nil {
                        return 40
                    } else if (lineCount != 0 && cell.descriptionLabel.text != "" && externalLink != nil) || (lineCount != 0 && cell.descriptionLabel.text != "" && self.bitsDetails[indexPath.row].containItens == 1) {
                        print("Line count \(lineCount)")
                        let height: Float = Float(86 + (lineCount * 18))
                        return CGFloat(height)
                    } else if lineCount != 0 && cell.descriptionLabel.text != "" && externalLink == nil {
                        let height: Float = Float(50 + (lineCount * 18))
                        return CGFloat(height)
                    }
                } else {
                    return 40
                }
            } else {
                return 40
            }
        } else if tempChoosed == 3 {
            if self.extraHeight == 0 {
                let height = (self.bitsDetails.count / 2) * 158
                print(height)
                return CGFloat(height)
            } else if extraHeight != 0 {
                let height = Float((self.bitsDetails.count / 2 - 1) * 158) + self.extraHeight
                print(height)
                return CGFloat(height)
            }
        } else if tempChoosed == 4 {
            alternativeHeight = 228
            if self.selectedIndex == indexPath.row && self.isCollapsed == true {
                if let cell = tableView.cellForRow(at: indexPath) as? SecondTempTableViewCell {
                    print(cell.descriptionLabel.maxNumberOfLines)
                    let lineCount = cell.descriptionLabel.maxNumberOfLines
                    var externalLink: String? = cell.externalLink
                    if lineCount == 0 && cell.descriptionLabel.text == "" && externalLink != nil {
                        print("Bes 1")
                        return 228
                    } else if lineCount == 0 && cell.descriptionLabel.text == "" && externalLink == nil {
                        print("Bes 2")
                        return 228
                    } else if (lineCount != 0 && cell.descriptionLabel.text != "" && externalLink != nil) || (lineCount != 0 && cell.descriptionLabel.text != "" &&  self.bitsDetails[indexPath.row].containItens == 1) {
                        print("Bes 3")
                        let height: Float = Float(284 + (lineCount * 18))
                        return CGFloat(height)
                    } else if (lineCount != 0 && cell.descriptionLabel.text != "" && externalLink == nil) || (lineCount != 0 && cell.descriptionLabel.text != "" && self.bitsDetails[indexPath.row].containItens != 1) {
                        print("Bes 4")
                        let height: Float = Float(248 + (lineCount * 18))
                        return CGFloat(height)
                    } else {
                        print("Bes 5")
                        return 228
                    }
                }
            } else {
                print("Bes 5")
                return 228
            }
        } else if tempChoosed == 2 {
            alternativeHeight = 119
            if self.selectedIndex == indexPath.row && self.isCollapsed == true {
                if let cell = tableView.cellForRow(at: indexPath) as? FourthTempTableViewCell {
                    print(cell.descriptionLabel.maxNumberOfLines)
                    let lineCount = cell.descriptionLabel.maxNumberOfLines
                    var externalLink: String? = cell.externalLink
                    if lineCount == 0 && cell.descriptionLabel.text == "" {
                        //                        return 119
                        print("Bes 1")
                        cell.moreButtonConstraint.constant = 80
                        return 119
                    } else if (lineCount == 1 && cell.descriptionLabel.text != "" && externalLink != nil) || (lineCount == 1 && cell.descriptionLabel.text != "" && self.bitsDetails[indexPath.row].containItens == 1) || (lineCount == 2 && cell.descriptionLabel.text != "" && externalLink != nil) || (lineCount == 2 && cell.descriptionLabel.text != "" && self.bitsDetails[indexPath.row].containItens == 1) || (lineCount == 3 && cell.descriptionLabel.text != "" && externalLink != nil) || (lineCount == 3 && cell.descriptionLabel.text != "" && self.bitsDetails[indexPath.row].containItens == 1) {
                        cell.moreButtonConstraint.constant = 80
                        print("Bes 2")
                        return 119
                    } else if (lineCount == 1 && externalLink == nil) || (lineCount == 1 && self.bitsDetails[indexPath.row].containItens != 1) || (lineCount == 2 && externalLink == nil) || (lineCount == 2 && self.bitsDetails[indexPath.row].containItens != 1) || (lineCount == 3 && externalLink == nil) || (lineCount == 3 && self.bitsDetails[indexPath.row].containItens != 1) {
                        print("Bes ٣٣")
                        cell.moreButtonConstraint.constant = 80
                        return 119
                        //                        return 131
                    }  else if (lineCount >= 4 && externalLink != nil) || (lineCount >= 4 && self.bitsDetails[indexPath.row].containItens == 1) {
                        let height: Float = Float(166 + ((lineCount - 4) * 18))
                        print(height)
                        print(cell.moreButtonConstraint.constant)
                        cell.moreButtonConstraint.constant = 12
                        print("Bes 4")
                        return CGFloat(height)
                    } else if (lineCount >= 4 && externalLink == nil) || (lineCount >= 4 && self.bitsDetails[indexPath.row].containItens != 1) {
                        let height: Float = Float(166 + ((lineCount - 4) * 18))
                                                cell.moreButtonConstraint.constant = 80
                        print("Bes 5")
                        return CGFloat(height)
                    }
                }
            } else {
                print("IIIIIIIII")
                return 119
            }
        } else if tempChoosed == 6 {
            alternativeHeight = 40
            return 40
        }
        
        return alternativeHeight ?? 200
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tempChoosed == 3 {
            if self.bitsDetails.count > 0 {
                return 1
            } else {
                return 0
            }
        } else {
            return self.bitsDetails.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.tempChoosed == 1 || self.tempChoosed == 5 {
            if let cell: FirstTempTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FirstTempTableViewCell") as? FirstTempTableViewCell {
                
//                self.bitsDetails[indexPath.row].description = "Besm allah"
                
                if tempChoosed == 5 {
                    cell.valueLabel.isHidden = true
                }
                
                if (self.bitsDetails[indexPath.row].description == nil || self.bitsDetails[indexPath.row].description == "") && self.bitsDetails[indexPath.row].externalLink == nil && self.bitsDetails[indexPath.row].containItens != 1 {
                    print("Leeh kol el m2asy 3la m2asy")
                    cell.arrowImageView.isHidden = true
                    cell.moreButton.isHidden = true
                    cell.arrowImageWidthConstraint.constant = 0
                    cell.valueLabelTrailingConstraint.constant = 12
                }
                
                if "Lang".localized == "en" {
                    if self.localization?.contains("ع") ?? false || self.localization?.contains("r") ?? false {
                        //                        self.countriesCollectionView.semanticContentAttribute = .forceRightToLeft
//                        self.headerView.semanticContentAttribute = .forceRightToLeft
//                        self.headerView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        self.bitView.semanticContentAttribute = .forceRightToLeft
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImage.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.moreButton.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceRightToLeft
                        cell.valueLabel.semanticContentAttribute = .forceRightToLeft
                        cell.titleLabel.semanticContentAttribute = .forceRightToLeft
                    }
                } else if "Lang".localized == "ar" {
                    if self.localization?.contains("n") ?? false {
//                        self.headerView.semanticContentAttribute = .forceLeftToRight
//                        self.headerView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        self.bitView.semanticContentAttribute = .forceLeftToRight
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImage.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceLeftToRight
                        cell.valueLabel.semanticContentAttribute = .forceLeftToRight
                        cell.titleLabel.semanticContentAttribute = .forceLeftToRight
                    }
                }
                          
                // Five states
                
                // description only -> arrow without action
                if self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil {
                    print("Arrow without action")
                }
                
                // contain items or external link only -> arrow with action
                if (self.bitsDetails[indexPath.row].containItens == 1 && (self.bitsDetails[indexPath.row].description == "" || self.bitsDetails[indexPath.row].description == nil)) || (self.bitsDetails[indexPath.row].externalLink != nil && (self.bitsDetails[indexPath.row].description == "" || self.bitsDetails[indexPath.row].description == nil)) {
                    print("Arrow with Action")
                    cell.delegate = self
                    cell.moreDelegate = self
                }
                
                // description with ( contain or external ) -> more with action & arrow without
                if (self.bitsDetails[indexPath.row].containItens == 1 && self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil) || (self.bitsDetails[indexPath.row].externalLink != nil && self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil) {
                    print("Arrow with more Action")
                    cell.arrowButton.isHidden = true
                    cell.moreDelegate = self
                }

                cell.moreButton.tag = indexPath.row
                cell.arrowButton.tag = indexPath.row
                cell.imageButton.tag = indexPath.row
                cell.imageDelegate = self
                
                if let attr1: String = self.bitsDetails[indexPath.row].attribue1 {
                    cell.titleLabel.text = attr1
                }
                
                if let attr2: String = bitsDetails[indexPath.row].attribue2 {
                    cell.valueLabel.text = attr2
                }
                
                if self.defaultImageBool == false {
                    if let image: String = bitsDetails[indexPath.row].image {
                        cell.cellImage.isHidden = false
                        cell.cellImageWidthCinstraint.constant = 40
                        cell.cellImage.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + image)
                    } else {
                        cell.cellImageWidthCinstraint.constant = 0
                        cell.cellImage.isHidden = true
                    }
                } else {
                    cell.cellImageWidthCinstraint.constant = 0
                    cell.cellImage.isHidden = true
                }
                
                if let description: String = bitsDetails[indexPath.row].description {
                    cell.descriptionLabel.text = description
                }
                
                if let bitExternalLink = bitsDetails[indexPath.row].externalLink {
                    cell.externalLink = bitExternalLink
                }
                
                // MARK:- This line for removing extra lines in tableview
                tableView.tableFooterView = UIView()
                
                if "Lang".localized == "en" {
                    cell.arrowImageView.image = UIImage(named: "right arrow")
                } else if "Lang".localized == "ar" {
                    cell.arrowImageView.image = UIImage(named: "left arrow")
                }
                
                if indexPath == currentlySelectedRow {
                    if cell.descriptionLabel.text?.isEmpty == true {
                        if "Lang".localized == "en" {
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
                    } else {
                        cell.arrowImageView.image = UIImage(named: "down")
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == false {
                    if "Lang".localized == "en" {
                        cell.arrowImageView.image = UIImage(named: "right arrow")
                    } else if "Lang".localized == "ar" {
                        cell.arrowImageView.image = UIImage(named: "left arrow")
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == true {
                    if cell.descriptionLabel.text == "" {
                        if "Lang".localized == "en" {
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
                    } else {
                        cell.arrowImageView.image = UIImage(named: "down")
                    }
                }
                                
                return cell
            }
        } else if self.tempChoosed == 4 {
            if let cell: SecondTempTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SecondTempTableViewCell") as? SecondTempTableViewCell {
                
//                self.bitsDetails[indexPath.row].description = "Besm allah"
                
                if (self.bitsDetails[indexPath.row].description == nil || self.bitsDetails[indexPath.row].description == "") && self.bitsDetails[indexPath.row].externalLink == nil && self.bitsDetails[indexPath.row].containItens != 1 {
                    print("Leeh kol el m2asy 3la m2asy")
                    cell.arrowImageView.isHidden = true
                    cell.moreButton.isHidden = true
                    cell.arrowImageWidthConstraint.constant = 0
                    cell.valueLabelTrailingConstraint.constant = 12
                }
                
                if "Lang".localized == "en" {
                    if self.localization?.contains("ع") ?? false || self.localization?.contains("r") ?? false {
                        //                        self.countriesCollectionView.semanticContentAttribute = .forceRightToLeft
                        self.bitView.semanticContentAttribute = .forceRightToLeft
                        self.fifthTempView.semanticContentAttribute = .forceRightToLeft
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.moreButton.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceRightToLeft
                        cell.valueLabel.semanticContentAttribute = .forceRightToLeft
                        cell.titleLabel.semanticContentAttribute = .forceRightToLeft
                    }
                } else if "Lang".localized == "ar" {
                    if self.localization?.contains("n") ?? false {
                        self.bitView.semanticContentAttribute = .forceLeftToRight
                        self.fifthTempView.semanticContentAttribute = .forceLeftToRight
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceLeftToRight
                        cell.valueLabel.semanticContentAttribute = .forceLeftToRight
                        cell.titleLabel.semanticContentAttribute = .forceLeftToRight
                    }
                }
                
                // Five states
                
                // description only -> arrow without action
                if self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil {
                    print("Arrow without action")
                }
                
                // contain items or external link only -> arrow with action
                if (self.bitsDetails[indexPath.row].containItens == 1 && (self.bitsDetails[indexPath.row].description == "" || self.bitsDetails[indexPath.row].description == nil)) || (self.bitsDetails[indexPath.row].externalLink != nil && (self.bitsDetails[indexPath.row].description == "" || self.bitsDetails[indexPath.row].description == nil)) {
                    print("Arrow with Action")
                    cell.delegate = self
                    cell.moreDelegate = self
                }
                
                // description with ( contain or external ) -> more with action & arrow without
                if (self.bitsDetails[indexPath.row].containItens == 1 && self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil) || (self.bitsDetails[indexPath.row].externalLink != nil && self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil) {
                    print("Arrow with more Action")
                    cell.arrowButton.isHidden = true
                    cell.moreDelegate = self
                }
                                
                cell.moreButton.tag = indexPath.row
                cell.imageButton.tag = indexPath.row
                cell.imageDelegate = self
                cell.arrowButton.tag = indexPath.row
                
                
                if let attr1: String = self.bitsDetails[indexPath.row].attribue1 {
                    cell.titleLabel.text = attr1
                }
                
                if let attr2: String = bitsDetails[indexPath.row].attribue2 {
                    cell.valueLabel.text = attr2
                }
                
                if let image: String = bitsDetails[indexPath.row].image {
                    cell.cellImageView.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + image)
                } else {
                    cell.cellImageView.isHidden = true
                }
                
                if let description: String = bitsDetails[indexPath.row].description {
                    cell.descriptionLabel.text = description
                }
                
                if let bitExternalLink = bitsDetails[indexPath.row].externalLink {
                    cell.externalLink = bitExternalLink
                }
                
                if "Lang".localized == "en" {
                    cell.arrowImageView.image = UIImage(named: "right arrow")
                } else if "Lang".localized == "ar" {
                    cell.arrowImageView.image = UIImage(named: "left arrow")
                }
                
                if indexPath == currentlySelectedRow {
                    if cell.descriptionLabel.text == "" {
                        if "Lang".localized == "en" {
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
                    } else {
                        cell.arrowImageView.image = UIImage(named: "down")
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == false {
                    if "Lang".localized == "en" {
                        cell.arrowImageView.image = UIImage(named: "right arrow")
                    } else if "Lang".localized == "ar" {
                        cell.arrowImageView.image = UIImage(named: "left arrow")
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == true {
                    if cell.descriptionLabel.text == "" {
                        if "Lang".localized == "en" {
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
                    } else {
                        cell.arrowImageView.image = UIImage(named: "down")
                    }
                }
                                
                return cell
            }
        } else if self.tempChoosed == 3 {
            
            if let cell: TestTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TestTableViewCell") as? TestTableViewCell {
                
                cell.bitsDetails = self.bitsDetails
                cell.leftBitsDetails = self.leftBitsDetails
                cell.rightBitsDetails = self.rightBitsDetails
                cell.localization = self.localization
                cell.delegate = self
                
                return cell
            }
        } else if tempChoosed == 2 {
            if let cell: FourthTempTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FourthTempTableViewCell") as? FourthTempTableViewCell {
                                                
//                self.bitsDetails[indexPath.row].description = "Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah"
//                self.bitsDetails[indexPath.row].description = "Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah"
//                self.bitsDetails[indexPath.row].description = "Besm allah Besm allah Besm allah Besm allah Besm allah Besm allah"

//                cell.moreButtonConstraint.constant = 80
                
                if (self.bitsDetails[indexPath.row].description == nil || self.bitsDetails[indexPath.row].description == "") && self.bitsDetails[indexPath.row].externalLink == nil && self.bitsDetails[indexPath.row].containItens != 1 {
                    print("Leeh kol el m2asy 3la m2asy")
                    cell.arrowImageView.isHidden = true
                    cell.moreButton.isHidden = true
                    cell.arrowImageWidthConstraint.constant = 0
                    cell.valueLabelTrailingConstraint.constant = 12
                }
                
                if "Lang".localized == "en" {
                    if self.localization?.contains("ع") ?? false || self.localization?.contains("r") ?? false {
                        //                        self.countriesCollectionView.semanticContentAttribute = .forceRightToLeft
                        self.bitView.semanticContentAttribute = .forceRightToLeft
                        self.fifthTempView.semanticContentAttribute = .forceRightToLeft
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.moreButton.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceRightToLeft
                        cell.valueLabel.semanticContentAttribute = .forceRightToLeft
                        cell.titleLabel.semanticContentAttribute = .forceRightToLeft
                    }
                } else if "Lang".localized == "ar" {
                    if self.localization?.contains("n") ?? false {
                        self.bitView.semanticContentAttribute = .forceLeftToRight
                        self.fifthTempView.semanticContentAttribute = .forceLeftToRight
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceLeftToRight
                        cell.valueLabel.semanticContentAttribute = .forceLeftToRight
                        cell.titleLabel.semanticContentAttribute = .forceLeftToRight
                    }
                }
                
                // Five states
                if let description: String = bitsDetails[indexPath.row].description {
                    //                    cell.descriptionLabel = 3
                    cell.descriptionLabel.text = description
                    if cell.descriptionLabel.maxNumberOfLines <= 4 {
                        print("Num: \(cell.descriptionLabel.maxNumberOfLines)")
                        cell.moreButtonConstraint.constant = 80
                    }
                }
                
                // description only -> arrow without action
                if self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil {
                    print("Arrow without action")
                }
                
                // contain items or external link only -> arrow with action
                if (self.bitsDetails[indexPath.row].containItens == 1 && (self.bitsDetails[indexPath.row].description == "" || self.bitsDetails[indexPath.row].description == nil || cell.descriptionLabel.maxNumberOfLines <= 4)) || (self.bitsDetails[indexPath.row].externalLink != nil && (self.bitsDetails[indexPath.row].description == "" || self.bitsDetails[indexPath.row].description == nil || cell.descriptionLabel.maxNumberOfLines <= 4)) {
                    print("Arrow with Action")
                    cell.arrowButton.isHidden = false
                    cell.moreButton.isHidden = true
                    cell.moreButtonConstraint.constant = 80
                    cell.delegate = self
                    cell.moreDelegate = self
                }
                                
                // description with ( contain or external ) -> more with action & arrow without
                if (self.bitsDetails[indexPath.row].containItens == 1 && self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil && cell.descriptionLabel.maxNumberOfLines > 4) || (self.bitsDetails[indexPath.row].externalLink != nil && self.bitsDetails[indexPath.row].description != "" && self.bitsDetails[indexPath.row].description != nil && cell.descriptionLabel.maxNumberOfLines > 4) {
                    print("Arrow with more Action")
                    cell.arrowButton.isHidden = true
                    cell.moreButton.isHidden = false
                    cell.delegate = self
                    cell.moreDelegate = self
                }
                                
                cell.moreButton.tag = indexPath.row
                cell.imageButton.tag = indexPath.row
                cell.imageDelegate = self
                cell.arrowButton.tag = indexPath.row
                
                if let attr1: String = self.bitsDetails[indexPath.row].attribue1 {
                    cell.titleLabel.text = attr1
                }
                
                if let attr2: String = bitsDetails[indexPath.row].attribue2 {
                    cell.valueLabel.text = attr2
                }
                
                if let image: String = bitsDetails[indexPath.row].image {
                    cell.cellImageView.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + image)
                } else {
                    cell.cellImageView.isHidden = true
                }
                                
                if let bitExternalLink = bitsDetails[indexPath.row].externalLink {
                    cell.externalLink = bitExternalLink
                }
                
                
                if "Lang".localized == "en" {
                    cell.arrowImageView.image = UIImage(named: "right arrow")
                } else if "Lang".localized == "ar" {
                    cell.arrowImageView.image = UIImage(named: "left arrow")
                }
//                cell.moreButton.isHidden = true
                print("s1")
                
                if indexPath == currentlySelectedRow {
                    if cell.descriptionLabel.text?.isEmpty == true || cell.descriptionLabel.maxNumberOfLines <= 4 {
                        print("s2")
                        if "Lang".localized == "en" {
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
//                        cell.moreButton.isHidden = true
                    } else {
                        cell.arrowImageView.image = UIImage(named: "down")
                        print("Elly ma ytsama= \(cell.descriptionLabel.maxNumberOfLines)")
                        print("s3")
                        var firstTime: Int = 0
                        if firstTime == 0 {
                            firstTime = 1
                            if cell.descriptionLabel.maxNumberOfLines == 5 {
                                if "Lang".localized == "en" {
                                    cell.arrowImageView.image = UIImage(named: "right arrow")
                                } else if "Lang".localized == "ar" {
                                    cell.arrowImageView.image = UIImage(named: "left arrow")
                                }
                                cell.moreButtonConstraint.constant = 80
                            }
                        }
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == false {
                    print("s4")
                    cell.moreButtonConstraint.constant = 80
                    if "Lang".localized == "en" {
                        cell.arrowImageView.image = UIImage(named: "right arrow")
                    } else if "Lang".localized == "ar" {
                        cell.arrowImageView.image = UIImage(named: "left arrow")
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == true {
                    if cell.descriptionLabel.text == "" || cell.descriptionLabel.maxNumberOfLines <= 4 {
                        print("s5")
                        if "Lang".localized == "en" {
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
//                        cell.moreButton.isHidden = true
                    } else {
                        print("s6")
                        cell.arrowImageView.image = UIImage(named: "down")
                        if cell.externalLink == nil {
//                            cell.moreButton.isHidden = true
                        } else {
//                            cell.moreButton.isHidden = false
                        }
                    }
                }
                                                
                return cell
            }
        } else if tempChoosed == 6 {
            if let cell: FifthTempTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FifthTempTableViewCell") as? FifthTempTableViewCell {
                
                if "Lang".localized == "en" {
                    if self.localization?.contains("ع") ?? false || self.localization?.contains("r") ?? false {
                        self.bitView.semanticContentAttribute = .forceRightToLeft
                        self.fifthTempView.semanticContentAttribute = .forceRightToLeft
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.tempImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.tempTitleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.buyValueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.sellValueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        //                        cell.moreButton.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.tempTitleLabel.semanticContentAttribute = .forceRightToLeft
                        cell.buyValueLabel.semanticContentAttribute = .forceRightToLeft
                        cell.sellValueLabel.semanticContentAttribute = .forceRightToLeft
                    }
                } else if "Lang".localized == "ar" {
                    if self.localization?.contains("n") ?? false {
                        self.bitView.semanticContentAttribute = .forceLeftToRight
                        self.fifthTempView.semanticContentAttribute = .forceLeftToRight
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.tempImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.tempTitleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.buyValueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.sellValueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.tempTitleLabel.semanticContentAttribute = .forceLeftToRight
                        cell.buyValueLabel.semanticContentAttribute = .forceLeftToRight
                        cell.sellValueLabel.semanticContentAttribute = .forceLeftToRight
                    }
                }
                
                //                if self.bitsDetails[indexPath.row].containItens == 1 {
                //                    cell.delegate = self
                //                }
                
                cell.imageButton.tag = indexPath.row
                cell.imageDelegate = self

                let bitDetails = self.bitsDetails[indexPath.row]
                
                if let currencyTitle: String = bitDetails.attribue1 {
                    cell.tempTitleLabel.text = currencyTitle
                }
                
                if let buyValue: String = bitDetails.attribue2 {
                    cell.buyValueLabel.text = buyValue
                }
                
                if let sellValue: String = bitDetails.attribue3 {
                    cell.sellValueLabel.text = sellValue
                }
                
                if self.defaultImageBool == false {
                    if let image: String = bitsDetails[indexPath.row].image {
                        cell.tempImageView.isHidden = false
                        cell.cellImageWidthCinstraint.constant = 45
                        cell.tempImageView.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + image)
                    } else {
                        cell.cellImageWidthCinstraint.constant = 0
                        cell.tempImageView.isHidden = true
                    }
                } else {
                    cell.cellImageWidthCinstraint.constant = 0
                    cell.tempImageView.isHidden = true
                }

                
                if let image: String = bitsDetails[indexPath.row].image {
                    cell.tempImageView.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + image)
                } else {
                    cell.tempImageView.isHidden = true
                }
                
                return cell
            }
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        lastSelectedRow = currentlySelectedRow  // previous indexpath
        currentlySelectedRow = indexPath    // current indexpath
        
        if selectedIndex == indexPath.row {
            if self.isCollapsed == false {
                self.isCollapsed = true
            } else if self.isCollapsed == true {
                self.isCollapsed = false
            }
        } else {
            self.isCollapsed = true
        }
        self.selectedIndex = indexPath.row
        tableView.reloadRows(at: [indexPath,(lastSelectedRow ?? indexPath)], with: .automatic)  // Update current & previous indexpath
    }
}


// MARK:- CollectionView Delegate
extension FirstTempViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return countries.count
        } else if collectionView.tag == 1 {
            return self.bitsDetails.count
        }
        return 0
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 0 {
            if let cell: CountryCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CountryCollectionViewCell", for: indexPath) as? CountryCollectionViewCell {
                cell.choosedView.isHidden = true
                cell.countryLabel.text = self.countries[indexPath.row]
                if countryChoosedIndex == indexPath.row {
                    cell.choosedView.isHidden = false
                    self.collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
                }
                return cell
            }
        }
        
        if collectionView.tag == 1 {
            if let cell: ThirdTempCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThirdTempCollectionViewCell", for: indexPath) as? ThirdTempCollectionViewCell {
                
                if (self.bitsDetails[indexPath.row].description == nil || self.bitsDetails[indexPath.row].description == "") && self.bitsDetails[indexPath.row].externalLink == nil && self.bitsDetails[indexPath.row].containItens != 1 {
                    cell.arrowImageView.isHidden = true
                    cell.arrowImageWidthConstraint.constant = 0
                }
                
                if "Lang".localized == "en" {
                    if self.localization?.contains("ع") ?? false || self.localization?.contains("r") ?? false {
                        //                        self.countriesCollectionView.semanticContentAttribute = .forceRightToLeft
                        self.bitView.semanticContentAttribute = .forceRightToLeft
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.moreButton.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceRightToLeft
                        cell.valueLabel.semanticContentAttribute = .forceRightToLeft
                        cell.titleLabel.semanticContentAttribute = .forceRightToLeft
                    }
                } else if "Lang".localized == "ar" {
                    if self.localization?.contains("n") ?? false {
                        self.bitView.semanticContentAttribute = .forceLeftToRight
                        cell.contentView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.cellImageView.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.valueLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.titleLabel.transform = CGAffineTransform(scaleX: -1,y: 1)
                        cell.descriptionLabel.semanticContentAttribute = .forceLeftToRight
                        cell.valueLabel.semanticContentAttribute = .forceLeftToRight
                        cell.titleLabel.semanticContentAttribute = .forceLeftToRight
                    }
                }
                
                if self.specialSceinario == 1 {
                    cell.delegate = self
                }
                cell.moreButton.tag = indexPath.row
                cell.imageButton.tag = indexPath.row
                cell.moreDelegate = self
                cell.imageDelegate = self
                
                //                if indexPath.row == 2 {
                //                    cell.descriptionLabel.text = "asdjqwehkjqhwe"
                //                } else if indexPath.row == 1 {
                //                    cell.descriptionLabel.text = "asdjqwehkjqhwe"
                //                } else if indexPath.row == 0 {
                //                    cell.descriptionLabel.text = "إنه في يوم الرابع عشر من إيلول الموافق السابع عشر من حزيران عبرت قواتنا المسلحة خط بارليف المنيع ببسالة وإقدام غير مسبوقين متخطين بذلك كل ما هو خطير غير مسبوقين متخطين بذلك كل ما هو خطير إنه في يوم الرابع عشر من إيلول الموافق السابع عشر من حزيران عبرت قواتنا المسلحة خط بارليف المنيع ببسالة وإقدام غير مسبوقين متخطين بذلك كل ما هو خطير غير مسبوقين متخطين بذلك كل ما"
                //                }
                
                if let attr1: String = self.bitsDetails[indexPath.row].attribue1 {
                    cell.titleLabel.text = attr1
                }
                
                if let attr2: String = bitsDetails[indexPath.row].attribue2 {
                    cell.valueLabel.text = attr2
                }
                
                if let image: String = bitsDetails[indexPath.row].image {
                    cell.cellImageView.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + image)
                } else {
                    cell.cellImageView.isHidden = true
                }
                
                if let description: String = bitsDetails[indexPath.row].description {
                    cell.descriptionLabel.text = description
                }
                
                if let bitExternalLink = bitsDetails[indexPath.row].externalLink {
                    cell.externalLink = bitExternalLink
                }
                
                if "Lang".localized == "en" {
                    cell.arrowImageView.image = UIImage(named: "right arrow")
                } else if "Lang".localized == "ar" {
                    cell.arrowImageView.image = UIImage(named: "left arrow")
                }
                
                if indexPath == currentlySelectedRow {
                    if cell.descriptionLabel.text == "" {
                        if "Lang".localized == "en" {
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
                    } else {
                        cell.arrowImageView.image = UIImage(named: "down")
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == false {
                    if "Lang".localized == "en" {
                        cell.arrowImageView.image = UIImage(named: "right arrow")
                    } else if "Lang".localized == "ar" {
                        cell.arrowImageView.image = UIImage(named: "left arrow")
                    }
                }
                
                if currentlySelectedRow == lastSelectedRow && isCollapsed == true {
                    if cell.descriptionLabel.text == "" {
                        if "Lang".localized == "en" {
                            cell.arrowImageView.image = UIImage(named: "right arrow")
                        } else if "Lang".localized == "ar" {
                            cell.arrowImageView.image = UIImage(named: "left arrow")
                        }
                    } else {
                        cell.arrowImageView.image = UIImage(named: "down")
                    }
                }
                
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView.tag == 0 {
                        
            if let cell: CountryCollectionViewCell = collectionView.cellForItem(at: indexPath) as? CountryCollectionViewCell {
                lastCountryChoosedIndex = self.countryChoosedIndex // previous indexpath
                self.countryChoosedIndex = indexPath.row // current indexpath
//                countriesCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)

                if let bitCode = self.bitCode {
                    self.getBIT(bitCode: bitCode, firstCountry: self.countries[indexPath.row], parentID: self.parentID, sorting2: nil, sorting3: nil)
                }

//                self.countriesCollectionView.reloadData()
                self.countriesCollectionView.reloadItems(at: [indexPath, IndexPath(row: lastCountryChoosedIndex ?? indexPath.row, section: 0)])
            }
        }
        
        if collectionView.tag == 1 {
            lastSelectedRow = currentlySelectedRow  // previous indexpath
            currentlySelectedRow = indexPath    // current indexpath
            
            if selectedIndex == indexPath.row {
                if self.isCollapsed == false {
                    self.isCollapsed = true
                } else if self.isCollapsed == true {
                    self.isCollapsed = false
                }
            } else {
                self.isCollapsed = true
            }
            self.selectedIndex = indexPath.row
            // MARK:- Comment this after crash
            collectionView.reloadItems(at: [indexPath,(lastSelectedRow ?? indexPath)])
            
            // MARK:- Old & Correct code
            
            //            if selectedIndex == indexPath.row {
            //                if self.isCollapsed == false {
            //                    self.isCollapsed = true
            //                } else if self.isCollapsed == true {
            //                    self.isCollapsed = false
            //                }
            //            } else {
            //                self.isCollapsed = true
            //            }
            //            self.selectedIndex = indexPath.row
            //            print(self.selectedIndex)
            //            collectionView.reloadItems(at: [indexPath])
        }
        
    }
}

// MARK:- CollectionView Layout delegate
extension FirstTempViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 1 {
            
            let width = (collectionView.bounds.width - 40 ) / 2.0
            
            if self.selectedIndex == indexPath.row && self.isCollapsed == true {
                if let cell = collectionView.cellForItem(at: indexPath) as? ThirdTempCollectionViewCell {
                    print(cell.descriptionLabel.maxNumberOfLines)
                    let lineCount = cell.descriptionLabel.maxNumberOfLines
                    if lineCount == 0 {
                        return CGSize (width: width, height: 145.5)
                    } else {
                        let height: Float = Float(192 + (lineCount * 18))
                        return CGSize (width: width, height: CGFloat(height))
                    }
                }
            } else {
                return CGSize (width: width, height: 145.5)
            }
        } else if collectionView.tag == 0 {
            let width = (collectionView.bounds.width ) / 3.0
            return CGSize (width: width, height: 50)
        }
        
        let width = (collectionView.bounds.width - 40 ) / 7.0
        return CGSize (width: width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView.tag == 1 {
            return UIEdgeInsets.init(top: 5 , left: 10, bottom: 10, right: 10)
        }
        return UIEdgeInsets.init(top: 0 , left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 1 {
            return 10
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 1 {
            return 10
        }
        return 0
    }
}

extension FirstTempViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Discover" {
            self.goToDiscoverPage()
        } else if keyWord == "Search" {
            self.goToSearchPage()
        } else if keyWord == "Home" {
            self.goToHomeBITS()
        } else if keyWord == "Settings" {
            self.goToSettingsPage()
        } else if keyWord == "Scanner" {
            self.goToScannerPage()
        }
    }
}
