//
//  SecondLayerTempViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/19/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class SecondLayerTempViewController: BaseViewController {
    
    var currenciesDeatils: [CurrencyDetails] = [CurrencyDetails]()
    var symbol: String?
    var bitName: String?
    var bitOwner: String?
    var dynamicLinks: [DynamicLink] = [DynamicLink]()
    var bitImage: String?
    var bit: BIT?

    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView

    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var updatedAtTimeValueLabel: UILabel!
    @IBOutlet weak var firstTempImageView: UIImageView!
    @IBOutlet weak var secondTempImageView: UIImageView!
    @IBOutlet weak var thirdTempImageView: UIImageView!
    @IBOutlet weak var fourthTempImageView: UIImageView!
    @IBOutlet weak var bitNameLabel: UILabel!
    @IBOutlet weak var bitOwnerLabel: UILabel!
    @IBOutlet weak var firstTempView: UIView!
    @IBOutlet weak var secondTempView: UIView!
    @IBOutlet weak var thirdTempView: UIView!
    @IBOutlet weak var fourthTempView: UIView!
    @IBOutlet weak var bitImageView: UIImageView!
    @IBOutlet weak var setButton: UIButton!
    @IBOutlet weak var lastUpdateLabel: UILabel!
    @IBOutlet weak var dateValueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        if let symbol = self.symbol {
            self.getCurrencies(symbol: symbol)
        }
        if self.dynamicLinks.count > 0 {
            self.bindDynamicLinks(dynamicLinks: self.dynamicLinks)
        }
    }
    
    func configureView() {
        
        setButton.setTitle("set button title".localized, for: .normal)
        lastUpdateLabel.text = "last update label".localized

        reusableView.frame = self.viewContainToolBar.bounds
        self.viewContainToolBar.addSubview(reusableView)
        reusableView.ButtonDelegate = self
        
        bitImageView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        firstTempImageView.addCornerRadius(raduis: firstTempImageView.frame.height / 2, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        secondTempImageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        thirdTempImageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        fourthTempImageView.addCornerRadius(raduis: 8, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        
        if let image = self.bitImage {
            bitImageView.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + image)
        }
        
        if let bitName = self.bitName {
            bitNameLabel.text = bitName
        }
        
        if let bitOwner = self.bitOwner {
            if "Lang".localized == "en" {
                self.bitOwnerLabel.text = "By: " + bitOwner
            } else if "Lang".localized == "ar" {
                self.bitOwnerLabel.text = "بواسطة: " + bitOwner
            }
        }

        firstTempImageView.isHidden = true
        secondTempView.isHidden = true
        thirdTempView.isHidden = true
        fourthTempView.isHidden = true
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "FifthTempTableViewCell", bundle: nil), forCellReuseIdentifier: "FifthTempTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func bindDynamicLinks(dynamicLinks: [DynamicLink]) {
        for i in 0...dynamicLinks.count - 1 {
            if i == 0 {
                if let image = dynamicLinks[i].image {
                    self.secondTempView.isHidden = false
                    self.secondTempImageView.loadImageFromUrl(imageUrl: DYNAMIC_LINKS_IMAGE_URL + image)
                }
            } else if i == 1 {
                if let image = dynamicLinks[i].image {
                    self.thirdTempView.isHidden = false
                    self.thirdTempImageView.loadImageFromUrl(imageUrl: DYNAMIC_LINKS_IMAGE_URL + image)
                }
            } else if i == 2 {
                if let image = dynamicLinks[i].image {
                    self.fourthTempView.isHidden = false
                    self.fourthTempImageView.loadImageFromUrl(imageUrl: DYNAMIC_LINKS_IMAGE_URL + image)
                }
            }
        }
    }
    
    func goToWebsite(website: String) {
        if let url = URL(string: website),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    //MARK:- Networking
    func getCurrencies(symbol: String) {
        let parameters: [String : AnyObject] = [:]
        
        self.startLoading()
        
        BITAPIManager().getCurrencyDetails(symbol: symbol, basicDictionary: parameters , onSuccess: { (currenciesDetails) in
            
            self.stopLoadingWithSuccess()
            
            self.currenciesDeatils = currenciesDetails
            if let updatedAt: String = self.currenciesDeatils[0].createdAt {
                self.updatedAtTimeValueLabel.text = updatedAt
            }
            self.tableView.reloadData()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }

    // MARK:- Actions    
    @IBAction func secondTempIsPressed(_ sender: Any) {
        if let website = self.dynamicLinks[0].link {
            goToWebsite(website: website)
        }
    }
    
    @IBAction func thirdTempIsPressed(_ sender: Any) {
        if let website = self.dynamicLinks[1].link {
            goToWebsite(website: website)
        }
    }
    
    @IBAction func fourthTempIsPressed(_ sender: Any) {
        if let website = self.dynamicLinks[2].link {
            goToWebsite(website: website)
        }
    }
    
    @IBAction func setWidgetButtonIsPressed(_ sender: Any) {
        if let bit = self.bit {
            self.goToSetWidget(bit: bit)
        }
    }
    
    // MARK: - Navigation
    func goToSetWidget(bit: BIT) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SetWidgetViewController") as! SetWidgetViewController
        
        viewController.bit = bit
        viewController.deepLinkBitCode = bit.bitCode!
        
        //        navigationController?.pushViewController(viewController, animated: true)
        self.present(viewController, animated: true, completion: nil)
    }


}
