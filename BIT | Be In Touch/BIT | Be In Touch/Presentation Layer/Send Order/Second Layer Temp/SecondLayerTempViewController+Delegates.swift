//
//  SecondLayerTempViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/19/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension SecondLayerTempViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Discover" {
            self.goToDiscoverPage()
        } else if keyWord == "Search" {
            self.goToSearchPage()
        } else if keyWord == "Home" {
            self.goToHomeBITS()
        } else if keyWord == "Settings" {
            self.goToSettingsPage()
        } else if keyWord == "Scanner" {
            self.goToScannerPage()
        }
    }
}

extension SecondLayerTempViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currenciesDeatils.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: FifthTempTableViewCell = tableView.dequeueReusableCell(withIdentifier: "FifthTempTableViewCell") as? FifthTempTableViewCell {
            
            let currencyDetails = currenciesDeatils[indexPath.row]
            
            if let currencyTitle: String = currencyDetails.bankNameEN, let buyValue: String = currencyDetails.buyPrice, let sellValue: String = currencyDetails.sellPrice {
                cell.tempTitleLabel.text = currencyTitle
                cell.buyValueLabel.text = buyValue
                cell.sellValueLabel.text = sellValue
//                cell.tempImageView.loadImageFromUrl(imageUrl: ImageURLVegetables + image)
            }
            
            return cell
        }
        return UITableViewCell()
    }
}
