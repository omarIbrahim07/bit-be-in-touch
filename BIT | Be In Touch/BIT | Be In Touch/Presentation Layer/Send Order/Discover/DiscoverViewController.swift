//
//  DiscoverViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import SwiftyGif

class DiscoverViewController: BaseViewController {
    
    var discoverBits: [BITDiscover] = [BITDiscover]()
    var seeAllButtonIsPressed: Bool? = false
    var test : [[BIT]] = [[BIT]]()
    var testBool: [Bool] = [Bool]()
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView

    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        tableView.tableHeaderView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(120))
        getDiscoverBits()
    }
    
    func configureView() {
//        reusableView.frame = self.viewContainToolBar.bounds
//        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "BITSTableViewCell", bundle: nil), forCellReuseIdentifier: "BITSTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CellHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: CellHeader.reuseIdentifier)
    }
    
    func checkSeeAllButton() {
        if self.seeAllButtonIsPressed == true {
            self.tableView.reloadData()
        }
    }
        
// MARK:- Networking
    func getDiscoverBits() {
        
        let parameters: [String : AnyObject] = [
            "locale"    : "Lang".localized as AnyObject
        ]
                
//        self.startLoading()
        self.animateGif(view: self.view)
        
        BITAPIManager().getDiscoverBITS(basicDictionary: parameters , onSuccess: { (discoverBits) in
            
            
            self.discoverBits = discoverBits
            if discoverBits.count > 0 {
                self.testBool = []
                for i in 0...discoverBits.count - 1 {
                    self.testBool.append(false)
                    self.test.append([BIT]())
                }
            }
            
            //            self.stopLoadingWithSuccess()
            self.stopAnimateGif()
            self.tableView.reloadData()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }
    
    func getDiscoverBlockBits(api: String, index: Int) {
        
        let parameters: [String : AnyObject] = [
            "locale"    : "Lang".localized as AnyObject
        ]
                
//        self.startLoading()
        self.animateGif(view: self.view)
        
        BITAPIManager().getDiscoverBlockBITS(api: api, basicDictionary: parameters , onSuccess: { (bits) in
            
//            self.stopLoadingWithSuccess()
            self.stopAnimateGif()
            
//            self.allBits = bits
            self.test.remove(at: index)
            self.test.insert(bits, at: index)
            self.tableView.reloadData()
            
        }) { (error) in
            self.stopLoadingWithError(error: error)
        }
    }

    // MARK: - Navigation
    func goToSetWidget(bit: BIT) {
        if let setWidgetVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetWidgetViewController") as? SetWidgetViewController {
            setWidgetVC.bit = bit
            setWidgetVC.bitCode = bit.bitCode
            setWidgetVC.deepLinkBitCode = bit.bitCode!
//            self.present(setWidgetVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(setWidgetVC, animated: true)
        }
    }

}

