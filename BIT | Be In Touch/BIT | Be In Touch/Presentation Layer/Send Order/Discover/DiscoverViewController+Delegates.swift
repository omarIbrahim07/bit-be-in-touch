//
//  DiscoverViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import SwiftyGif

extension DiscoverViewController : SwiftyGifDelegate {
    
    func gifDidLoop(sender: UIImageView) {
        print("gifDidLoop")
//        presentSplashScreen()
    }
}

extension DiscoverViewController: seeAllButtonTableViewCellDelegate {
    func seeAllButtonPressed(index: Int) {
        testBool[index] = true
        print(testBool)
        print(index)
        if let api = discoverBits[index].api {
            self.getDiscoverBlockBits(api: api, index: index)
        }
    }
}

extension DiscoverViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.discoverBits.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if testBool[section] == false {
            return discoverBits[section].bits?.count ?? 0
        } else if testBool[section] == true {
            return test[section].count
        }
                
        return 0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell: BITSTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BITSTableViewCell") as? BITSTableViewCell {
            
            tableView.tableFooterView = UIView()
            
            if testBool[indexPath.section] == false {

                let discoverBit = self.discoverBits[indexPath.section]
                if let bits = discoverBit.bits {
                    if let bitName = bits[indexPath.row].title {
                        cell.bitNameLabel.text = bitName
                    }
                    if let ownerName = bits[indexPath.row].ownerName {
                        if "Lang".localized == "en" {
                            cell.bitOwnerLabel.text = "By: " + ownerName
                        } else if "Lang".localized == "ar" {
                            cell.bitOwnerLabel.text = "بواسطة: " + ownerName
                        }
                    }
                    if let bitImage = bits[indexPath.row].image {
                        cell.bitImageView.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + bitImage)
                    }
                    if let category = bits[indexPath.row].category {
                        cell.bitTypeLabel.text = category
                    }
                }
                
            } else if testBool[indexPath.section] == true {

                let discoverBITS = self.test[indexPath.section]
                if let bitName = discoverBITS[indexPath.row].title {
                    cell.bitNameLabel.text = bitName
                }
                if let ownerName = discoverBITS[indexPath.row].ownerName {
                    cell.bitOwnerLabel.text = "By: " + ownerName
                }
                if let bitImage = discoverBITS[indexPath.row].image {
                    cell.bitImageView.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + bitImage)
                }
                if let category = discoverBITS[indexPath.row].category {
                    cell.bitTypeLabel.text = category
                }
            }
                    
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if testBool[indexPath.section] == false {
            let discoverBit = self.discoverBits[indexPath.section]
            if let bits = discoverBit.bits {
                let bit = bits[indexPath.row]
                self.goToSetWidget(bit: bit)
            }
        } else if testBool[indexPath.section] == true {
            let discoverBITS = self.test[indexPath.section]
            let bit = discoverBITS[indexPath.row]
            self.goToSetWidget(bit: bit)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CellHeader") as? CellHeader {
            //                headerView.headerLabel.text = "info".localized
            headerView.headerLabel.text = self.discoverBits[section].blockName?.uppercased()
            headerView.index = section
            headerView.seeAllButton.tag = section
            headerView.delegate = self
            if testBool[section] == true {
//                headerView.seeAllButton.isHidden = true
                print(testBool)
            }
            return headerView
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 124  // or whatever
    }
}

extension DiscoverViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Discover" {
//            self.goToDiscoverPage()
        } else if keyWord == "Search" {
            self.goToSearchPage()
        } else if keyWord == "Home" {
            self.goToHomeBITS()
        } else if keyWord == "Settings" {
            self.goToSettingsPage()
        } else if keyWord == "Scanner" {
            self.goToScannerPage()
        }
    }
}
