//
//  BITSTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class BITSTableViewCell: UITableViewCell {

    @IBOutlet weak var bitImageView: UIImageView!
    @IBOutlet weak var bitOwnerLabel: UILabel!
    @IBOutlet weak var getButton: UIButton!
    @IBOutlet weak var bitNameLabel: UILabel!
    @IBOutlet weak var bitTypeLabel: UILabel!
    @IBOutlet weak var rightArrowImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
        configureColours()
        self.selectionStyle = .none
    }
    
    func configure() {
        getButton.setTitle("get button title".localized, for: .normal)
        bitImageView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bitTypeLabel.text = "bit type label".localized
        if "Lang".localized == "en" {
            rightArrowImageView.image = UIImage(named: "Right arrow")
        } else if "Lang".localized == "ar" {
            rightArrowImageView.image = UIImage(named: "Arrow")
        }
    }
    
    func configureColours() {
        bitOwnerLabel.textColor = SamnyColour
        bitNameLabel.textColor = SamnyColour
        bitTypeLabel.textColor = SamnyColour
        getButton.setTitleColor(SamnyColour, for: .normal)
        bitImageView.backgroundColor = #colorLiteral(red: 0.2039215686, green: 0.2117647059, blue: 0.2117647059, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func rightArrowButtonIsPressed(_ sender: Any) {
        print("Right arrow button is pressed")
    }
    
}
