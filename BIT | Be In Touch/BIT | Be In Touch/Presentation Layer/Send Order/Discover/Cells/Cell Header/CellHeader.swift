//
//  CellHeader.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol seeAllButtonTableViewCellDelegate {
    func seeAllButtonPressed(index: Int)
}

class CellHeader: UITableViewHeaderFooterView {

    static let reuseIdentifier = "CellHeader"
    var delegate: seeAllButtonTableViewCellDelegate?
    var index: Int?
    
    var seeAllButtonIsPressed: [Bool]? = [false, false, false]
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var seeAllButton: UIButton!
    
    override func awakeFromNib() {
        configureColours()
        setLocalization()
    }
    
    func configureColours() {
        headerLabel.textColor = SamnyColour
        seeAllButton.setTitleColor(SamnyColour, for: .normal)
    }
    
    func setLocalization() {
        seeAllButton.setTitle("see all label".localized, for: .normal)
    }
    
    func seeAllButtonPressed(index: Int) {
        if let delegateValue = delegate {
            delegateValue.seeAllButtonPressed(index: index)
        }
    }
    
    @IBAction func seeAllButton(_ sender: Any) {
        if seeAllButtonIsPressed?[index!] == true {
            return
        }
        seeAllButtonIsPressed?[index!] = true
        if let index = self.index {
            seeAllButtonPressed(index: index)
        }
    }
}
