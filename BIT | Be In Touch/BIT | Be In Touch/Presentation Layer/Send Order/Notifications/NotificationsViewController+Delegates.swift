//
//  NotificationsViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

extension NotificationsViewController: SetNotificationsButtonTableViewCellDelegate {
    func arrowButtonIsPressed(bitCode: String) {
        goToTemp(bitCode: bitCode)
    }
    
    func setNotificationButtonPressed(bitCode: String, tag: Int) {
        print(bitCode)
//        self.setNotification(bitCode: bitCode, tag: tag)
        self.showReportPopUp(bitCode: bitCode, tag: tag)
    }
}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as? NotificationTableViewCell {
            let notification = notifications[indexPath.row]
            
            cell.notificationSwitchReplacementButton.tag = indexPath.row
            
            if let bitCode = notification.bitCode {
                cell.bitCode = bitCode
            }
            if let notificationFlag = notification.notificationFlag {
                if notificationFlag == 0 {
                    cell.setNotificationSwitch.setOn(false, animated: false)
                } else if notificationFlag == 1 {
                    cell.setNotificationSwitch.setOn(true, animated: false)
                }
            }
            if let bitTitle = notification.title {
                cell.bitNameLabel.text = bitTitle
            }
            if let ownerName = notification.ownerName {
                if "Lang".localized == "en" {
                    cell.bitOwnerLabel.text = "By: " + ownerName
                } else if "Lang".localized == "ar" {
                    cell.bitOwnerLabel.text = "بواسطة: " + ownerName
                }
            }
            if let image = notification.image {
                cell.bitImageView.loadImageFromUrl(imageUrl: BIT_IMAGE_URL + image)
            }
            if let category: String = notification.category {
                cell.bitTypeLabel.text = category
            }
            cell.delegate = self
            return cell
        }
        
        return UITableViewCell()
    }

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            if let bit = self.bit {
//                self.goToSetWidget(bit: bit)
//            }
//        } else if searched == false {
//
//        }
//    }
}

extension NotificationsViewController: ViewButtonDelegate {
    func didButtonPressed(keyWord: String) {
        if keyWord == "Discover" {
            self.goToDiscoverPage()
        } else if keyWord == "Search" {
            self.goToSearchPage()
        } else if keyWord == "Home" {
            self.goToHomeBITS()
        } else if keyWord == "Settings" {
            self.goToSettingsPage()
        } else if keyWord == "Scanner" {
            self.goToScannerPage()
        }
    }
}
