//
//  NotificationTableViewCell.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol SetNotificationsButtonTableViewCellDelegate {
    func setNotificationButtonPressed(bitCode: String, tag: Int)
    func arrowButtonIsPressed(bitCode: String)
}

class NotificationTableViewCell: UITableViewCell {

    var delegate: SetNotificationsButtonTableViewCellDelegate?
    var bitCode: String?

    @IBOutlet weak var bitImageView: UIImageView!
    @IBOutlet weak var setNotificationSwitch: UISwitch!
    @IBOutlet weak var bitOwnerLabel: UILabel!
    @IBOutlet weak var bitNameLabel: UILabel!
    @IBOutlet weak var bitTypeLabel: UILabel!
    @IBOutlet weak var rightArrowImageView: UIImageView!
    @IBOutlet weak var notificationSwitchReplacementButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configure()
        if "Lang".localized == "ar" {
            setNotificationSwitch.transform = CGAffineTransform(scaleX: -1,y: 1)
        }
    }
    
    func configure() {
        bitImageView.addCornerRadius(raduis: 20, borderColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0)
        bitTypeLabel.text = "bit type label".localized
        if "Lang".localized == "en" {
            rightArrowImageView.image = UIImage(named: "Right arrow")
        } else if "Lang".localized == "ar" {
            rightArrowImageView.image = UIImage(named: "Arrow")
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setNotificationButtonPressed(bitCode: String, tag: Int) {
        if let delegateValue = delegate {
            delegateValue.setNotificationButtonPressed(bitCode: bitCode, tag: tag)
        }
    }
    
    func arrowButtonIsPressed(bitCode: String) {
        if let delegateValue = delegate {
            delegateValue.arrowButtonIsPressed(bitCode: bitCode)
        }
    }
    
    @IBAction func languageIsPressed(_ sender: UISwitch) {
//        if let bitCode = self.bitCode {
//            setNotificationButtonPressed(bitCode: bitCode)
//        }

        if sender.isOn {
            if let bitCode = self.bitCode {
//                setNotificationButtonPressed(bitCode: bitCode)
            }
            print("Is on")
        } else if sender.isOn == false {
            if let bitCode = self.bitCode {
//                setNotificationButtonPressed(bitCode: bitCode)
            }
            print("Is off")
        }
    }
    
    @IBAction func arrowButtonIsPressed(_ sender: Any) {
        if let bitCode = self.bitCode {
            arrowButtonIsPressed(bitCode: bitCode)
        }
    }
    
    @IBAction func notificationReplacementButtonIsPressed(_ sender: Any) {
        if let bitCode = self.bitCode {
            setNotificationButtonPressed(bitCode: bitCode, tag: notificationSwitchReplacementButton.tag)
        }

    }
    
}
