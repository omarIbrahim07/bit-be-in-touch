//
//  NotificationsViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/13/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseViewController {

    var notifications: [Notificationn] = [Notificationn]()
    
    var reusableView = Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)?.first as! CustomView

    @IBOutlet weak var viewContainToolBar: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notificationsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
        self.getNotifications(tag: nil)
    }
    
    func configureView() {
        notificationsLabel.text = "notification label".localized
//        reusableView.frame = self.viewContainToolBar.bounds
//        self.viewContainToolBar.addSubview(reusableView)
//        reusableView.ButtonDelegate = self
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func showReportPopUp(bitCode: String, tag: Int) {
        let alertController = UIAlertController(title: "notification button title".localized, message: "notification alert message".localized, preferredStyle: .alert)
        let openAction = UIAlertAction(title: "change notification configuration".localized, style: .default) { (action) in
            self.setNotification(bitCode: bitCode, tag: tag)
        }
        let cancelAction = UIAlertAction(title: "cancel notification".localized, style: .cancel)
        alertController.addAction(openAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
   
// MARK:- Netwoking
    func getNotifications(tag: Int?) {
        
        weak var weakSelf = self
        
        let parameters: [String : AnyObject] = [
        "device_id"    : deviceID as AnyObject,
        "locale" : "Lang".localized as AnyObject
        ]
        
//        startLoading()
        self.animateGif(view: self.view)
        
        PackageAPIManager().getNotifications(basicDictionary: parameters, onSuccess: { (myNotifications) in
            
            self.notifications = myNotifications
            
            //            self.stopLoadingWithSuccess()
            self.stopAnimateGif()
            if tag == nil {
               self.tableView.reloadData()
            } else {
                let index = IndexPath(row: tag ?? 0, section: 0)
                self.tableView.reloadRows(at: [index], with: .automatic)
            }
            
            
        }) { (error) in
            weakSelf?.stopLoadingWithSuccess()
        }
    }
    
    func setNotification(bitCode: String, tag: Int) {
        
        weak var weakSelf = self
        
        let parameters: [String : AnyObject] = [
            "device_id" : deviceID as AnyObject,
            "bit_code" : bitCode as AnyObject,
        ]
                
        PackageAPIManager().setNotificationSetting(basicDictionary: parameters, onSuccess: { (saved) in
            
            print(saved)
            if saved == true {
                self.getNotifications(tag: tag)
            }
            
        }) { (error) in
            weakSelf?.stopLoadingWithSuccess()
        }
    }
    
    // MARK: - Navigation
    func goToTemp(bitCode: String) {
        if let firstTempVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstTempViewController") as? FirstTempViewController {
            firstTempVC.bitCode = bitCode
            firstTempVC.backToHome = false
            //            firstTempVC.firstCountry = firstCountry
            
//            self.present(firstTempVC, animated: true, completion: nil)
            self.navigationController?.pushViewController(firstTempVC, animated: true)
        }
    }
    
}
