//
//  ChangeLanguageViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ChangeLanguageViewController: BaseViewController {

    var languages: [String] = ["en", "ar"]
    var searching = false
    var searchedLanguages: [String] = [String]()
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var languagesLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
        configureTableView()
    }
    
    func configureView() {
        languagesLabel.text = "change language title label".localized
        searchBar.placeholder = "language searchbar placeholder".localized
        searchBar.delegate = self
        searchBar.enablesReturnKeyAutomatically = true
        searchBar.searchTextField.clearButtonMode = .never
        closeKeypad()
    }
    
    func configureTableView() {
        tableView.register(UINib(nibName: "LanguageTableViewCell", bundle: nil), forCellReuseIdentifier: "LanguageTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func closeKeypad() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    // MARK: - Navigation
    func reset() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let viewController = CustomTabbarViewController(nibName: "CustomTabbarViewController", bundle: nil)
        appDelegate.window!.rootViewController = viewController
        appDelegate.window!.makeKeyAndVisible()
//        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//        let stry = UIStoryboard(name: "Main", bundle: nil)
//        rootviewcontroller.rootViewController = stry.instantiateViewController(withIdentifier: "HomeNavigationVC")
    }

}
