//
//  ChangeLanguageViewController+Delegates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/27/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit
import MOLH

extension ChangeLanguageViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searching = true
        searchBar.showsCancelButton = true
        self.searchedLanguages = languages.filter({ (subcatChar) -> Bool in
            guard let text = searchBar.text else { return false }
            return (subcatChar.contains(text.localizedLowercase))
        })
        print(searchedLanguages)
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searching = false
        searchBar.showsCancelButton = false
        searchBar.text = ""
        tableView.reloadData()
    }
    
}

extension ChangeLanguageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searching == true {
            return searchedLanguages.count
        } else {
            return languages.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell: LanguageTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell") as? LanguageTableViewCell {
            
            tableView.tableFooterView = UIView()

            if searching {

                let language = searchedLanguages[indexPath.row]
                
                cell.languageLabel.text = language
            } else if !searching {
                let language = languages[indexPath.row]
                
                cell.languageLabel.text = language
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searching {
            if "Lang".localized != self.searchedLanguages[indexPath.row] {
                let language = self.searchedLanguages[indexPath.row]
                MOLH.setLanguageTo(language)
                reset()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
//            MOLH.reset()
        } else if !searching {
            if "Lang".localized != self.languages[indexPath.row] {
                let language = self.languages[indexPath.row]
                MOLH.setLanguageTo(language)
                reset()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
//            MOLH.reset()
        }
    }
}
