//
//  ImageZoomedViewController.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 8/6/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

class ImageZoomedViewController: BaseViewController {

    var imageURL: String?
    @IBOutlet weak var image: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let imageURL = self.imageURL {
            image.loadImageFromUrl(imageUrl: BIT_ITEMS_IMAGE_URL + imageURL)
        }
    }

}
