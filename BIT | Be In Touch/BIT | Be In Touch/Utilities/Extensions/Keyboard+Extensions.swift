//
//  Keyboard+Extensions.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 7/11/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import UIKit


// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
