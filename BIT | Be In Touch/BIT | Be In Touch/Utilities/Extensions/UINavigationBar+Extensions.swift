//
//  UINavigationBar+Extensions.swift
//  GameOn
//
//  Created by Hassan on 2/3/18.
//  Copyright © 2018 Hassan. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    func adjustDefaultNavigationBar() {
//        self.barTintColor = mainBlueColor
        self.barTintColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
        
        self.setBackgroundImage(UIImage(), for: .default)
//        self.backgroundColor = mainBlueColor
        self.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.2039215686, blue: 0.2549019608, alpha: 1)
        
        self.tintColor = #colorLiteral(red: 0.9215686275, green: 0.2039215686, blue: 0.2549019608, alpha: 1)

        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.red]
        self.titleTextAttributes = (titleDict as! [NSAttributedString.Key : Any])
        
        self.isTranslucent = false
        self.shadowImage = UIImage()
    }
}
