//
//  UIView+Extensions.swift
//  GameOn
//
//  Created by Hassan on 12/16/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import UIKit
import Toast_Swift

extension UIView {
    
//    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
//        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
//        let mask = CAShapeLayer()
//        mask.path = path.cgPath
//        layer.mask = mask
//    }
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            clipsToBounds = true
            layer.cornerRadius = radius
            layer.maskedCorners = CACornerMask(rawValue: corners.rawValue)
        } else {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
    }
    
    func addCornerRadius(raduis: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = raduis
        self.layer.masksToBounds = true

        if borderWidth > 0 {
            self.layer.borderColor = borderColor.cgColor
            self.layer.borderWidth = borderWidth
        }
    }
    
    func createGradientLayer(color: UIColor) {
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.bounds
        
        gradientLayer.colors = [UIColor.clear.cgColor, color.cgColor]
        
        self.layer.addSublayer(gradientLayer)
    }
    
    func addShadowLikeNavigationBar() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 4
    }

    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func showError(error: APIError) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
//        style.messageFont = FontsManager.shared.OpenSansRegularWithSize(12)
        self.makeToast(error.message, duration: 3.0, position: .bottom, title: nil, image: #imageLiteral(resourceName: "toast-error"), style: style, completion: nil)
    }
    
    func showError(error: APIError, complete: @escaping (Bool)->Void) {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        var style: ToastStyle = ToastStyle.init()
        style.imageSize = CGSize(width: 44, height: 44)
//        style.messageFont = FontsManager.shared.OpenSansRegularWithSize(12)
        
        let messageModified: String = error.message! + "\nPlease press here to retry"
        
        self.makeToast(messageModified, duration: 60.0, position: .bottom, title: nil, image: #imageLiteral(resourceName: "toast-error"), style: style, completion: complete)
    }
}


extension UISearchBar {
    
    func change(textFont : UIFont?) {
        
        for view : UIView in (self.subviews[0]).subviews {
            
            if let textField = view as? UITextField {
                textField.font = textFont
            }
        }
    } }
