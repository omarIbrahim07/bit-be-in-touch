//
//  CustomTabbarViewController.swift
//  Global On
//
//  Created by Omar Ibrahim on 10/29/20.
//  Copyright © 2020 Global On. All rights reserved.
//

import UIKit

class CustomTabbarViewController: UITabBarController {
    
    enum TabBarItems: Int, CaseIterable {
        case Discover
        case Scanner
        case BITs
        case Search
        case Settings
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        // Create Tab one
        
        let viewController = storyboard.instantiateViewController(identifier: "DiscoverViewController")

        let tabOne = UINavigationController(rootViewController: viewController)
        let tabOneBarItem = UITabBarItem(title: "discover label".localized, image: #imageLiteral(resourceName: "Widgets"), selectedImage: #imageLiteral(resourceName: "Widgets"))
        
        tabOne.tabBarItem = tabOneBarItem
        
        // Create Tab two
        let viewController2 = storyboard.instantiateViewController(identifier: "ScannerViewController")

        let tabTwo = UINavigationController(rootViewController: viewController2)
        let tabTwoBarItem2 = UITabBarItem(title: "scanner label".localized, image: #imageLiteral(resourceName: "Scanner"), selectedImage: #imageLiteral(resourceName: "Scanner"))
        
        tabTwo.tabBarItem = tabTwoBarItem2
        
        let viewController3 = storyboard.instantiateViewController(identifier: "HomeViewController")
        let tabThree = UINavigationController(rootViewController: viewController3)
        let tabThreeBarItem3 = UITabBarItem(title: "bits label".localized, image: UIImage(named: ""), selectedImage: UIImage(named: ""))
        
        tabThree.tabBarItem = tabThreeBarItem3

        let viewController4 = storyboard.instantiateViewController(identifier: "SearchViewController")
        let tabFour = UINavigationController(rootViewController: viewController4)
        let tabFourBarItem4 = UITabBarItem(title: "search label".localized, image: #imageLiteral(resourceName: "Search2"), selectedImage: #imageLiteral(resourceName: "Search2"))
        
        tabFour.tabBarItem = tabFourBarItem4
        
        let viewController5 = storyboard.instantiateViewController(identifier: "SettingViewController")
        let tabFive = UINavigationController(rootViewController: viewController5)
        let tabFiveBarItem5 = UITabBarItem(title: "settings label".localized, image: #imageLiteral(resourceName: "Settings2"), selectedImage: #imageLiteral(resourceName: "Settings2"))

        
        tabFive.tabBarItem = tabFiveBarItem5

        createGradientView()
        
        
        self.viewControllers = [tabOne, tabTwo, tabThree, tabFour, tabFive]
        self.selectedIndex = 2
//        self.tabBar.selectedItem = tabThreeBarItem3
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
        self.tabBar.tintColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
        self.tabBar.barTintColor = #colorLiteral(red: 0.8549019608, green: 0.3254901961, blue: 0.3607843137, alpha: 1)
        self.tabBar.unselectedItemTintColor = .white
        tabBar.isTranslucent = false
        
        selectedIndex = 2

        // Do any additional setup after loading the view.
//        self.setValue(CustomTabBar(), forKey: "tabBar")
//        self.delegate = self
//        setupTabbarItems()
//        setupTabBarUI()
//        createGradientView()
    }
        
    lazy var middleButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "neeew"), for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.8549019608, green: 0.3254901961, blue: 0.3607843137, alpha: 1)
        button.layer.cornerRadius = 25
        button.imageEdgeInsets = .init(top: 0, left: 5, bottom: 5, right: 5)
        return button
    }()
    
    lazy var gradientView: Gradient = {
        let gradien = Gradient()
        gradien.startColor = #colorLiteral(red: 0.8549019608, green: 0.3254901961, blue: 0.3607843137, alpha: 1)
        gradien.endColor = #colorLiteral(red: 0.8549019608, green: 0.3254901961, blue: 0.3607843137, alpha: 1)
        gradien.layer.cornerRadius = 30
        return gradien
    }()
    
    func setupTabBarUI() {
        
        tabBar.isTranslucent = false
        
        tabBar.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.2039215686, blue: 0.2549019608, alpha: 1)
        tabBar.tintColor = #colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1568627451, alpha: 1)
//        tabBar.unselectedItemTintColor = #colorLiteral(red: 0.6901960784, green: 0.7450980392, blue: 0.831372549, alpha: 1)
        tabBar.items?[TabBarItems.BITs.rawValue].isEnabled = false
        middleButton.addTarget(self, action: #selector(didPressMiddleButton), for: .touchUpInside)
    }
    
    private func createMiddleButton() {
        self.gradientView.addSubview(middleButton)
        middleButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            middleButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            middleButton.widthAnchor.constraint(equalToConstant: 50),
            middleButton.heightAnchor.constraint(equalToConstant: 50),
            middleButton.topAnchor.constraint(equalTo: gradientView.topAnchor, constant: 5)
        ])
    }
    
    private func createGradientView() {
        self.tabBar.addSubview(gradientView)
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            gradientView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            gradientView.widthAnchor.constraint(equalToConstant: 60),
            gradientView.heightAnchor.constraint(equalToConstant: 60),
            gradientView.topAnchor.constraint(equalTo: tabBar.topAnchor, constant: -20)
        ])
        createMiddleButton()
    }
    
    func setupTabbarItems() {
        self.viewControllers = TabBarItems.allCases.map({
            self.tabBarController?.selectedIndex = TabBarItems.BITs.rawValue
            let view = viewControllerForTabBarItem($0)
            let navigation = UINavigationController(rootViewController: view)
            navigation.setNavigationBarHidden(true, animated: true)
            return navigation
        })
    }
        
    func viewControllerForTabBarItem(_ item: TabBarItems) -> UIViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        switch item {
        case .Discover:
            let view = storyboard.instantiateViewController(withIdentifier: "DiscoverViewController")
            view.tabBarItem = tabBarItem(for: item)
            return view
        case .Scanner:
            let view = storyboard.instantiateViewController(withIdentifier: "ScannerViewController")
            view.tabBarItem = tabBarItem(for: item)
            return view
        case .BITs:
            let view = storyboard.instantiateViewController(withIdentifier: "HomeViewController")
            view.tabBarItem = tabBarItem(for: item)
            return view
        case .Settings:
            let view = storyboard.instantiateViewController(withIdentifier: "SearchViewController")
            view.tabBarItem = tabBarItem(for: item)
            return view
        case .Search:
            let view = storyboard.instantiateViewController(withIdentifier: "SettingViewController")
            view.tabBarItem = tabBarItem(for: item)
            return view
        }
    }
            
    private func tabBarItem(for item: TabBarItems) -> UITabBarItem? {
        let tabBarItem: UITabBarItem
        switch item {
        case .Discover:
            tabBarItem = .init(title: "discover label".localized, image: #imageLiteral(resourceName: "Widgets"), selectedImage: #imageLiteral(resourceName: "Widgets"))
        case .Scanner:
            tabBarItem = .init(title: "scanner label".localized, image: #imageLiteral(resourceName: "Scanner"), selectedImage: #imageLiteral(resourceName: "Scanner"))
        case .BITs:
//            tabBarItem = .init(title: "+", image: #imageLiteral(resourceName: "heart"), selectedImage: #imageLiteral(resourceName: "heart"))
            return nil
        case .Settings:
            tabBarItem = .init(title: "settings label".localized, image: #imageLiteral(resourceName: "sitting"), selectedImage: #imageLiteral(resourceName: "sitting"))
        case .Search:
            tabBarItem = .init(title: "search label".localized, image: #imageLiteral(resourceName: "Search2"), selectedImage: #imageLiteral(resourceName: "Search2"))
        }
//        tabBarItem.titlePositionAdjustment = .init(horizontal: 0, vertical: 15)
//        tabBarItem.imageInsets = .init(top: 10, left: 0, bottom: -10, right: 0)
        
        
        return tabBarItem
    }

    @objc func didPressMiddleButton(){
        print("Pressed Middle Button")
        //        coordinator.Pizza.navigate(to: .pizzaMakerOptions, with: .presentWithNavigation)
        // add pizza generator impl
        selectedIndex = TabBarItems.BITs.rawValue
        let rootView = self.viewControllers![selectedIndex] as! UINavigationController
        rootView.popToRootViewController(animated: false)
    }


}

extension CustomTabbarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if selectedIndex == TabBarItems.Discover.rawValue {
            selectedIndex = TabBarItems.Discover.rawValue
        } else if selectedIndex == TabBarItems.Settings.rawValue {
            selectedIndex = TabBarItems.Settings.rawValue
        } else if selectedIndex == TabBarItems.Search.rawValue {
            selectedIndex = TabBarItems.Search.rawValue
        } else if selectedIndex == TabBarItems.Scanner.rawValue {
            selectedIndex = TabBarItems.Scanner.rawValue
        }
            
        let rootView = self.viewControllers![selectedIndex] as! UINavigationController
        rootView.popToRootViewController(animated: false)
    }
}

