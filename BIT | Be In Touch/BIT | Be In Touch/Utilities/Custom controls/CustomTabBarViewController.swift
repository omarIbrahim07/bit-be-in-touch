//
//  CustomTabBarViewController.swift
//  7ady3rf
//
//  Created by Hassan on 1/18/19.
//  Copyright © 2019 7ady3rf. All rights reserved.
//

import UIKit

class CustomTabBarViewController: UITabBarController {
    
    let menuButton = UIButton(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMiddleButton()
    }
    
    func setupMiddleButton() {
        
//        let button = UIButton(type: .custom)
//        var toMakeButtonUp = 40
//        button.frame = CGRect(x: 0.0, y: 0.0, width: 120, height: 120)
////        button.setBackgroundImage(UIImage(named: "Add new question"), for: .normal)
////        button.setBackgroundImage(UIImage(named: "Add new question"), for: .highlighted)
//        button.setBackgroundImage(UIImage(named: "17"), for: .normal)
//        button.setBackgroundImage(UIImage(named: "17"), for: .highlighted)
//        let heightDifference: CGFloat = CGFloat(toMakeButtonUp)
//        if heightDifference < 0 {
//            button.center = tabBar.center
//        } else {
//            var center: CGPoint = tabBar.center
//            center.y = center.y - heightDifference / 0.6
//            button.center = center
//        }
//        button.addTarget(self, action: #selector(addNewQuestionAction), for:.touchUpInside)
//        view.addSubview(button)

        
//        let buttonImage = UIImage(named: "Add new question")
//        menuButton.frame = CGRect(x: 0, y: 0, width: (buttonImage?.size.width)!, height: (buttonImage?.size.height)!)
//        var menuButtonFrame = menuButton.frame
//        menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height - self.view.safeAreaInsets.bottom
//        menuButtonFrame.origin.x = self.view.bounds.width/2 - menuButtonFrame.size.width/2
//        menuButton.frame = menuButtonFrame
//        menuButton.setImage(buttonImage, for: .normal)
//        menuButton.addTarget(self, action: #selector(addNewQuestionAction), for: .touchUpInside)
//        self.view.addSubview(menuButton)
//        self.view.layoutIfNeeded()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        menuButton.frame.origin.y = self.view.bounds.height - menuButton.frame.height - self.view.safeAreaInsets.bottom
    }

//    MARK:- Actions
    @objc func addNewQuestionAction() {
        self.selectedIndex = 2
        if let tabBarItems = self.tabBar.items {
            self.tabBar(self.tabBar, didSelect: tabBarItems[2])
        }
    }

}
