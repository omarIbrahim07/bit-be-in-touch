//
//  CustomView.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 5/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import UIKit

protocol ViewButtonDelegate {
    func didButtonPressed(keyWord: String)
}

class CustomView: UIView {
    
    var ButtonDelegate: ViewButtonDelegate?

    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var tView: UIView!
    @IBOutlet weak var discoverLabel: UILabel!
    @IBOutlet weak var scannerLabel: UILabel!
    @IBOutlet weak var bitsLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var searchLabel: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        setLocalization()
        let button = UIButton(type: .custom)
        var toMakeButtonUp = 40
        button.frame = CGRect(x: self.centerView.frame.width / 2 - 25, y: self.centerView.frame.height / 2 - 50, width: 50, height: 55)
        print(self.centerView.frame.width / 2 - 25)
//        button.setBackgroundImage(UIImage(named: "Add new question"), for: .normal)
//        button.setBackgroundImage(UIImage(named: "Add new question"), for: .highlighted)
//        button.setBackgroundImage(UIImage(named: "17"), for: .normal)
//        button.setBackgroundImage(UIImage(named: "17"), for: .highlighted)
        button.setBackgroundImage(UIImage(named: "neeew"), for: .normal)
        button.setBackgroundImage(UIImage(named: "neeew"), for: .highlighted)
        let heightDifference: CGFloat = CGFloat(toMakeButtonUp)
//        if heightDifference < 0 {
//            button.center = tabBar.center
//        } else {
//            var center: CGPoint = tabBar.center
//            center.y = center.y - heightDifference / 0.6
//            button.center = center
//        }
        button.addTarget(self, action: #selector(goToHome), for:.touchUpInside)
        centerView.addSubview(button)
        tView.roundCorners([.topLeft, .topRight], radius: 30)
    }
    
    func setLocalization() {
        searchLabel.text = "search label".localized
        settingsLabel.text = "settings label".localized
        bitsLabel.text = "bits label".localized
        scannerLabel.text = "scanner label".localized
        discoverLabel.text = "discover label".localized
    }
    
    @objc func goToHome() {
        print("Home")
        didButtonPressed(keyWord: "Home")
    }
    
    //MARK:- Delegate Helpers
    func didButtonPressed(keyWord: String) {
        if let delegateValue = ButtonDelegate {
            delegateValue.didButtonPressed(keyWord: keyWord)
        }
    }

    @IBAction func discoverButtonIsPressed(_ sender: Any) {
        print("Discover")
        didButtonPressed(keyWord: "Discover")
    }
    
    @IBAction func serchButtonIsPressed(_ sender: Any) {
        print("Search")
        didButtonPressed(keyWord: "Search")
    }
    
    @IBAction func homeButtonIsPressed(_ sender: Any) {
        print("Home Home Home")
        didButtonPressed(keyWord: "Home")
    }
    
    @IBAction func settingsButtonIsPressed(_ sender: Any) {
        print("Settings")
        didButtonPressed(keyWord: "Settings")
    }
    
    @IBAction func scannerButtonIsPressed(_ sender: Any) {
        print("Scanner")
        didButtonPressed(keyWord: "Scanner")
    }
}
