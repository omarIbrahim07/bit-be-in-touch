//
//  View.swift
//  Maktbty
//
//  Created by Gado on 6/12/19.
//  Copyright © 2019 Gado. All rights reserved.
//

import UIKit

//protocol ViewButtonDelegate {
//    func didButtonPressed(keyWord: String)
//}

protocol ViewSoundDelegate {
//    func didSoundDisapeared(keyWord: String, bookDetils: BookDetails)
}

class ViewContainingPlaySoundUIToolbar: UIView {

//    var isPlaying = false
//    var isMuting = false
    
    var isPlaying: Bool?
    var isMuting: Bool?


    
//    var bookDetails: BookDetails?
    var mute: String?
    var play: String?
    
    var ButtonDelegate: ViewButtonDelegate?
    var BookButtonDelegate: ViewSoundDelegate?
    
    @IBOutlet weak var tView: UIView!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var playButton: UIBarButtonItem!
    @IBOutlet weak var muteButton: UIBarButtonItem!
    @IBOutlet weak var firstItem: UIBarButtonItem!
    
//    //MARK:- Delegate Helpers
//    func didButtonPressed(keyWord: String) {
//        if let delegateValue = ButtonDelegate {
//            delegateValue.didButtonPressed(keyWord: keyWord)
//        }
//    }
    
    //MARK:- Delegate Helpers
//    func didSoundDisapeared(keyWord: String, bookDetils: BookDetails) {
//        if let delegateValue = BookButtonDelegate {
//            delegateValue.didSoundDisapeared(keyWord: keyWord, bookDetils: bookDetails!)
//        }
//    }
    
    override func awakeFromNib()
    {
        self.toolBar.setBackgroundImage(UIImage(),
                                        forToolbarPosition: .any,
                                        barMetrics: .default)
        self.toolBar.setShadowImage(UIImage(), forToolbarPosition: .any)
    //playButton.imageInsets = UIEdgeInsets(top: 10, left:10, bottom: 10, right: 10)
        playButton.width = 10
        tView.roundCorners([.topLeft, .topRight], radius: 35)
    }
    

    func setOnMute()
    {
//        if let book = self.bookDetails {
//            didSoundDisapeared(keyWord: "Mute", bookDetils: book)
//        } else {
//            return
//        }
//        isMuting = true
//
//        let myImage = UIImage(named: "01_0001_Rectangle-1")
//        muteButton.image = myImage
//        print("Mute is pressed")
        
    }
    
    func setOnUnMute()
    {
//        if let book = self.bookDetails {
//            didSoundDisapeared(keyWord: "Unmute", bookDetils: book)
//        } else {
//            return
//        }
//        isMuting = false
//
//        let myImage = UIImage(named: "sma3a")
//        muteButton.image = myImage
//        print("Unmute is pressed")
    }
    
    func setOnPause()
    {
//        if let book = self.bookDetails {
//            didSoundDisapeared(keyWord: "Pause", bookDetils: book)
//        } else {
//            return
//        }
//        isPlaying = false
//
//        let myImage = UIImage(named: "play-button (4)")
//        playButton.image = myImage
//        print("Pause is pressed")
    }
    
    func setOnPlay()
    {
//        if let book = self.bookDetails {
//            didSoundDisapeared(keyWord: "Play", bookDetils: book)
//        } else {
//            return
//        }
//        isPlaying = true
//
//        let myImage = UIImage(named: "pause5")
//        playButton.image = myImage
//        print("Play is pressed")
    }
    
    
//    @IBAction func playButtonTapped(_ sender: Any) {
//        if isPlaying == true
//        {
//            setOnPause()
//        } else {
//            setOnPlay()
//        }
//    }
//
//
//    @IBAction func muteButtonTapped(_ sender: Any) {
//        if isMuting == true
//        {
//            setOnUnMute()
//
//        } else {
//            setOnMute()
//        }
//    }
//
//    @IBAction func homeButtonPressed(_ sender: Any) {
//        print("Home is pressed")
//        didButtonPressed(keyWord: "Home")
//    }
//
//    @IBAction func authorsBittonIsPressed(_ sender: Any) {
//        print("Authors is pressed")
//        didButtonPressed(keyWord: "Author")
//    }
//
//    @IBAction func favouritesButtonIsPressed(_ sender: Any) {
//        print("Favourite is pressed")
//        didButtonPressed(keyWord: "Favourite")
//    }
}
