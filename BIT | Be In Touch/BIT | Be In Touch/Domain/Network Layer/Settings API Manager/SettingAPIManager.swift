//
//  SettingAPIManager.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class SettingAPIManager: BaseAPIManager {
    
    func createBITSettings(countriesArray: [String], arrayKey: String, basicDictionary params:APIParams , onSuccess: @escaping (Setting)->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .post, path: CREATE_BIT_SETTINGS_URL, parameters: params)

        super.performNetworkRequestWithFormStringArray(array: countriesArray, arrayKey: arrayKey, forRouter: engagementRouter, onSuccess: { (responseObject) in
                
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<Setting>().map(JSON: json) {
            onSuccess(wrapper)
        }

        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "bit settings api error".localized
            }
            onFailure(apiError)
        }
    }
        
    func updateBITSettings(countriesArray: [String], arrayKey: String, basicDictionary params:APIParams , onSuccess: @escaping (Setting)->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .post, path: UPDATE_BIT_SETTINGS_URL, parameters: params)

    super.performNetworkRequestWithFormStringArray(array: countriesArray, arrayKey: arrayKey, forRouter: engagementRouter, onSuccess: { (responseObject) in

        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<Setting>().map(JSON: json) {
            onSuccess(wrapper)
        }

        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "bit settings api error".localized
            }
            onFailure(apiError)
        }
    }

    func getBITSettings(basicDictionary params:APIParams , onSuccess: @escaping (Setting)->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .post, path: GET_BIT_SETTINGS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
                
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<Setting>().map(JSON: json) {
            onSuccess(wrapper)
        }

        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "bit settings api error".localized
            }
            onFailure(apiError)
        }
    }
}
