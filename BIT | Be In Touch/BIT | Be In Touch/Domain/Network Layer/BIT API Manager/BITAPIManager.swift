//
//  BITAPIManager.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 4/26/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class BITAPIManager: BaseAPIManager {
    
    var dateRequired: String?
    var twoDaysDateRequired: String?
    
    func deleteBIT(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: DELETE_BIT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: Bool = response["deleted"] as? Bool {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "Bit isn't deleted".localized
            }
            onFailure(apiError)
        }
    }
    
    func reportBIT(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: REPORT_BIT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: Bool = response["saved"] as? Bool {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "Bit isn't reported".localized
            }
            onFailure(apiError)
        }
    }

    func viewBITAdvertisement(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: VIEW_BIT_ADVERTISEMENT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: Bool = response["saved"] as? Bool {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "Bit advertisement isn't viewed".localized
            }
            onFailure(apiError)
        }
    }
    
    func openBITAdvertisement(basicDictionary params:APIParams , onSuccess: @escaping (Bool)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: OPEN_BIT_ADVERTISEMENT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let message: Bool = response["saved"] as? Bool {
                
                onSuccess(message)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "Bit advertisement isn't opened".localized
            }
            onFailure(apiError)
        }
    }

    func getHomeBITS(basicDictionary params:APIParams , onSuccess: @escaping ([BIT])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .post, path: GET_HOME_BITS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in

        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<BIT>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }

        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getSearchedBIT(basicDictionary params:APIParams , onSuccess: @escaping ([BIT])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_SEARCHED_BIT_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<BIT>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func getAllBITS(basicDictionary params:APIParams , onSuccess: @escaping ([BIT])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_ALL_BITS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<BIT>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getDiscoverBITS(basicDictionary params:APIParams , onSuccess: @escaping ([BITDiscover])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_DISCOVER_BITS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<BITDiscover>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func getDiscoverBlockBITS(api: String, basicDictionary params:APIParams , onSuccess: @escaping ([BIT])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: "bits/" + api, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<BIT>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func getTrendingBITs(basicDictionary params:APIParams , onSuccess: @escaping ([BIT])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_TRENDING_BITS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<BIT>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getBITDetails(basicDictionary params:APIParams , onSuccess: @escaping ([BITDetails])->Void, onFailure: @escaping  (APIError)->Void) {
//    func getBITDetails(basicDictionary params:APIParams , onSuccess: @escaping ([BITDetails], HeaderData?)->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_BIT_DETAILS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
//        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]], let dataData: [String : Any] = response["dataData"] as? [String : Any] {
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<BITDetails>().mapArray(JSONArray: jsonArray)
//            onSuccess(wrapper, wrapper2)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
//    func getBITDetails(basicDictionary params:APIParams , onSuccess: @escaping (BIT)->Void, onFailure: @escaping  (APIError)->Void) {
//
//    let engagementRouter = BaseRouter(method: .get, path: GET_BIT_DETAILS_URL, parameters: params)
//
//    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//
//        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["bitData"] as? [String : Any], let wrapper = Mapper<BIT>().map(JSON: json) {
//            onSuccess(wrapper)
//        }
//
//        else {
//            let apiError = APIError()
//            onFailure(apiError)
//        }
//
//        }) { (apiError) in
//            if apiError.responseStatusCode == 3 {
//                apiError.message = "search bit api error".localized
//            }
//            onFailure(apiError)
//        }
//    }
    
    func getAllVegetables(basicDictionary params:APIParams , onSuccess: @escaping ([Vegetable])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_VEGETABLES_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<Vegetable>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getAllVegetablesPrices(basicDictionary params:APIParams , onSuccess: @escaping ([VegetablePrice])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "dd-MM-yyyy"
        let formattedDate = format.string(from: date)
        // for arabic date
        let confirmFormateDate = formattedDate.withWesternNumbers
        print(confirmFormateDate)
        
        let newPath: String = GET_VEGETABLES_PRICE_URL + confirmFormateDate
        print(newPath)

    let engagementRouter = BaseRouter(method: .get, path: newPath, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<VegetablePrice>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getCurrencies(basicDictionary params:APIParams , onSuccess: @escaping ([CurrencySymbols])->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_CURRENCIES_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<CurrencySymbols>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getCurrencyDetails(symbol: String, basicDictionary params:APIParams , onSuccess: @escaping ([CurrencyDetails])->Void, onFailure: @escaping  (APIError)->Void) {

        let getCurrenciesDetailsURL = GET_CURRENCY_DETAILS_URL + symbol
        
    let engagementRouter = BaseRouter(method: .get, path: getCurrenciesDetailsURL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<CurrencyDetails>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getGoldRates(country: String, basicDictionary params:APIParams , onSuccess: @escaping ([GoldRate])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let getGoldRateURL = GET_GOLD_RATES_URL + country

    let engagementRouter = BaseRouter(method: .get, path: getGoldRateURL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
        
        if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
            let wrapper = Mapper<GoldRate>().mapArray(JSONArray: jsonArray)
            onSuccess(wrapper)
        }
            
        else {
            let apiError = APIError()
            onFailure(apiError)
        }
        
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getPrayerTimings(basicDictionary params:APIParams , onSuccess: @escaping (PrayerTimings)->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_PRAYER_TIMINGS_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
                
        if let response: [String : Any] = responseObject as? [String : Any], let data: [String : Any] = response["data"] as? [String : Any] , let json: [String : Any] = data["timings"] as? [String : Any], let wrapper = Mapper<PrayerTimings>().map(JSON: json) {
            onSuccess(wrapper)
        }
        
        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func getCurrencyRates(basicDictionary params:APIParams , onSuccess: @escaping (CurrencyRates)->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_CURRENCY_RATES_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
                
        if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["rates"] as? [String : Any], let wrapper = Mapper<CurrencyRates>().map(JSON: json) {
            onSuccess(wrapper)
        }
        
        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getWeather(basicDictionary params:APIParams , onSuccess: @escaping (WeatherJSON)->Void, onFailure: @escaping  (APIError)->Void) {

    let engagementRouter = BaseRouter(method: .get, path: GET_WEATHER_URL, parameters: params)

    super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
                
        if let response: [String : Any] = responseObject as? [String : Any], let wrapper = Mapper<WeatherJSON>().map(JSON: response) {
            onSuccess(wrapper)
        }
        
        else {
            let apiError = APIError()
            onFailure(apiError)
        }

        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func getPreviousDay() {
        
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        let yesterdayString = yesterday?.toString(dateFormat: "MM/dd/yyyy")
        print(yesterdayString)
        
        let y = yesterdayString?.withWesternNumbers
        
        let year: String = y!.substring(with: 8..<10)
        let day: String = y!.substring(with: 3..<5)
                
        if y!.first == "0" {
            let month: String = y!.substring(with: 1..<2)
            
            let datee = y!.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
            let newDate = "\(month)/\(day)/\(year)"
            dateRequired = newDate
            print(newDate)
        } else {
            let month: String = y!.substring(with: 0..<2)
            
            let datee = y!.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
            let newDate = "\(month)/\(day)/\(year)"
            dateRequired = newDate
            print(newDate)
        }

    }
    
    func getTwoDaysAgo() {
        
        let twoDaysAgo = Calendar.current.date(byAdding: .day, value: -2, to: Date())
        let twoDaysAgoString = twoDaysAgo?.toString(dateFormat: "MM/dd/yyyy")
        print(twoDaysAgoString)
        
        let td = twoDaysAgoString?.withWesternNumbers
        
        let year: String = td!.substring(with: 8..<10)
        let day: String = td!.substring(with: 3..<5)
                
        if td!.first == "0" {
            let month: String = td!.substring(with: 1..<2)
            let datee = td!.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
            let newDate = "\(month)/\(day)/\(year)"
            twoDaysDateRequired = newDate
            print(newDate)
        } else {
            let month: String = td!.substring(with: 0..<2)
            
            let datee = td!.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
            let newDate = "\(month)/\(day)/\(year)"
            twoDaysDateRequired = newDate
            print(newDate)
        }

    }

    
//    func getTime() {
//        let date = Date()
//        let formatter = DateFormatter()
//
//        formatter.dateFormat = "MM/dd/yyyy"
//
//        let result = formatter.string(from: date)
//
//        print(result)
//        let year: String = result.substring(with: 8..<10)
//        let day: String = result.substring(with: 3..<5)
//
//        if result.first == "0" {
//            let month: String = result.substring(with: 1..<2)
//            var prevDayInt: Int = Int(day)!
//            prevDayInt = prevDayInt - 1
//            var prevDayString: String = String(prevDayInt)
//            let datee = result.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
//            let newDate = "\(month)/\(prevDayString)/\(year)"
//            dateRequired = newDate
//            print(newDate)
//        } else {
//            let month: String = result.substring(with: 0..<2)
//            print(year)
//            print(month)
//            print(day)
//
//            let datee = result.replacingOccurrences(of: "/", with: "-", options: .literal, range: nil)
//            let newDate = "\(month)/\(day)/\(year)"
//            dateRequired = newDate
//            print(newDate)
//        }
//
//    }

    
}
