//
//  PackageAPIManager.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 5/22/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

class PackageAPIManager: BaseAPIManager {
    
//    func getDeliveryMethods(basicDictionary params:APIParams , onSuccess: @escaping ([DeliveryMethod])->Void, onFailure: @escaping  (APIError)->Void) {
//        
//        let engagementRouter = BaseRouter(method: .get, path: GET_DELIVERY_METHODS_URL, parameters: params)
//        
//        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
//            
//            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
//                let wrapper = Mapper<DeliveryMethod>().mapArray(JSONArray: jsonArray)
//                onSuccess(wrapper)
//            }
//                
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//            
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
    func getTypes(basicDictionary params:APIParams , onSuccess: @escaping ([Category])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_TYPES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<Category>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
    func getAdditionalServices(basicDictionary params:APIParams , onSuccess: @escaping ([AdditionalService])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_ADDITIONAL_SERVICES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<AdditionalService>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getAllLocations(basicDictionary params:APIParams , onSuccess: @escaping ([Location])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_ALL_LOCATIONS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<Location>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getFeedbackElements(basicDictionary params:APIParams , onSuccess: @escaping ([FeedbackItem])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_FEEDBACK_ELEMENTS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<FeedbackItem>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func addPackage(basicDictionary params:APIParams , onSuccess: @escaping (Int)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: POST_ORDER_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let postID: Int = response["order_id"] as? Int {
                onSuccess(postID)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func confirmOrder(basicDictionary params:APIParams , onSuccess: @escaping ()->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: POST_ORDER_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any] {
                onSuccess()
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func addPackageLocation(basicDictionary params:APIParams , onSuccess: @escaping (Bool) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: POST_SHIPPING_LOCATION_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
                onSuccess(saved)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    func addExtraServices(basicDictionary params:APIParams , onSuccess: @escaping (String) -> Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: POST_ORDERS_EXTRA_SERVICES_URL, parameters: params)

        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let orderPrice: String = response["order_price"] as? String {
                onSuccess(orderPrice)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func addExtraServicesFloat(basicDictionary params:APIParams , onSuccess: @escaping (Float) -> Void, onFailure: @escaping  (APIError)->Void) {

        let engagementRouter = BaseRouter(method: .post, path: POST_ORDERS_EXTRA_SERVICES_URL, parameters: params)

        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in

            if let response: [String : Any] = responseObject as? [String : Any], let orderPrice: Float = response["order_price"] as? Float {
                onSuccess(orderPrice)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }

        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getShippingLocations(basicDictionary params:APIParams , onSuccess: @escaping ([ShippingLocation])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_SHIPPING_LOCATIONS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<ShippingLocation>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func evaluateServices(basicDictionary params:APIParams , onSuccess: @escaping (Bool) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: RATE_SERVICES_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
                onSuccess(saved)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func rateDriver(basicDictionary params:APIParams , onSuccess: @escaping (Bool) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: RATE_DRIVER_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
                onSuccess(saved)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getMyPackages(basicDictionary params:APIParams , onSuccess: @escaping ([Package])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_MY_PACKAGES_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<Package>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    
    func getPackage(basicDictionary params:APIParams , onSuccess: @escaping (Package) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_PACKAGE_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let data = response["data"] as? [String : Any], let wrapper = Mapper<Package>().map(JSON: data) {
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getTrackedPackages(basicDictionary params:APIParams , onSuccess: @escaping ([OrderId])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_TRACKED_PAKAGES, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["orders"] as? [[String : Any]] {
                let wrapper = Mapper<OrderId>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func getNotifications(basicDictionary params:APIParams , onSuccess: @escaping ([Notificationn])->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_NOTIFICATIONS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let jsonArray: [[String : Any]] = response["data"] as? [[String : Any]] {
                let wrapper = Mapper<Notificationn>().mapArray(JSONArray: jsonArray)
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }
    
    func setNotificationSetting(basicDictionary params:APIParams , onSuccess: @escaping (Bool) -> Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .post, path: SET_NOTIFICATION_SETTING_FOR_BIT_URL, parameters: params)
        
        super.performNetworkRequestWithFormData(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let saved: Bool = response["saved"] as? Bool {
                onSuccess(saved)
            }
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            onFailure(apiError)
        }
    }

    
//    func add(array: [Int], basicDictionary params:APIParams ,onSuccess: @escaping (Float) -> Void, onFailure: @escaping  (APIError)->Void) {
//
//            let engagementRouter = BaseRouter(method: .post, path: POST_ORDERS_EXTRA_SERVICES_URL, parameters: params)
//        let key = "services"
//
//        super.performNetworkRequestWithFormDataArrayAndData(array: array, arrayKey: key,forRouter: engagementRouter, onSuccess: { (responseObject) in
//            if let response: [String : Any] = responseObject as? [String : Any], let orderPrice = response["order_price"] as? Float {
//                onSuccess(orderPrice)
//            }
//            else {
//                let apiError = APIError()
//                onFailure(apiError)
//            }
//        }) { (apiError) in
//            onFailure(apiError)
//        }
//    }
    
}
