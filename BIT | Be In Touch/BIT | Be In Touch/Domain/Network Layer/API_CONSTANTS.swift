//
//  API_CONSTANTS.swift
//  GameOn
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import Foundation

let MainURL = "https://app.beintouch.club/api/v1/backend/"
//let MainURL = "https:/"
//let GET_ALL_BITS_URL = "api.beintouch.club/api/v1/backend/bits"
//let DELETE_BIT_URL = "api.beintouch.club/api/v1/backend/bitsfav/delete"
//let GET_BIT_DETAILS_URL = "api.beintouch.club/api/v1/backend/bitdetails"
//let GET_HOME_BITS_URL = "api.beintouch.club/api/v1/backend/bitsfav"
//let GET_BIT_SETTINGS_URL = "api.beintouch.club/api/v1/backend/bitsetting"
//let CREATE_BIT_SETTINGS_URL = "api.beintouch.club/api/v1/backend/bitsetting/store"
//let UPDATE_BIT_SETTINGS_URL = "api.beintouch.club/api/v1/backend/bitsetting/update"
//let GET_CURRENCIES_URL = "api.beintouch.club/api/v1/backend/currencies"
//let GET_CURRENCY_DETAILS_URL = "api.beintouch.club/api/v1/backend/currenciesDays/"
//let GET_GOLD_RATES_URL = "api.beintouch.club/api/v1/backend/golds/"
//let GET_SEARCHED_BIT_URL = "api.beintouch.club/api/v1/backend/bits/home"

let GET_ALL_BITS_URL = "bits"
let DELETE_BIT_URL = "bitsfav/delete"
let REPORT_BIT_URL = "bits/report"
let VIEW_BIT_ADVERTISEMENT_URL = "bit/ads/views"
let OPEN_BIT_ADVERTISEMENT_URL = "bit/ads/views"
let GET_BIT_DETAILS_URL = "bitdetails"
let GET_HOME_BITS_URL = "bitsfav"
let GET_DISCOVER_BITS_URL = "bits/discover"
let GET_BIT_SETTINGS_URL = "bitsetting"
let CREATE_BIT_SETTINGS_URL = "bitsetting/store"
let UPDATE_BIT_SETTINGS_URL = "bitsetting/update"
let GET_CURRENCIES_URL = "currencies"
let GET_CURRENCY_DETAILS_URL = "currenciesDays/"
let GET_GOLD_RATES_URL = "golds/"
let GET_SEARCHED_BIT_URL = "bits/home"
let GET_NOTIFICATIONS_URL = "bitsfav/notifications"
let GET_TRENDING_BITS_URL = "bits/trending"
let SET_NOTIFICATION_SETTING_FOR_BIT_URL = "bitsfav/update"
let GET_TUTORIALS_URL = "tutorials"
let GET_TERMS_AND_CONDITIONS_URL = "staticpages/1"

let ImageURLVegetables = "https://app.beintouch.club/uploads/gitems/"
let BIT_IMAGE_URL = "https://app.beintouch.club/uploads/bits/"
let BIT_ITEMS_IMAGE_URL = "https://app.beintouch.club/uploads/bitItems/"
let CURRENCY_ITEMS_IMAGE_URL = "https://app.beintouch.club/uploads/Citems/"
let DYNAMIC_LINKS_IMAGE_URL = "https://app.beintouch.club/uploads/social/"
let TUTORIALS_IMAGE_URL = "https://app.beintouch.club/uploads/tutorials/"


//let ImageURLVegetables = "https://app.beintouch.club/uploads/gitems/"
//let BIT_IMAGE_URL = "http://api.beintouch.club/uploads/bits/"
//let BIT_ITEMS_IMAGE_URL = "https://api.beintouch.club/uploads/bitsitems/"
//let DYNAMIC_LINKS_IMAGE_URL = "http://api.beintouch.club/uploads/social/"


let GET_VEGETABLES_URL = "app.beintouch.club/api/v1/backend/prices/items"
let GET_VEGETABLES_PRICE_URL = "app.beintouch.club/api/v1/backend/prices/days/"
let GET_PRAYER_TIMINGS_URL = "api.aladhan.com/v1/timingsByCity"
let GET_CURRENCY_RATES_URL = "metals-api.com/api/latest"
let GET_WEATHER_URL = "api.openweathermap.org/data/2.5/weather"

/* OOOOM APIS */
/* Old */

//let MainURL = "https:/"
//let ImageURLServices = "http://app.oooomapp.com/uploads/services/"
//let ImageURL_USERS = "http://app.oooomapp.com/uploads/users/"
//let ImageURLWorkerWorks = "http://app.oooomapp.com/uploads/workerWork/"
//let ImageURLOffers = "http://app.oooomapp.com/uploads/offers/"
//let ImageURLOrders = "https://app.rawa-sa.com/uploads/orders/"
//
//
//let GET_PRAYER_TIMINGS_URL = "api.aladhan.com/v1/timingsByCity"
//let GET_CURRENCY_RATES_URL = "metals-api.com/api/latest"
//let GET_WEATHER_URL = "api.openweathermap.org/data/2.5/weather"




let GET_SERVICES_URL = "services"
let LOGIN_URL = "login"
let GET_USER_URL = "user"
let REGISTER_CLIENT_URL = "register"
let SEND_VERIFICATION_CODE_URL = "verify"
let CHECK_VERIFICATION_CODE_URL = "verify"
let FORGET_PASSWORD_URL = "forgetpassword"
let CHANGE_PASSWORD_FROM_LOGIN_URL = "forgetpassword"
let CHANGE_PASSWORD_URL = "changepassword"
let GET_CITIES_URL = "areas"
let GET_ITEMS_URL = "items"
let GET_TECHNICIANS_URL = "workers"
let GET_MENTINANCE_ORDERS_URL = "orders"
let GET_ORDER_DETAILS_URL = "orders"
let SUBMIT_ACTION_ON_ORDER_URL = "orders/save"
let GET_BIDS_URL = "bids"
let GET_BID_OFFERS_URL = "bidoffers"
let GET_STATIC_PAGE_URL = "staticpages"
let SEND_ORDER_URL = "orders/save"
let SEND_BID_URL = "bids/save"
let SEND_FEEDBACK_URL = "feedback/save"
let RATE_ORDER_URL = "orders/save"
let SEND_PROMOCODE_URL = "promocodes/save"
let GET_PROMOCODE_URL = "promocodes"
let EDIT_PROFILE_URL = "edituser"
let GET_OFFERS_URL = "offers"
let GET_OFFER_DETAILS_URL = "offers"

let GET_FILE_UPLOAD_URL = "files/save"

let GET_ROOM_ID_URL = "chatrooms/save"
let GET_LIST_OF_CHATS_URL = "chatrooms"


























let GET_ADDITIONAL_SERVICES_URL = "additionalServices"
let GET_ALL_LOCATIONS_URL = "locations"
let GET_ABOUT_URL = "staticPages/"
let GET_FEEDBACK_ELEMENTS_URL = "feedbackItems"
let GET_TYPES_URL = "categories"
let POST_ORDER_URL = "orders/save"
let POST_ORDERS_EXTRA_SERVICES_URL = "orders/addservices"
let POST_SHIPPING_LOCATION_URL = "trackings/save"

let GET_POSTS = ""

let REGISTER_URL = "register"
//let LOGIN_URL = "login"
let LOGOUT_URL = "logout"
//let GET_USER_URL = "profile"
//let EDIT_PROFILE_URL = "editprofile"

let GIVE_FEEDBACK = "appFeedbacks/save"
let GET_SHIPPING_LOCATIONS_URL = "locations"

let RATE_DRIVER_URL = "driverRatings/save"
let RATE_SERVICES_URL = "orderFeedbacks/save"

let GET_MY_PACKAGES_URL = "orders"
let GET_PACKAGE_URL = "orders"
let GET_TRACKED_PAKAGES = "serviceorders"

let GET_NOTIFICATIONS = "notifications"

/*********************************************************************************/

let GET_MAIN_CATEGORIES_URL = "cats"
let GET_SUB_CATEGORIES_URL = "subcats/"
let GET_COUNTRIES_URL = "countries"
//let GET_CITIES_URL = "cites/"
let GET_SECONDARY_CATEGORIES_URL = "ads/cat/"
let GET_SPECIAL_SECONDARY_CATEGORIES_URL = "ads/featured/cat/"
let GET_ADVERTISEMENT_URL = "ad/"

let GET_ALL_ADVERTISEMENTS = "ads"
let GET_ALL_SPECIAL_ADVERTISEMENTS = "ads/featured"
let GET_ALL_MY_ADVERTISEMENTS = "ads/user/"
let GET_ALL_MY_SPECIAL_ADVERTISEMENTS = "ads/featured/user/"
let GET_SIMILAR_ADVERTISEMENTS_URL = "similarads/ad/"
let GET_SAVED_ADVERTISEMENTS_URL = "user/savedads/"
let GET_USER_ADVERTISEMENT_BY_USER_ID = "ads/user/"
let GET_SOLD_ADVERTISEMENTS_URL = "ads/sold/user/"
let GET_SEARCHED_ADVERTISEMENTS_URL = "search"

let registerURL = "register"
let getUserURL = "user"

let FIRST_LEVEL_POST_ADVERTISEMENT_URL = "postad"
let EDIT_FIRST_LEVEL_POST_ADVERTISEMENT_URL = "editad/"
//let CHANGE_PASSWORD_URL = "user/changepassword"
let CHANGE_USER_PROFILE_URL = "user/editprofile"
let SAVE_POST_URL = "ads/savead"
let CHECK_SAVED_POST_URL = "ads/issaved"
let DELETE_POST_URL = "ad/delete/"
let GET_POST_TYPES_URL = "posttypes"
let SELL_ADVERTISEMENT_URL = "setsold/ad/"

let GET_ORDERS_URL = "orders"

let SAVE_AD_PICS = "postpics"
let GET_MAIN_BANNER_URL = "mainbanner"

let GET_MESSAGES_URL = "messages"
let GET_CHAT_URL = "chat"
let GET_CHAT_MESSAGES_URL = "chat"
let TEXT_CHAT_MESSAGE_URL = "sendmsg"

let GET_INTERESTS_URL = "get-interests"
let GET_POSTS_URL = "get-posts"
//let LOGOUT_URL = "logout"
let ADD_NEW_POST_URL = "add-post"
let GET_NOTIFICATION_URL = "get-notifications"
let GET_READ_POST_URL = "get-read-post"
let ADD_FOLLOW_POST = "add-follow-post"
let DELETE_POST = "delete-post"
let REPORT_POST = "add-feedback-post"
let DELETE_COMMENT = "delete-comment-post"
let DELETE_REPLY = "delete-comment-replies-post"
let LIKE_COMMENT = "add-like-comment-post"
let LIKE_REPLY = "add-like-comment-replies-post"
let GET_PROFILE_DATA = "profile"

