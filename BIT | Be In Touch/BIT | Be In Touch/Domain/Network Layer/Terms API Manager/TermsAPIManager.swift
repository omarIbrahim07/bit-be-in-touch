//
//  TermsAPIManager.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 10/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class TermsAPIManager: BaseAPIManager {
    
    func getTermsAndConditions(basicDictionary params:APIParams , onSuccess: @escaping (Terms)->Void, onFailure: @escaping  (APIError)->Void) {
        
        let engagementRouter = BaseRouter(method: .get, path: GET_TERMS_AND_CONDITIONS_URL, parameters: params)
        
        super.performNetworkRequest(forRouter: engagementRouter, onSuccess: { (responseObject) in
            
            if let response: [String : Any] = responseObject as? [String : Any], let json: [String : Any] = response["data"] as? [String : Any], let wrapper = Mapper<Terms>().map(JSON: json) {
                onSuccess(wrapper)
            }
                
            else {
                let apiError = APIError()
                onFailure(apiError)
            }
            
        }) { (apiError) in
            if apiError.responseStatusCode == 3 {
                apiError.message = "bit settings api error".localized
            }
            onFailure(apiError)
        }
    }
    
}
