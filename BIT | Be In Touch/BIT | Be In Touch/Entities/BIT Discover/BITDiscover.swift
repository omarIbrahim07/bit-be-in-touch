//
//  BITDiscover.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/28/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class BITDiscover: Mappable {
    
    var blockName: String?
    var api: String?
    var bits: [BIT]?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        blockName <- map["name"]
        api <- map["api"]
        bits <- map["bits"]
    }
    
}
