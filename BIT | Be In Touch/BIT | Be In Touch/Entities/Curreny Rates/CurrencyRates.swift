//
//  CurrencyRates.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 4/28/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrencyRates: Mappable {
    
    var usd: Float?
    var eur : Float?
    var sar : Float?
    var gbp: Float?
    var kwd : Float?
    var aed : Float?
    var cny : Float?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        usd <- map["USD"]
        eur <- map["EUR"]
        sar <- map["SAR"]
        gbp <- map["GBP"]
        kwd <- map["KWD"]
        aed <- map["AED"]
        cny <- map["CNY"]
    }
}
