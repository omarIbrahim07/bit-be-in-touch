//
//  User.swift
//  Blabber
//
//  Created by Hassan on 12/22/17.
//  Copyright © 2017 Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject, Mappable, NSCoding {
    
    var id : Int?
    var firstName : String?
    var lastName : String?
    var email : String?
    var phone : String?
    var address: String?
    var image : String?
    var isVerified: Int?
    var cityID: Int?
//    var area: String?
//    var areaEn: String?
    
    required override init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        email <- map["email"]
        phone <- map["phone"]
        address <- map["address"]
        isVerified <- map["is_verified"]
        image <- map["image"]
        cityID <- map["city_id"]
//        area <- map["area_name"]
//        areaEn <- map["area_nameEN"]
    }
    
    //MARK: - NSCoding -
    required init(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObject(forKey: "id") as? Int
        self.firstName = aDecoder.decodeObject(forKey: "first_name") as? String
        self.lastName = aDecoder.decodeObject(forKey: "last_name") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        self.phone = aDecoder.decodeObject(forKey: "phone") as? String
        self.address = aDecoder.decodeObject(forKey: "address") as? String
        self.isVerified = aDecoder.decodeObject(forKey: "is_verified") as? Int
        self.image = aDecoder.decodeObject(forKey: "image") as? String
        self.cityID = aDecoder.decodeObject(forKey: "city_id") as? Int
//        self.area = aDecoder.decodeObject(forKey: "area_name") as? String
//        self.areaEn = aDecoder.decodeObject(forKey: "area_nameEN") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(firstName, forKey: "first_name")
        aCoder.encode(lastName, forKey: "last_name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(isVerified, forKey: "is_verified")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(cityID, forKey: "city_id")
//        aCoder.encode(areaEn, forKey: "area_nameEN")
//        aCoder.encode(area, forKey: "area_name")
    }
    
}
