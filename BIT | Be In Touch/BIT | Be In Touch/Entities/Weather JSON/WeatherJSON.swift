//
//  WeatherJSON.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 4/29/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class WeatherJSON: Mappable {
    
    var weather: [Weather]?
    var main : Main?
    var wind : Wind?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        weather <- map["weather"]
        main <- map["main"]
        wind <- map["wind"]
    }
}

class Weather: Mappable {
    
    var main: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        main <- map["main"]
    }
}

class Main: Mappable {
    
    var temp: Float?
    var pressure : Float?
    var humidity : Float?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        temp <- map["temp"]
        pressure <- map["pressure"]
        humidity <- map["humidity"]
    }
}

class Wind: Mappable {
    
    var speed: Float?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        speed <- map["speed"]
    }
}
