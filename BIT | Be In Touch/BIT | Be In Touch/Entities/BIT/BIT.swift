//
//  BIT.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class BIT: Mappable {
        
    var id: Int?
    var specialSceinario: Int?
    var name : String?
    var nameEn : String?
    var title: String?
    var bitCode: String?
    var websiteLink: String?
    var ownerName: String?
    var country: String?
    var createdAt: String?
    var updatedAt: String?
    var active : Int?
    var defaultTemplate : Int?
    var image: String?
    var countries: [String]?
    var languages: [String]?
    var description: String?
    var dynamicLinks: [DynamicLink]?
    var category: String?
    var ads: Int?
    var sorting2: Int?
    var sorting3: Int?
//        "user_id": 1,
//        "status": 1,

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["nameAR"]
        nameEn <- map["nameEN"]
        title <- map["title"]
//        title <- map["name"]
        image <- map["image"]
        bitCode <- map["bit_code"]
        websiteLink <- map["website_link"]
        ownerName <- map["owner_name"]
        country <- map["country"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        active <- map["active"]
        defaultTemplate <- map["default_template"]
        countries <- map["countries"]
        languages <- map["languages"]
        description <- map["description"]
        specialSceinario <- map["scenario"]
        dynamicLinks <- map["dynamiclinks"]
        category <- map["category"]
        ads <- map["ads"]
        sorting2 <- map["sorting2"]
        sorting3 <- map["sorting3"]
    }
}

class DynamicLink: Mappable {
    
    var link: String?
    var type : String?
    var image : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        link <- map["link"]
        type <- map["type"]
        image <- map["image"]
    }
}
