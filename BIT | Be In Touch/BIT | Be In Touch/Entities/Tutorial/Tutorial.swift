//
//  Tutorial.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 9/22/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Tutorial: Mappable {
        
    var tutorialTitle: String?
    var image: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        tutorialTitle <- map["title"]
        image <- map["image"]
    }
}
