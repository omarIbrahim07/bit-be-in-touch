//
//  Notification.swift
//  El-Zafer
//
//  Created by Omar Ibrahim on 7/29/19.
//  Copyright © 2019 Egy designer. All rights reserved.
//

import Foundation
import ObjectMapper

class Notificationn: Mappable {
    
//    var id: Int?
//    var name : String?
//    var nameEn : String?
    var title: String?
    var bitCode: String?
    var notificationFlag: Int?
    var image: String?
    var ownerName: String?
    var category: String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
//        id <- map["id"]
//        name <- map["nameAR"]
//        nameEn <- map["nameEN"]
        title <- map["title"]
        image <- map["image"]
        bitCode <- map["bit_code"]
        ownerName <- map["owner_name"]
        notificationFlag <- map["notification"]
        category <- map["category"]
    }
    
}
