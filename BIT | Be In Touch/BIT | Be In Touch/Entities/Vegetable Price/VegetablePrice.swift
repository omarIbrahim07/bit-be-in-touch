//
//  VegetablePrice.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 6/3/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class VegetablePrice: Mappable {
    
    var id: Int?
    var date : String?
    var itemId : Int?
    var price : String?
    var itemNameAR : String?
    var itemNameEN : String?
    var unit : Int?
    var image : String?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        date <- map["date"]
        itemId <- map["item_id"]
        price <- map["price"]
        itemNameAR <- map["itemNameAR"]
        itemNameEN <- map["itemNameEN"]
        unit <- map["unit"]
        image <- map["image"]
    }
}
