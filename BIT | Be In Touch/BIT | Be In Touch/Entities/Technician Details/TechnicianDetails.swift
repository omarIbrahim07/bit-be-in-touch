//
//  TechnicianDetails.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/13/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class TechnicianDetails : Mappable {
    
    var age : Int?
    var rating : Float?
    var price : Float?
    var image: String?
    var name : String?
    var id: Int?
    var service: String?
    var from: String?
    var fromEn: String?
    var area: String?
    var areaEn: String?
    var completedOrders: Int?
    var inspectionExpenses: Int?
    var workerWorks: [Work]?
    var workerOrders: [Order]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        age <- map["age"]
        rating <- map["rating"]
        price <- map["price"]
        image <- map["image"]
        name <- map["name"]
        id <- map["user_id"]
        service <- map["service"]
        completedOrders <- map["orders"]
        inspectionExpenses <- map["worker_price"]
        from <- map["from"]
        fromEn <- map["fromEN"]
        area <- map["area"]
        areaEn <- map["areaEN"]
        workerWorks <- map["worker_works"]
        workerOrders <- map["worker_orders"]
    }
    
}

class Work : Mappable {
    
    var image: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        image <- map["image"]
    }
    
}

class Client: Mappable {
    
    var firstName: String?
    var lastName: String?
    var image: String?
   
   required init?(map: Map) {
       
   }
   
   func mapping(map: Map) {
       firstName <- map["first_name"]
       lastName <- map["last_name"]
       image <- map["image"]
   }
    
}
