//
//  BITTest.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 11/9/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation

struct bTest: Codable {
    
    var bitTestArray: [BITTest]?
    
    enum CodingKeys: String, CodingKey {
        case bitTestArray = "data"
    }
}

struct BITTest: Codable {
        
    var id: Int?
    var specialSceinario: Int?
    var name : String?
    var nameEn : String?
    var title: String?
    var bitCode: String?
    var websiteLink: String?
    var ownerName: String?
    var country: String?
    var createdAt: String?
    var updatedAt: String?
    var active : Int?
    var defaultTemplate : Int?
    var image: String?
    var countries: [String]?
    var languages: [String]?
    var description: String?
    var dynamicLinks: [DynamicoLink]?
    var category: String?
    
    enum CodingKeys: String, CodingKey {
        case id, title, image, country, active, countries, languages, description, category
        case name = "nameAR"
        case nameEn = "nameEN"
        case bitCode = "bit_code"
        case websiteLink = "website_link"
        case ownerName = "owner_name"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case defaultTemplate = "default_template"
        case specialSceinario = "scenario"
        case dynamicLinks = "dynamiclinks"
    }
}

struct DynamicoLink: Codable {
    
    var link: String?
    var type : String?
    var image : String?
        
}
