//
//  CurrencyDetails.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/19/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrencyDetails: Mappable {
        
    var id: Int?
    var bankID : Int?
    var currencyID: Int?
    var buyPrice: String?
    var sellPrice: String?
    var date: String?
    var createdAt: String?
    var bankNameAR: String?
    var bankNameEN: String?
    var currencyNameAR: String?
    var currencyNameEN: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        bankID <- map["bank_id"]
        currencyID <- map["currency_id"]
        buyPrice <- map["buy_price"]
        sellPrice <- map["sell_price"]
        date <- map["date"]
        createdAt <- map["created_at"]
        bankNameAR <- map["bankNameAR"]
        bankNameEN <- map["bankNameEN"]
        currencyNameAR <- map["currencyNameAR"]
        currencyNameEN <- map["currencyNameEN"]
    }
}
