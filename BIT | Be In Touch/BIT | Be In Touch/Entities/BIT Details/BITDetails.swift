//
//  BITDetails.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/21/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

import Foundation
import ObjectMapper

class BITDetails: Mappable {
        
    var id: Int?
//    var price : String?
    var bitID: Int?
    var image: String?
    var externalLink: String?
    var createdAt: String?
    var updatedAt: String?
    var country: String?
    var title: String?
    var description: String?
    var containItens: Int?
//    "price": null,
    var label1: String?
    var label2: String?
    var label3: String?
    
//    "label1":"Name","label2":"Price","label3":null
    var attribue1: String?
    var attribue2: String?
    var attribue3: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
//        price <- map["price"]
        image <- map["image"]
        externalLink <- map["external_link"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        country <- map["country"]
        title <- map["title"]
        description <- map["desc"]
        bitID <- map["bit_id"]
        label1 <- map["bitData.title1"]
        label2 <- map["bitData.title2"]
        label3 <- map["bitData.title3"]
        attribue1 <- map["attr1"]
        attribue2 <- map["attr2"]
        attribue3 <- map["attr3"]
        containItens <- map["contain_items"]
    }
}

class HeaderData: Mappable {
    
    var title: String?
    var labell1: String?
    var labell2: String?
    var labell3: String?
    var secondlabell1: String?
    var secondlabell2: String?
    var secondlabell3: String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        //        price <- map["price"]
        labell1 <- map["labell1"]
        labell2 <- map["labell2"]
        labell3 <- map["labell3"]
        //        price <- map["price"]
        secondlabell1 <- map["secondlabell1"]
        secondlabell2 <- map["secondlabell2"]
        secondlabell3 <- map["secondlabell3"]
    }
}
