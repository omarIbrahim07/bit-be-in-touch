//
//  CurrencySymbols.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/19/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class CurrencySymbols: Mappable {
        
    var id: Int?
    var name : String?
    var nameEn: String?
    var symbol: String?
    var active: Int?
    var image: String?
    var createdAt: String?
    var updatedAt: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEn <- map["nameEN"]
        symbol <- map["symbol"]
        active <- map["active"]
        image <- map["image"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
    }
}
