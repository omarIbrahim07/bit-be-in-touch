//
//  Vegetable.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 6/3/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Vegetable: Mappable {
    
    var id: Int?
    var name : String?
    var nameEn : String?
    var image : String?
    var active : Int?
    var description : String?
    var unit : Int?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        nameEn <- map["nameEN"]
        image <- map["image"]
        active <- map["active"]
        description <- map["description"]
        unit <- map["unit"]
    }
}
