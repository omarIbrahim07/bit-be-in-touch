//
//  GoldRate.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/20/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class GoldRate: Mappable {
        
    var id: Int?
    var countryID : Int?
    var goldCaliberID: Int?
    var price: String?
    var date: String?
    var createdAt: String?
    var updatedAt: String?
    var goldCaliberNameAR: String?
    var goldCaliberNameEN: String?
    var countryNameAR: String?
    var countryNameEN: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        countryID <- map["country_id"]
        goldCaliberID <- map["goldcaliber_id"]
        price <- map["price"]
        date <- map["date"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        goldCaliberNameAR <- map["goldcaliberNameAR"]
        goldCaliberNameEN <- map["goldcaliberNameEN"]
        countryNameAR <- map["CountryNameAR"]
        countryNameEN <- map["CountryNameEN"]
    }
}
