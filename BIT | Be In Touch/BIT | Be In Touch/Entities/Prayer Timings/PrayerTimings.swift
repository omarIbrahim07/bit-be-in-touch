//
//  PrayerTimings.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 4/28/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class PrayerTimings: Mappable {
    
    var fajr: String?
    var sunRise : String?
    var dhuhr : String?
    var asr: String?
    var maghrib : String?
    var isha : String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        fajr <- map["Fajr"]
        sunRise <- map["Sunrise"]
        dhuhr <- map["Dhuhr"]
        asr <- map["Asr"]
        maghrib <- map["Maghrib"]
        isha <- map["Isha"]
    }
}
