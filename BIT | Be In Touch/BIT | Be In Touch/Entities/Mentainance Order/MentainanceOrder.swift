//
//  MentainanceOrder.swift
//  OOOOM
//
//  Created by Omar Ibrahim on 11/18/19.
//  Copyright © 2019 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class MentainanceOrder : Mappable {
    
    var id: Int?
    var service: String?
    var serviceEn: String?
    var createdAt: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        service <- map["service"]
        serviceEn <- map["serviceEN"]
        createdAt <- map["created_at"]
    }
    
}
