//
//  Setting.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 7/14/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Setting: Mappable {
        
    var id: Int?
    var deviceID : String?
    var bitCode: String?
    var createdAt: String?
    var updatedAt: String?
    var countries: [String]?
    var language: String?
    var description: String?
    var bitName: String?
    var bitImage: String?
    var bitOwner: String?
    var dynamicLinks: [DynamicLink]?
    var firstTemplate: Int?
    var secondTemplate: Int?
    var thirdTemplate: Int?
    var bitLabel11: String?
    var bitLabel12: String?
    var bitLabel13: String?
    var bitLabel21: String?
    var bitLabel22: String?
    var bitLabel23: String?
    var bitLabel31: String?
    var bitLabel32: String?
    var bitLabel33: String?
    var category: String?
    var sorting2: Int?
    var sorting3: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        deviceID <- map["device_id"]
        bitCode <- map["bit_code"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        countries <- map["countries"]
        language <- map["locale"]
        description <- map["description"]
        bitName <- map["bit_name"]
        bitImage <- map["bit_image"]
        bitOwner <- map["bit_owner"]
        dynamicLinks <- map["dynamic_links"]
        firstTemplate <- map["default_template"]
        secondTemplate <- map["second_template"]
        thirdTemplate <- map["third_template"]
        bitLabel11 <- map["bit_labell1"]
        bitLabel12 <- map["bit_labell2"]
        bitLabel13 <- map["bit_labell3"]
        bitLabel21 <- map["bit_label21"]
        bitLabel22 <- map["bit_label22"]
        bitLabel23 <- map["bit_label23"]
        bitLabel31 <- map["bit_label31"]
        bitLabel32 <- map["bit_label32"]
        bitLabel33 <- map["bit_label33"]
        category <- map["category"]
        sorting2 <- map["sorting2"]
        sorting3 <- map["sorting3"]
    }
}
