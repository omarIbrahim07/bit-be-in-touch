//
//  Terms.swift
//  BIT | Be In Touch
//
//  Created by Omar Ibrahim on 10/7/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Terms: Mappable {
        
    var conetntEN: String?
    var conetntAR: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        conetntAR <- map["conetntAR"]
        conetntEN <- map["conetntEN"]
    }
}
