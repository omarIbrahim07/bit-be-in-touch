//
//  Item.swift
//  Rawa
//
//  Created by Omar Ibrahim on 4/12/20.
//  Copyright © 2020 EgyDesigner. All rights reserved.
//

import Foundation
import ObjectMapper

class Item: Mappable {

    var id: Int?
    var title: String?
    var price: Int?
    var description: String?
        
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["name"]
        price <- map["price"]
        description <- map["description"]
    }
    
}
